package Effectors
using RTDB, Prereqs, Perks

// Increase time between hits when the player is sprinting
SprintTBHIncrease : ApplyStatGroupEffector
{
    prereqRecord = "Perks.IsPlayerSprinting"

    statGroup =
    {
        statModifiers = 
        [
            { statType = "BaseStats.TBHsBaseCoefficient" modifierType = "AdditiveMultiplier" value = 0.5 } : ConstantStatModifier,
        ]
    }
}

// Increase time between hits when the player is crouching
CrouchTBHIncrease : ApplyStatGroupEffector
{
    prereqRecord = "Perks.IsPlayerCrouching"

    statGroup =
    {
        statModifiers = 
        [
            { statType = "BaseStats.TBHsBaseCoefficient" modifierType = "AdditiveMultiplier" value = 0.5 } : ConstantStatModifier,
        ]
    }
}

// Increase time between hits when the player is in a car
VehicleTBHIncrease : ApplyStatGroupEffector
{
    prereqRecord = "Prereqs.IsInVehiclePrereq"

    statGroup =
    {
        statModifiers = 
        [
            { statType = "BaseStats.TBHsBaseCoefficient" modifierType = "AdditiveMultiplier" value = 2 } : ConstantStatModifier,
        ]
    }
}

// Increase time between hits when the player is in critical health
LowHealthTBHIncrease : ApplyStatusEffectEffector
{
    prereqRecord = 
    {
        prereqClassName = "ConstantStatPoolPrereq"
	    float valueToCheck = 40
	    CName statPoolType = "Health"
	    CName comparisonType = "Less"
        bool skipOnApply = true
	} : StatPoolPrereq

    statusEffect = "Effectors.LowHealthTBH"
}

// Increase time between hits when the player is in critical health
CriticalHealthTBHIncrease : ApplyStatusEffectEffector
{
    prereqRecord = 
    {
        prereqClassName = "ConstantStatPoolPrereq"
	    float valueToCheck = 20
	    CName statPoolType = "Health"
	    CName comparisonType = "Less"
        bool skipOnApply = true
	} : StatPoolPrereq

    statusEffect = "Effectors.CriticalHealthTBH"
}

// Increase time between hits when the player is in cover
InCoverTBHIncrease : ApplyStatGroupEffector
{
    prereqRecord = "Prereqs.UsingCoverPSMPrereq"

    statGroup =
    {
        statModifiers = 
        [
            { statType = "BaseStats.TBHsBaseCoefficient" modifierType = "AdditiveMultiplier" value = 1.5 } : ConstantStatModifier,
        ]
    }
}

// Increase time between hits when the player is in cover
DodgingTBHIncrease : ApplyStatGroupEffector
{
    prereqRecord = "Perks.IsPlayerDodging"

    statGroup =
    {
        statModifiers = 
        [
            { statType = "BaseStats.TBHsBaseCoefficient" modifierType = "AdditiveMultiplier" value = 1 } : ConstantStatModifier,
        ]
    }
}

// Increase time between hits when the player is in cover
SlidingTBHIncrease : ApplyStatGroupEffector
{
    prereqRecord = "Perks.IsPlayerSliding"

    statGroup =
    {
        statModifiers = 
        [
            { statType = "BaseStats.TBHsBaseCoefficient" modifierType = "AdditiveMultiplier" value = 1 } : ConstantStatModifier,
        ]
    }
}

// Force misses by NPCs for a brief window after they are hit
HitReactionTBHIncrease : ApplyEffectorEffector
{
    prereqRecord = { statusEffect = "BaseStatusEffect.HitReactionTBHCooldown" } : StatusEffectAbsentPrereq

    effectorToApply =
    {
        prereqRecord = "Prereqs.ProcessHitReceived"
        statusEffect = 
        {
            duration = 
            { 
                statModifiers = 
                [
                    { statType = "BaseStats.MaxDuration" modifierType = "Additive" value = 0.3 } : ConstantStatModifier,
                ]
            }

            packages =
            [
                {
                    stats =
                    [
                        { statType = "BaseStats.Accuracy" modifierType = "Multiplier" value = 0.000001 } : ConstantStatModifier,
                    ]
                    effectors = 
                    [
                        "Effectors.HitReactionTBHCooldown"
                    ]
                }
            ]
        } : StatusEffect
        removeWithEffector = false
    } : ApplyStatusEffectEffector
}

HitReactionTBHCooldown : ApplyStatusEffectEffector
{
    statusEffect = "BaseStatusEffect.HitReactionTBHCooldown" 
    removeWithEffector = false
}

LowHealthTBH : StatusEffect
{
    duration = 
    { 
        statModifiers = 
        [
            { statType = "BaseStats.MaxDuration" modifierType = "Additive" value = 1.5 } : ConstantStatModifier,
        ]
    }

    packages =
    [
        {
            stats =
            [
                { statType = "BaseStats.TBHsBaseCoefficient" modifierType = "Multiplier" value = 3 } : ConstantStatModifier,
            ]
        }
    ]
}

CriticalHealthTBH : StatusEffect
{
    duration = 
    { 
        statModifiers = 
        [
            { statType = "BaseStats.MaxDuration" modifierType = "Additive" value = 2.5 } : ConstantStatModifier,
        ]
    }

    packages =
    [
        {
            stats =
            [
                { statType = "BaseStats.TBHsBaseCoefficient" modifierType = "Multiplier" value = 3 } : ConstantStatModifier,
            ]
        }
    ]
}