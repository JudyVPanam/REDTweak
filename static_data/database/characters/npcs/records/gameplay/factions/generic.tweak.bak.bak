package Character
using RTDB, BaseStats, Factions, ArchetypeData, ArchetypeType

// AK TODO: update after character art is done

// Additional info on archetypes : https://docs.google.com/spreadsheets/d/1tLsNjKlr-7LFx2RGev7lFxHFBgt8X72hQu5Up80vrfk/edit#gid=1709769177

//-----------------------------------------------------------
//---------------------- BASE RECORDS -----------------------
//-----------------------------------------------------------

generic_street_base : Gameplay_NPC_Base < Character >
{
	reactionPreset = "ReactionPresets.Ganger_Aggressive"

	actionMap = "Gang.Map"

	fk< AISquadParams > squadParamsID = "FactionSquads.GenericSquad"

	fk< Affiliation > affiliation = "Factions.Unaffiliated"

	visualTags = [ "Generic" ]

    bountyDrawTable = "BountyDrawTable.Generic"
	abilities += 
	[
		"Ability.IsDefensive"
		"Ability.CanUseFragGrenades"
	]
	isBumpable = true
}

generic_street_base_ma : generic_street_base < Character >
{
	entityTemplatePath = "base\characters\entities\corpo\corpo__generic_ma.ent"
	affiliation = "Factions.UnaffiliatedCorpo"
}
 
generic_street_base_wa : generic_street_base < Character >
{
	entityTemplatePath = "base\characters\entities\corpo\corpo__generic_wa.ent"
	affiliation = "Factions.UnaffiliatedCorpo"
}

generic_street_base_mb : generic_street_base < Character >
{
	visualTags += [ "Big" ]

	entityTemplatePath = "base\characters\entities\corpo\corpo_generic_mb.ent"
	affiliation = "Factions.UnaffiliatedCorpo"
}

generic_street_base_wb : generic_street_base < Character >
{
	entityTemplatePath = "base\characters\entities\corpo\corpo__generic_wa.ent"
	affiliation = "Factions.UnaffiliatedCorpo"
}

//-----------------------------------------------------------
//------------------------- RECORDS -------------------------
//-----------------------------------------------------------

//--------------------------- MELEE GRUNTS-------------------------
//GameDebug.SpawnChRec(Character.generic_grunt_melee1_knife_ma)
generic_grunt_melee1_knife_ma : generic_street_base_ma
{
	primaryEquipment	= 
	{ 
		equipmentItems = 
		[ 
			{ 
				item = "Items.Preset_Knife_Military" equipSlot = "AttachmentSlots.WeaponRight"
				equipCondition = [ "WeaponConditions.BaseMeleePrimaryWeaponEquipCondition" ]
				unequipCondition = [ "WeaponConditions.BaseMeleePrimaryWeaponUnequipCondition" ]
			} : NPCEquipmentItem 
		] 
	}
	secondaryEquipment	= 
	{	
		equipmentItems =
		[
			{} : TygerClawsSecondaryHandgunPool
		] 
	}
	rarity = "NPCRarity.Normal"
	visualTags += [ "Grunt" ]
	archetypeData = { type = { localizedName = "LocKey#22345" } : ArchetypeType.GenericMeleeT1 } : ArchetypeData.GenericMeleeT1
}

//----------------------- RANGED GRUNTS -----------------------

//GameDebug.SpawnChRec(Character.generic_grunt_ranged1_slaughtomatic_ma)
generic_grunt_ranged1_slaughtomatic_ma : generic_street_base_ma
{
	primaryEquipment	= 
	{	
		equipmentItems =
		[
			{} : MaelstromPrimaryHandgunPool
		] 
	}
	secondaryEquipment	= 
	{	
		equipmentItems =
		[
			{} : TygerClawsSecondaryHandgunPool
		] 
	}

	visualTags += [ "Grunt" ]
	rarity = "NPCRarity.Normal"
	archetypeData = { type = { localizedName = "LocKey#22343" } : ArchetypeType.GenericRangedT1 } : ArchetypeData.GenericRangedT1
}

//GameDebug.SpawnChRec(Character.generic_grunt_ranged1_slaughtomatic_wa)
generic_grunt_ranged1_slaughtomatic_wa : generic_street_base_wa
{
	primaryEquipment	= 
	{	
		equipmentItems =
		[
			{} : MaelstromPrimaryHandgunPool
		] 
	}
	secondaryEquipment	= 
	{	
		equipmentItems =
		[
			{} : TygerClawsSecondaryHandgunPool
		] 
	}

	visualTags += [ "Grunt", "Lvl1" ]
	rarity = "NPCRarity.Normal"
	archetypeData = { type = { localizedName = "LocKey#42618" } : ArchetypeType.GenericRangedT1 } : ArchetypeData.GenericRangedT1
}

//GameDebug.SpawnChRec(Character.generic_grunt_ranged1_pulsar_ma)
generic_grunt_ranged1_pulsar_ma : generic_street_base_ma
{
	primaryEquipment	= 
	{	
		equipmentItems =
		[
			{} : MaelstromSMGPool
		] 
	}
	secondaryEquipment	= 
	{	
		equipmentItems =
		[
			{} : TygerClawsSecondaryHandgunPool
		] 
	}

	visualTags += [ "Grunt" ]
	rarity = "NPCRarity.Normal"
	archetypeData = { type = { localizedName = "LocKey#22343" } : ArchetypeType.GenericRangedT1 } : ArchetypeData.GenericRangedT1
}

//GameDebug.SpawnChRec(Character.generic_grunt_ranged1_pulsar_wa)
generic_grunt_ranged1_pulsar_wa : generic_street_base_wa
{
	primaryEquipment	= 
	{	
		equipmentItems =
		[
			{} : MaelstromSMGPool
		] 
	}
	secondaryEquipment	= 
	{	
		equipmentItems =
		[
			{} : TygerClawsSecondaryHandgunPool
		] 
	}

	visualTags += [ "Grunt", "Lvl1" ]
	rarity = "NPCRarity.Normal"
	archetypeData = { type = { localizedName = "LocKey#42618" } : ArchetypeType.GenericRangedT1 } : ArchetypeData.GenericRangedT1
}

//GameDebug.SpawnChRec(Character.generic_merc_ranged2_ajax_ma)
generic_merc_ranged2_ajax_ma : generic_street_base_ma
{
	primaryEquipment	= 
	{	
		equipmentItems =
		[
			{} : MaelstromAssaultRiflePool
		] 
	}
	secondaryEquipment	= 
	{	
		equipmentItems =
		[
			{} : TygerClawsSecondaryHandgunPool
		] 
	}

	visualTags += [ "Grunt" ]
	rarity = "NPCRarity.Weak"
	archetypeData = { type = { localizedName = "LocKey#22344" } : ArchetypeType.GenericRangedT2 } : ArchetypeData.GenericRangedT2
}

//GameDebug.SpawnChRec(Character.generic_merc_ranged2_ajax_wa)
generic_merc_ranged2_ajax_wa : generic_street_base_wa
{
	primaryEquipment	= 
	{	
		equipmentItems =
		[
			{} : MaelstromAssaultRiflePool
		] 
	}
	secondaryEquipment	= 
	{	
		equipmentItems =
		[
			{} : TygerClawsSecondaryHandgunPool
		] 
	}

	visualTags += [ "Grunt", "Lvl1" ]
	rarity = "NPCRarity.Weak"
	archetypeData = { type = { localizedName = "LocKey#42619" } : ArchetypeType.GenericRangedT2 } : ArchetypeData.GenericRangedT2
}

//GameDebug.SpawnChRec(Character.generic_merc_ranged2_copperhead_ma)
generic_merc_ranged2_copperhead_ma : generic_street_base_ma
{
	primaryEquipment	= 
	{	
		equipmentItems =
		[
			{} : MaelstromAssaultRiflePool
		] 
	}
	secondaryEquipment	= 
	{	
		equipmentItems =
		[
			{} : TygerClawsSecondaryHandgunPool
		] 
	}

	visualTags += [ "Grunt" ]
	rarity = "NPCRarity.Weak"
	archetypeData = { type = { localizedName = "LocKey#22344" } : ArchetypeType.GenericRangedT2 } : ArchetypeData.GenericRangedT2
}

//GameDebug.SpawnChRec(Character.generic_merc_ranged2_copperhead_wa)
generic_merc_ranged2_copperhead_wa : generic_street_base_wa
{
	primaryEquipment	= 
	{	
		equipmentItems =
		[
			{} : MaelstromAssaultRiflePool
		] 
	}
	secondaryEquipment	= 
	{	
		equipmentItems =
		[
			{} : TygerClawsSecondaryHandgunPool
		] 
	}

	visualTags += [ "Grunt", "Lvl1" ]
	rarity = "NPCRarity.Weak"
	archetypeData = { type = { localizedName = "LocKey#42619" } : ArchetypeType.GenericRangedT2 } : ArchetypeData.GenericRangedT2
}

//----------------------- SHOTGUN -----------------------

//GameDebug.SpawnChRec(Character.generic_shotgun_shotgun2_palica_ma)
generic_shotgun_fshotgun2_palica_ma : generic_street_base_ma
{
	primaryEquipment	= 
	{	
		equipmentItems =
		[
			{} : AnimalsShotgunPool
		] 
	}
	secondaryEquipment	= 
	{	
		equipmentItems =
		[
			{} : TygerClawsSecondaryHandgunPool
		] 
	}

	visualTags += [ "Shotgun" ]
	rarity = "NPCRarity.Normal"
	archetypeData = { type = { localizedName = "LocKey#42621" } : ArchetypeType.FastShotgunnerT2 } : ArchetypeData.FastShotgunnerT2
}

//GameDebug.SpawnChRec(Character.generic_shotgun_shotgun3_carnage_mb_elite)
generic_shotgun_shotgun3_carnage_mb_elite : generic_street_base_mb
{
	primaryEquipment	= 
	{	
		equipmentItems =
		[
			{} : AnimalsShotgunPool
		] 
	}
	secondaryEquipment	= 
	{	
		equipmentItems =
		[
			{} : TygerClawsSecondaryHandgunPool
		] 
	}
	visualTags += [ "Enforcer" ]
	rarity = "NPCRarity.Elite"
	archetypeData = { type = { localizedName = "LocKey#22438" } : ArchetypeType.ShotgunnerT3 } : ArchetypeData.ShotgunnerT3
}


//----------------------- FAST MELEE -----------------------
//GameDebug.SpawnChRec(Character.generic_fast_fmelee2_knife_wa_rare)
generic_fast_fmelee2_knife_wa_rare : generic_street_base_wa
{
	primaryEquipment	= 
	{ 
		equipmentItems = 
		[ 
			{ 
				item = "Items.Preset_Knife_Military" equipSlot = "AttachmentSlots.WeaponRight"
				equipCondition = [ "WeaponConditions.BaseMeleePrimaryWeaponEquipCondition" ]
				unequipCondition = [ "WeaponConditions.BaseMeleePrimaryWeaponUnequipCondition" ]
			} : NPCEquipmentItem 
		] 
	}
	secondaryEquipment	= 
	{	
		equipmentItems =
		[
			{} : TygerClawsSecondaryHandgunPool
		] 
	}
	
	visualTags += [ "Lvl2", "Fast" ]
	rarity = "NPCRarity.Rare"
	archetypeData = { type = { localizedName = "LocKey#42624" } : ArchetypeType.FastMeleeT2 } : ArchetypeData.FastMeleeT2
}

//GameDebug.SpawnChRec(Character.generic_fast_fmelee2_machete_ma_rare)
generic_fast_fmelee2_machete_ma_rare : generic_street_base_ma
{
	primaryEquipment	= 
	{ 
		equipmentItems = 
		[ 
			{ 
				item = "Items.Preset_Machete_Default" equipSlot = "AttachmentSlots.WeaponRight"
				equipCondition = [ "WeaponConditions.BaseMeleePrimaryWeaponEquipCondition" ]
				unequipCondition = [ "WeaponConditions.BaseMeleePrimaryWeaponUnequipCondition" ]
			} : NPCEquipmentItem 
		] 
	}
	secondaryEquipment	= 
	{	
		equipmentItems =
		[
			{} : TygerClawsSecondaryHandgunPool
		] 
	}
	
	visualTags += [ "Lvl2", "Fast" ]
	rarity = "NPCRarity.Rare"
	archetypeData = { type = { localizedName = "LocKey#42623" } : ArchetypeType.FastMeleeT2 } : ArchetypeData.FastMeleeT2
}

//----------------------- HEAVY MELEE -----------------------
//----------------------- NETRUNNER -----------------------
//GameDebug.SpawnChRec(Character.generic_netrunner_netrunner_chao_ma_rare)
generic_netrunner_netrunner_chao_ma_rare : generic_street_base_ma
{
    primaryEquipment	= 
	{	
		equipmentItems =
		[
			{} : TygerClawsPrimaryHandgunPool
		] 
	}
	secondaryEquipment	= 
	{	
		equipmentItems =
		[
			{} : TygerClawsSecondaryHandgunPool
		] 
	}

	visualTags += [ "Netrunner" ]
	rarity = "NPCRarity.Rare"
	archetypeData = { type = { localizedName = "LocKey#42626" } : ArchetypeType.NetrunnerT2 } : ArchetypeData.NetrunnerT2
}

//GameDebug.SpawnChRec(Character.generic_netrunner_netrunner_chao_wa_rare)
generic_netrunner_netrunner_chao_wa_rare : generic_street_base_wa
{
    primaryEquipment	= 
	{	
		equipmentItems =
		[
			{} : TygerClawsPrimaryHandgunPool
		] 
	}
	secondaryEquipment	= 
	{	
		equipmentItems =
		[
			{} : TygerClawsSecondaryHandgunPool
		] 
	}

	visualTags += [ "Netrunner" ]
	rarity = "NPCRarity.Rare"
	archetypeData = { type = { localizedName = "LocKey#42626" } : ArchetypeType.NetrunnerT2 } : ArchetypeData.NetrunnerT2
}

//----------------------- GUNNER ----------------------------

//GameDebug.SpawnChRec(Character.generic_gunner_gunner3_hmg_mb_elite)
generic_gunner_gunner3_hmg_mb_elite : generic_street_base_mb
{
	primaryEquipment	= 
	{ 
		equipmentItems = 
		[ 
			{ item = "Items.Preset_HMG_Neon" equipSlot = "AttachmentSlots.WeaponRight" } : NPCEquipmentItem 
		] 
	}
	secondaryEquipment	= 
	{	
		equipmentItems =
		[
			{} : AnimalsSecondaryHandgunPool
		] 
	}

	visualTags += [ "Enforcer" ]
	rarity = "NPCRarity.Elite"
	archetypeData = { type = { localizedName = "LocKey#42627" } : ArchetypeType.HeavyRangedT3 } : ArchetypeData.HeavyRangedT3
}

//----------------------- SNIPER -----------------------
//GameDebug.SpawnChRec(Character.generic_sniper_sniper2_grad_ma_rare)
generic_sniper_sniper2_grad_ma_rare : generic_street_base_ma
{
	primaryEquipment	= 
	{ 
		equipmentItems = 
		[ 	
			{ 	
				item = "Items.Preset_Nekomata_Neon" equipSlot = "AttachmentSlots.WeaponRight"  onBodySlot = "AttachmentSlots.ItemSlotGenericRanged"
				equipCondition = [ "WeaponConditions.SniperPrimaryWeaponSniperEquipCondition" ] 
				unequipCondition = [ "WeaponConditions.SniperPrimaryWeaponSniperUnequipCondition" ] 
			} : NPCEquipmentItem

			{ 	
				item = "Items.Preset_Pulsar_Neon" equipSlot = "AttachmentSlots.WeaponRight"  onBodySlot = "AttachmentSlots.ItemSlotGenericRanged"
				equipCondition = [ "WeaponConditions.SniperPrimaryWeaponRangedEquipCondition" ] 
				unequipCondition = [ "WeaponConditions.SniperPrimaryWeaponRangedUnequipCondition" ] 
			} : NPCEquipmentItem
		]
	}
	secondaryEquipment	= 
	{	
		equipmentItems =
		[
			{} : TygerClawsSecondaryHandgunPool
		] 
	}
	sensePreset = "Senses.SniperRelaxed50m"
	relaxedSensesPreset	= "SniperRelaxed50m"
	alertedSensesPreset	= "SniperAlerted50m"
	combatSensesPreset	= "SniperCombat50m"
	rarity = "NPCRarity.Rare"
	visualTags += [ "Sniper" ]
	archetypeData = { type = { localizedName = "LocKey#42628" } : ArchetypeType.SniperT2 } : ArchetypeData.SniperT2
}

//----------------Obsolete, to be phased out-----------------

//----------------------- SHORT RANGE -----------------------

//GameDebug.SpawnChRec(Character.street_big_goon_short_range_m_easy)
street_big_goon_short_range_m_easy : generic_street_base_mb < Character >
{
	primaryEquipment	= 
	{ 
		equipmentItems = 
		[ 	
			{ 	
				item = "Items.Preset_Testera_Neon" equipSlot = "AttachmentSlots.WeaponRight"  onBodySlot = "AttachmentSlots.ItemSlotGenericRanged"
				equipCondition = [ "WeaponConditions.BaseRangedPrimaryWeaponEquipCondition" ]
				unequipCondition = [ "WeaponConditions.BaseRangedPrimaryWeaponUnequipCondition" ]
			} : NPCEquipmentItem
		]
	}
	secondaryEquipment	= 
	{ 
		equipmentItems = 
		[ 
			{ item = "Items.Preset_Lexington_Default" equipSlot = "AttachmentSlots.WeaponRight" onBodySlot = "AttachmentSlots.ItemSlotHandgunLeft" } : NPCEquipmentItem 
			//{ item = "Items.w_melee_004__fists_a" equipSlot = "AttachmentSlots.WeaponRight" } : NPCEquipmentItem 
		] 
	}

	visualTags += [ "Lvl1" ]
}
//GameDebug.SpawnChRec(Character.street_big_goon_short_range_m_medium)
street_big_goon_short_range_m_medium : generic_street_base_mb < Character >
{
	primaryEquipment	= 
	{ 
		equipmentItems = 
		[ 	
			{ 	
				item = "Items.Preset_Igla_Neon" equipSlot = "AttachmentSlots.WeaponRight"  onBodySlot = "AttachmentSlots.ItemSlotGenericRanged"
				equipCondition = [ "WeaponConditions.BaseRangedPrimaryWeaponEquipCondition" ]
				unequipCondition = [ "WeaponConditions.BaseRangedPrimaryWeaponUnequipCondition" ]
			} : NPCEquipmentItem
		]
	}
	secondaryEquipment	= 
	{ 
		equipmentItems = 
		[ 
			{ item = "Items.Preset_Nova_Default" equipSlot = "AttachmentSlots.WeaponRight" onBodySlot = "AttachmentSlots.ItemSlotHandgunLeft" } : NPCEquipmentItem 
			//{ item = "Items.Preset_Iron_Pipe_Default" equipSlot = "AttachmentSlots.WeaponRight" onBodySlot = "AttachmentSlots.ItemSlotKnifeLeft" } : NPCEquipmentItem 
		] 
	}

	visualTags += [ "Lvl2" ]
}
//GameDebug.SpawnChRec(Character.street_big_goon_short_range_m_hard)
street_big_goon_short_range_m_hard : generic_street_base_mb < Character >
{
	primaryEquipment	= 
	{ 
		equipmentItems = 
		[ 	
			{ 	
				item = "Items.Preset_Satara_Neon" equipSlot = "AttachmentSlots.WeaponRight"  onBodySlot = "AttachmentSlots.ItemSlotGenericRanged"
				equipCondition = [ "WeaponConditions.BaseRangedPrimaryWeaponEquipCondition" ]
				unequipCondition = [ "WeaponConditions.BaseRangedPrimaryWeaponUnequipCondition" ]
			} : NPCEquipmentItem
		]
	}
	secondaryEquipment	= 
	{ 
		equipmentItems = 
		[ 
			{ item = "Items.Preset_Quasar_Default" equipSlot = "AttachmentSlots.WeaponRight" onBodySlot = "AttachmentSlots.ItemSlotHandgunLeft" } : NPCEquipmentItem 
			//{ item = "Items.Preset_Knife_Default" equipSlot = "AttachmentSlots.WeaponRight" onBodySlot = "AttachmentSlots.ItemSlotKnifeLeft" } : NPCEquipmentItem 
		] 
	}

	visualTags += [ "Lvl3" ]
}

//GameDebug.SpawnChRec(Character.street_big_goon_short_range_w_easy)
street_big_goon_short_range_w_easy : generic_street_base_wb < Character >
{
	primaryEquipment	= 
	{ 
		equipmentItems = 
		[ 	
			{ 	
				item = "Items.Preset_Testera_Default" equipSlot = "AttachmentSlots.WeaponRight"  onBodySlot = "AttachmentSlots.ItemSlotGenericRanged"
				equipCondition = [ "WeaponConditions.BaseRangedPrimaryWeaponEquipCondition" ]
				unequipCondition = [ "WeaponConditions.BaseRangedPrimaryWeaponUnequipCondition" ]
			} : NPCEquipmentItem
		]
	}
	secondaryEquipment	= 
	{ 
		equipmentItems = 
		[ 
			{ item = "Items.Preset_Lexington_Default" equipSlot = "AttachmentSlots.WeaponRight" onBodySlot = "AttachmentSlots.ItemSlotHandgunLeft" } : NPCEquipmentItem 
			//{ item = "Items.w_melee_004__fists_a" equipSlot = "AttachmentSlots.WeaponRight" } : NPCEquipmentItem 
		] 
	}

	visualTags += [ "Lvl1" ]
}
//GameDebug.SpawnChRec(Character.street_big_goon_short_range_w_medium)
street_big_goon_short_range_w_medium : generic_street_base_wb < Character >
{
	primaryEquipment	= 
	{ 
		equipmentItems = 
		[ 	
			{ 	
				item = "Items.Preset_Igla_Default" equipSlot = "AttachmentSlots.WeaponRight"  onBodySlot = "AttachmentSlots.ItemSlotGenericRanged"
				equipCondition = [ "WeaponConditions.BaseRangedPrimaryWeaponEquipCondition" ]
				unequipCondition = [ "WeaponConditions.BaseRangedPrimaryWeaponUnequipCondition" ]
			} : NPCEquipmentItem
		]
	}
	secondaryEquipment	= 
	{ 
		equipmentItems = 
		[ 
			{ item = "Items.Preset_Nova_Default" equipSlot = "AttachmentSlots.WeaponRight" onBodySlot = "AttachmentSlots.ItemSlotHandgunLeft" } : NPCEquipmentItem 
			//{ item = "Items.Preset_Iron_Pipe_Default" equipSlot = "AttachmentSlots.WeaponRight" onBodySlot = "AttachmentSlots.ItemSlotKnifeLeft" } : NPCEquipmentItem 
		] 
	}

	visualTags += [ "Lvl2" ]
}
//GameDebug.SpawnChRec(Character.street_big_goon_short_range_w_hard)
street_big_goon_short_range_w_hard : generic_street_base_wb < Character >
{
	primaryEquipment	= 
	{ 
		equipmentItems = 
		[ 	
			{ 	
				item = "Items.Preset_Satara_Default" equipSlot = "AttachmentSlots.WeaponRight"  onBodySlot = "AttachmentSlots.ItemSlotGenericRanged"
				equipCondition = [ "WeaponConditions.BaseRangedPrimaryWeaponEquipCondition" ]
				unequipCondition = [ "WeaponConditions.BaseRangedPrimaryWeaponUnequipCondition" ]
			} : NPCEquipmentItem
		]
	}
	secondaryEquipment	= 
	{ 
		equipmentItems = 
		[ 
			{ item = "Items.Preset_Quasar_Default" equipSlot = "AttachmentSlots.WeaponRight" onBodySlot = "AttachmentSlots.ItemSlotHandgunLeft" } : NPCEquipmentItem 
			//{ item = "Items.Preset_Knife_Default" equipSlot = "AttachmentSlots.WeaponRight" onBodySlot = "AttachmentSlots.ItemSlotKnifeLeft" } : NPCEquipmentItem 
		] 
	}

	visualTags += [ "Lvl3" ]
}


//-------------------------- MEDIUM RANGE --------------------------

//GameDebug.SpawnChRec(Character.street_goon_medium_range_m_easy)
street_goon_medium_range_m_easy : generic_street_base_ma < Character >
{
	primaryEquipment	= 
	{ 
		equipmentItems = 
		[ 	
			{ 	
				item = "Items.Preset_Lexington_Default" equipSlot = "AttachmentSlots.WeaponRight"  onBodySlot = "AttachmentSlots.ItemSlotGenericRanged"
				equipCondition = [ "WeaponConditions.BaseRangedPrimaryWeaponEquipCondition" ] 
				unequipCondition = [ "WeaponConditions.BaseRangedPrimaryWeaponUnequipCondition" ] 
			} : NPCEquipmentItem
		]
	}
	secondaryEquipment	= 
	{ 
		equipmentItems = 
		[ 
			{ item = "Items.Preset_Quasar_Default" equipSlot = "AttachmentSlots.WeaponRight" onBodySlot = "AttachmentSlots.ItemSlotHandgunLeft" } : NPCEquipmentItem 
			//{ item = "Items.Preset_Knife_Default" equipSlot = "AttachmentSlots.WeaponRight" onBodySlot = "AttachmentSlots.ItemSlotKnifeLeft" } : NPCEquipmentItem 
		] 
	}

	visualTags += [ "Lvl1" ]
}
//GameDebug.SpawnChRec(Character.street_goon_medium_range_m_medium)
street_goon_medium_range_m_medium : generic_street_base_ma < Character >
{
	primaryEquipment	= 
	{ 
		equipmentItems = 
		[ 	
			{ 	
				item = "Items.Preset_Nova_Default" equipSlot = "AttachmentSlots.WeaponRight"  onBodySlot = "AttachmentSlots.ItemSlotGenericRanged"
				equipCondition = [ "WeaponConditions.BaseRangedPrimaryWeaponEquipCondition" ] 
				unequipCondition = [ "WeaponConditions.BaseRangedPrimaryWeaponUnequipCondition" ] 
			} : NPCEquipmentItem
		]
	}
	secondaryEquipment	= 
	{ 
		equipmentItems = 
		[ 
			{ item = "Items.Preset_Nova_Default" equipSlot = "AttachmentSlots.WeaponRight" onBodySlot = "AttachmentSlots.ItemSlotHandgunLeft" } : NPCEquipmentItem 
			//{ item = "Items.Preset_Knife_Default" equipSlot = "AttachmentSlots.WeaponRight" onBodySlot = "AttachmentSlots.ItemSlotKnifeLeft" } : NPCEquipmentItem 
		] 
	}

	visualTags += [ "Lvl2" ]
}
//GameDebug.SpawnChRec(Character.street_goon_medium_range_m_hard)
street_goon_medium_range_m_hard : generic_street_base_ma < Character >
{
	primaryEquipment	= 
	{ 
		equipmentItems = 
		[ 	
			{ 	
				item = "Items.Preset_Pulsar_Default" equipSlot = "AttachmentSlots.WeaponRight"  onBodySlot = "AttachmentSlots.ItemSlotGenericRanged"
				equipCondition = [ "WeaponConditions.BaseRangedPrimaryWeaponEquipCondition" ] 
				unequipCondition = [ "WeaponConditions.BaseRangedPrimaryWeaponUnequipCondition" ] 
			} : NPCEquipmentItem
		]
	}
	secondaryEquipment	= 
	{ 
		equipmentItems = 
		[ 
			{ item = "Items.Preset_Nova_Default" equipSlot = "AttachmentSlots.WeaponRight" onBodySlot = "AttachmentSlots.ItemSlotHandgunLeft" } : NPCEquipmentItem 
			//{ item = "Items.Preset_Knife_Default" equipSlot = "AttachmentSlots.WeaponRight" onBodySlot = "AttachmentSlots.ItemSlotKnifeLeft" } : NPCEquipmentItem 
		] 
	}

	visualTags += [ "Lvl3" ]
}

//GameDebug.SpawnChRec(Character.street_goon_medium_range_w_easy)
street_goon_medium_range_w_easy : generic_street_base_wa < Character >
{
	primaryEquipment	= 
	{ 
		equipmentItems = 
		[ 	
			{ 	
				item = "Items.Preset_Lexington_Default" equipSlot = "AttachmentSlots.WeaponRight"  onBodySlot = "AttachmentSlots.ItemSlotGenericRanged"
				equipCondition = [ "WeaponConditions.BaseRangedPrimaryWeaponEquipCondition" ] 
				unequipCondition = [ "WeaponConditions.BaseRangedPrimaryWeaponUnequipCondition" ] 
			} : NPCEquipmentItem
		] 
	}
	secondaryEquipment	= 
	{ 
		equipmentItems = 
		[ 
			{ item = "Items.Preset_Nova_Default" equipSlot = "AttachmentSlots.WeaponRight" onBodySlot = "AttachmentSlots.ItemSlotHandgunLeft" } : NPCEquipmentItem 
			//{ item = "Items.Preset_Knife_Default" equipSlot = "AttachmentSlots.WeaponRight" onBodySlot = "AttachmentSlots.ItemSlotKnifeLeft" } : NPCEquipmentItem 
		] 
	}

	visualTags += [ "Lvl1" ]
}
//GameDebug.SpawnChRec(Character.street_goon_medium_range_w_medium)
street_goon_medium_range_w_medium : generic_street_base_wa < Character >
{
	primaryEquipment	= 
	{ 
		equipmentItems = 
		[ 	
			{ 	
				item = "Items.Preset_Nova_Default" equipSlot = "AttachmentSlots.WeaponRight"  onBodySlot = "AttachmentSlots.ItemSlotGenericRanged"
				equipCondition = [ "WeaponConditions.BaseRangedPrimaryWeaponEquipCondition" ] 
				unequipCondition = [ "WeaponConditions.BaseRangedPrimaryWeaponUnequipCondition" ] 
			} : NPCEquipmentItem
			{ 
				item = "Items.Preset_Iron_Pipe_Default" equipSlot = "AttachmentSlots.WeaponRight" onBodySlot = "AttachmentSlots.ItemSlotGenericMelee" 
				equipCondition = [ "WeaponConditions.RangedHybridPrimaryWeaponMeleeEquipCondition" ] 
				unequipCondition = [ "WeaponConditions.RangedHybridPrimaryWeaponMeleeUnequipCondition" ] 
			} : NPCEquipmentItem
		] 
	}
	secondaryEquipment	= 
	{ 
		equipmentItems = 
		[ 
			{ item = "Items.Preset_Nova_Default" equipSlot = "AttachmentSlots.WeaponRight" onBodySlot = "AttachmentSlots.ItemSlotHandgunLeft" } : NPCEquipmentItem 
			//{ item = "Items.Preset_Knife_Default" equipSlot = "AttachmentSlots.WeaponRight" onBodySlot = "AttachmentSlots.ItemSlotKnifeLeft" } : NPCEquipmentItem 
		] 
	}

	visualTags += [ "Lvl2" ]
}
//GameDebug.SpawnChRec(Character.street_goon_medium_range_w_hard)
street_goon_medium_range_w_hard : generic_street_base_wa < Character >
{
	primaryEquipment	=  
	{ 
		equipmentItems = 
		[ 	
			{ 	
				item = "Items.Preset_Pulsar_Default" equipSlot = "AttachmentSlots.WeaponRight"  onBodySlot = "AttachmentSlots.ItemSlotGenericRanged"
				equipCondition = [ "WeaponConditions.BaseRangedPrimaryWeaponEquipCondition" ] 
				unequipCondition = [ "WeaponConditions.BaseRangedPrimaryWeaponUnequipCondition" ] 
			} : NPCEquipmentItem
		] 
	}
	secondaryEquipment	= 
	{ 
		equipmentItems = 
		[ 
			{ item = "Items.Preset_Nova_Default" equipSlot = "AttachmentSlots.WeaponRight" onBodySlot = "AttachmentSlots.ItemSlotHandgunLeft" } : NPCEquipmentItem 
			//{ item = "Items.Preset_Knife_Default" equipSlot = "AttachmentSlots.WeaponRight" onBodySlot = "AttachmentSlots.ItemSlotKnifeLeft" } : NPCEquipmentItem 
		] 
	}

	visualTags += [ "Lvl3" ]
}

//------------------------ LONG RANGE -----------------------


//------------------------- NETRUNNER -----------------------


//-------------------------- ROBOTS -------------------------
// GameDebug.SpawnChRec(Character.boxingrobot_easy)
boxingrobot_easy : Android_NPC_Base 
{
	entityTemplatePath = "base\characters\entities\generic_enemy\generic__boxingrobot_ma.ent"
    actionMap = "GangAndroid.Map"
	displayName = "Boxing Robot"
	reactionPreset = "ReactionPresets.Mechanical_Passive"
    baseAttitudeGroup = "neutral"

	visualTags += [ "Lvl1" ]
    
	primaryEquipment = 
	{ 
		equipmentItems = 
		[ 
			{ 	
				item = "Items.NPC_Strong_Arms" equipSlot = "AttachmentSlots.WeaponRight"
			} : NPCEquipmentItem
		] 
	}

	archetypeData = "ArchetypeData.AndroidMeleeT1"
}

// GameDebug.SpawnChRec(Character.boxingrobot_medium)
boxingrobot_medium : Android_NPC_Base
{
	entityTemplatePath = "base\characters\entities\generic_enemy\generic__boxingrobot_ma.ent"
    actionMap = "GangAndroid.Map"
	displayName = "Boxing Robot"
	reactionPreset = "ReactionPresets.Mechanical_Passive"
    baseAttitudeGroup = "neutral"

	visualTags += [ "Lvl2" ]
    
	primaryEquipment	= 
	{ 
		equipmentItems = 
		[ 	
			{ 	
				item = "Items.NPC_Strong_Arms" equipSlot = "AttachmentSlots.WeaponRight"
			} : NPCEquipmentItem
		]  
	}
	
	archetypeData = "ArchetypeData.AndroidMeleeT2"
}

// GameDebug.SpawnChRec(Character.boxingrobot_hard)
boxingrobot_hard : Android_NPC_Base
{
	entityTemplatePath = "base\characters\entities\generic_enemy\generic__boxingrobot_ma.ent"
    actionMap = "GangAndroid.Map"
	displayName = "Boxing Robot"
	reactionPreset = "ReactionPresets.Mechanical_Passive"
    baseAttitudeGroup = "neutral"

	fk< StatModifier >[] statModifiers += 
	[		
		"NPCStatPreset.MediumHackingResistMod"
		{ statType = "BaseStats.Health" 	modifierType = "AdditiveMultiplier" 	value = 10	} : ConstantStatModifier
	]

	visualTags += [ "Lvl3" ]
    
	primaryEquipment = 
	{ 
		equipmentItems = 
		[ 
			{ 	
				item = "Items.NPC_Strong_Arms" equipSlot = "AttachmentSlots.WeaponRight"
				equipCondition = [ "WeaponConditions.BaseMeleePrimaryWeaponEquipCondition" ] 
			} : NPCEquipmentItem
		] 
	}

	archetypeData = "ArchetypeData.AndroidMeleeT2"
}