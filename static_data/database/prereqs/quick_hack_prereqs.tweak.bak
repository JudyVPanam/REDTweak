package Prereqs
using RTDB

QuickHackUploadingPrereq : ObjectActionPrereq
{
    failureConditionPrereq = 
    [
        {
            valueToCheck = 0
	        statPoolType = "QuickHackUpload"
	        comparisonType = "LessOrEqual"
        } : StatPoolPrereq
    ]	
   	failureExplanation = "LocKey#43809"
}

QuickHackActivePrereq : ObjectActionPrereq
{
    failureConditionPrereq = []	
   	failureExplanation = "LocKey#43808"
}

QuickHackTargetMortalPrereq : QuickHackActivePrereq
{
    failureConditionPrereq = 
    [
        { } : PuppetMortalPrereq
    ]

    failureExplanation = "LocKey#78386"
}

IsNotCyberpsycho : QuickHackActivePrereq
{
    failureConditionPrereq = 
    [
        { allowedTags = [ "Cyberpsycho" ] invert = true } : GameplayTagsPrereq
    ]

    failureExplanation = "LocKey#78386"
}

IsNotImmuneToSystemCollapse : QuickHackActivePrereq
{
    failureConditionPrereq = 
    [
        { statType = "SystemCollapseImmunity" valueToCheck = 0 comparisonType = "LessOrEqual" } : StatPrereq 
    ]

    failureExplanation = "LocKey#78386"
}

IsPlayerReachable : IPrereq
{
	prereqClassName = "IsPlayerReachablePrereq"
	bool invert = false
	bool checkForRMA = false;
}

IsPlayerNotReachableWhistleAction : IPrereq
{
	prereqClassName = "IsPlayerReachablePrereq"
	bool invert = false
	bool checkForRMA = true;
}

PlayerReachableActionPrereq : ObjectActionPrereq
{
    failureConditionPrereq = ["Prereqs.IsPlayerNotReachableWhistleAction"]	
   	failureExplanation = "LocKey#77351"
}

WeaponMalfunctionPrereq : QuickHackActivePrereq
{
    failureConditionPrereq = 
    [
        {
            invert = true
			CName tagToCheck = "JamWeapon"
			string checkType = "TAG"
        } : StatusEffectPrereq
    ]
}

GrenadeExplodePrereq : QuickHackActivePrereq
{
    failureConditionPrereq = 
    [
        {
            invert = true
			CName tagToCheck = "SuicideWithGrenade"
			string checkType = "TAG"
        } : StatusEffectPrereq
    ]
}

CanNpcUseGrenadesPrereq : QuickHackActivePrereq
{
    failureConditionPrereq = 
    [
        {
            prereqClassName = "gameMultiPrereq"
            aggregationType = "OR" 
            nestedPrereqs = 
            [
                { CName statType = "CanUseFragGrenades" comparisonType = "Greater" } : StatPrereq
                { CName statType = "CanUseFlashbangGrenades" comparisonType = "Greater" } : StatPrereq
                { CName statType = "CanUseBiohazardGrenades" comparisonType = "Greater" } : StatPrereq
                { CName statType = "CanUseEMPGrenades" comparisonType = "Greater" } : StatPrereq
                { CName statType = "CanUseIncendiaryGrenades" comparisonType = "Greater" } : StatPrereq
                { CName statType = "CanUseReconGrenades" comparisonType = "Greater" } : StatPrereq
                { CName statType = "CanUseCuttingGrenades" comparisonType = "Greater" } : StatPrereq
            ]
        }  : MultiPrereq
    ]

    failureExplanation = "LocKey#51710"
}

GunSuicidePrereq : QuickHackActivePrereq
{
    failureConditionPrereq = 
    [
        {
            invert = true
			CName tagToCheck = "SuicideWithWeapon"
			string checkType = "TAG"
        } : StatusEffectPrereq
    ]
}

QuickHackBlindPrereq : QuickHackActivePrereq
{
    failureConditionPrereq = 
    [
        {
            invert = true
			CName tagToCheck = "QuickHackBlind"
			string checkType = "TAG"
        } : StatusEffectPrereq
    ]
}

QuickHackBlindImmunePrereq : QuickHackActivePrereq
{
    failureConditionPrereq = 
    [
        { CName statType = "BlindImmunity" comparisonType = "Less" valueToCheck = 1 } : StatPrereq
    ]

    failureExplanation = "LocKey#78386"
}

QuickHackBurningImmunePrereq : QuickHackActivePrereq
{
    failureConditionPrereq = 
    [
        { CName statType = "BurningImmunity" comparisonType = "Less" valueToCheck = 1 } : StatPrereq
    ]

    failureExplanation = "LocKey#78386"
}

OverloadPrereq : QuickHackActivePrereq
{
    failureConditionPrereq = 
    [
        {
            invert = true
			CName tagToCheck = "Overload"
			string checkType = "TAG"
        } : StatusEffectPrereq
    ]
}

BrainMeltPrereq : QuickHackActivePrereq
{
    failureConditionPrereq = 
    [
        {
            invert = true
			CName tagToCheck = "BrainMelt"
			string checkType = "TAG"
        } : StatusEffectPrereq
    ]
}

MadnessPrereq : QuickHackActivePrereq
{
    failureConditionPrereq = 
    [
        {
            invert = true
			CName tagToCheck = "Madness"
			string checkType = "TAG"
        } : StatusEffectPrereq
    ]
}

CommsCallInPrereq : QuickHackActivePrereq
{
    failureConditionPrereq = 
    [
        {
            invert = true
			CName tagToCheck = "CommsCallIn"
			string checkType = "TAG"
        } : StatusEffectPrereq
    ]
}

WhistlePrereq : QuickHackActivePrereq
{
    failureConditionPrereq = 
    [
        {
            invert = true
			CName tagToCheck = "Whistle"
			string checkType = "TAG"
        } : StatusEffectPrereq
    ]
}

CommsNoisePrereq : QuickHackActivePrereq
{
    failureConditionPrereq = 
    [
        {
            invert = true
			CName tagToCheck = "CommsNoise"
			string checkType = "TAG"
        } : StatusEffectPrereq
    ]
}

CommsCallOutPrereq : QuickHackActivePrereq
{
    failureConditionPrereq = 
    [
        {
            invert = true
			CName tagToCheck = "CommsCallOut"
			string checkType = "TAG"
        } : StatusEffectPrereq
    ]
}

CyberwareMalfunctionPrereq : QuickHackActivePrereq
{
    failureConditionPrereq = 
    [
        {
            invert = true
			CName tagToCheck = "CyberwareMalfunction"
			string checkType = "TAG"
        } : StatusEffectPrereq
    ]
}

OverheatPrereq : QuickHackActivePrereq
{
    failureConditionPrereq = 
    [
        {
            invert = true
			CName tagToCheck = "Overheat"
			string checkType = "TAG"
        } : StatusEffectPrereq
    ]
}

SystemCollapsePrereq : QuickHackActivePrereq
{
    failureConditionPrereq = 
    [
        {
            invert = true
			CName tagToCheck = "SystemCollapse"
			string checkType = "TAG"
        } : StatusEffectPrereq
    ]
}

LocomotionMalfunctionPrereq : QuickHackActivePrereq
{
    failureConditionPrereq = 
    [
        {
            invert = true
			CName tagToCheck = "LocomotionMalfunction"
			string checkType = "TAG"
        } : StatusEffectPrereq
    ]
}

PingPrereq : QuickHackActivePrereq
{
    failureConditionPrereq = 
    [
        {
            invert = true
			CName tagToCheck = "Ping"
			string checkType = "TAG"
        } : StatusEffectPrereq
    ]
}

