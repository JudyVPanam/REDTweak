package Items
using RTDB, BaseStats, Prereqs, Effectors, Perks
// ========================
//        SANDEVISTAN
// ========================

[ notQueryable ]
SandevistanFragmentBase : Fragment
{
	quality = "Quality.Random"

	placementSlots = 
	[
		"AttachmentSlots.SandevistanSlot1",
		"AttachmentSlots.SandevistanSlot2",
		"AttachmentSlots.SandevistanSlot3",
	]

	buyPrice += ["Price.SandevistanFragment"]
	sellPrice += ["Price.SandevistanFragment"]

	statModifiers = 
	[
		{ statType = "BaseStats.Weight" 					modifierType = "Additive" 	value = 0.2f } : ConstantStatModifier,
	]
}

SandevistanFragment1 : SandevistanFragmentBase
{
	displayName = "LocKey#43415"
	iconPath = "cwf_sandevistan1"

	statModifiers += 
	[
		{ statType = "BaseStats.TimeDilationSandevistanDuration" 	modifierType = "Additive" value = 0.5f } : ConstantStatModifier,
		{ statType = "BaseStats.TimeDilationSandevistanDuration" 	modifierType = "Additive" 	refStat = "BaseStats.Quality" opSymbol = "*" value = 0.2f	} : CombinedStatModifier  
	]

	OnAttach = 
	[
		{
			UIData = 
			{
				localizedDescription = "LocKey#43416"
				stats = ["BaseStats.TimeDilationSandevistanDuration"]
			}
		}
	]

	fk< CraftingPackage > CraftingData = 
	{
		fk< RecipeElement >[] craftingRecipe += 
		[
			{ ingredient = "Items.UncommonMaterial1" amount = 50 },
			{ ingredient = "Items.RareMaterial1" amount = 50 },
			{ ingredient = "Items.EpicMaterial1" amount = 50 }
		]
	}
}

SandevistanFragment2 : SandevistanFragmentBase
{
	displayName = "LocKey#43417"
	quality = "Quality.Rare"
	iconPath = "cwf_sandevistan2"

	fk< GameplayLogicPackage >[] OnEquip = 
	[
		{ 
			fk< Effector >[] effectors = 
			[
				{ 
					prereqRecord = { string stateName = "Sandevistan" bool isInState = true } : Prereqs.TimeDilationPSMPrereq
					fk< StatModifierGroup > statGroup =
					{
						statModifiers =
						[
							{ statType = "BaseStats.CritChance" modifierType = "Additive" value = 5f } : ConstantStatModifier,
						]
					}
				} : ApplyStatGroupEffector
			]

			UIData = 
			{
				localizedDescription = "LocKey#43418"
				intValues = [5]
			}
		} : GameplayLogicPackage
	]

	fk< CraftingPackage > CraftingData = 
	{
		fk< RecipeElement >[] craftingRecipe += 
		[
			{ ingredient = "Items.UncommonMaterial1" amount = 50 },
			{ ingredient = "Items.RareMaterial1" amount = 50 },
			{ ingredient = "Items.EpicMaterial1" amount = 50 }
		]
	}
}

SandevistanFragment3 : SandevistanFragmentBase
{
	displayName = "LocKey#43419"
	quality = "Quality.Rare"
	iconPath = "cwf_sandevistan3"

	fk< GameplayLogicPackage >[] OnEquip = 
	[
		{ 
			fk< Effector >[] effectors = 
			[
				{ 
					prereqRecord = { string stateName = "Sandevistan" bool isInState = true } : Prereqs.TimeDilationPSMPrereq
					fk< StatModifierGroup > statGroup =
					{
						statModifiers =
						[
							{ statType = "BaseStats.CritDamage" modifierType = "Additive" value = 15f } : ConstantStatModifier,
						]
					}
				} : ApplyStatGroupEffector
			]

			UIData = 
			{
				localizedDescription = "LocKey#43420"
				intValues = [ 15 ]
			}
		} : GameplayLogicPackage
	]

	fk< CraftingPackage > CraftingData = 
	{
		fk< RecipeElement >[] craftingRecipe += 
		[
			{ ingredient = "Items.UncommonMaterial1" amount = 50 },
			{ ingredient = "Items.RareMaterial1" amount = 50 },
			{ ingredient = "Items.EpicMaterial1" amount = 50 }
		]
	}
}

SandevistanFragment4 : SandevistanFragmentBase
{
	displayName = "LocKey#43421"
	iconPath = "cwf_sandevistan4"

	statModifiers += 
	[
		{ statType = "BaseStats.TimeDilationSandevistanCooldownBase" modifierType = "Additive" value = -2f } : ConstantStatModifier,
		{ statType = "BaseStats.TimeDilationSandevistanCooldownBase" modifierType = "Additive" 	refStat = "BaseStats.Quality" opSymbol = "*" value = -0.5f	} : CombinedStatModifier  
	]

	fk< GameplayLogicPackage >[] OnAttach = 
	[
		{
			UIData = 
			{
				localizedDescription = "LocKey#43422"
				stats = ["BaseStats.TimeDilationSandevistanCooldownBase"]
			}
		}
	]

		fk< CraftingPackage > CraftingData = 
	{
		fk< RecipeElement >[] craftingRecipe += 
		[
			{ ingredient = "Items.UncommonMaterial1" amount = 50 },
			{ ingredient = "Items.RareMaterial1" amount = 50 },
			{ ingredient = "Items.EpicMaterial1" amount = 50 }
		]
	}
}

SandevistanFragment8 : SandevistanFragmentBase
{
	displayName = "LocKey#43429"
	iconPath = "cwf_sandevistan8"
	quality = "Quality.Legendary"
	OnEquip =
	[
		{ 
			fk< Effector >[] effectors = 
			[
				{
					prereqRecord = { string stateName = "Sandevistan" bool isInState = true } : Prereqs.TimeDilationPSMPrereq
					string[] effectTypes  = [ "Burning", "Poisoned", "Bleeding", "Electrocuted", "EMP" ]
				} : RemoveStatusEffectsEffector
			]

			UIData = 
			{
				localizedDescription = "LocKey#43430"
			}
		} : GameplayLogicPackage
	]

		fk< CraftingPackage > CraftingData = 
	{
		fk< RecipeElement >[] craftingRecipe += 
		[
			{ ingredient = "Items.UncommonMaterial1" amount = 50 },
			{ ingredient = "Items.RareMaterial1" amount = 50 },
			{ ingredient = "Items.EpicMaterial1" amount = 50 }
		]
	}
}

TygerClawsSandevistanFragment1 : SandevistanFragmentBase
{ 
	displayName = "LocKey#43433"
	quality = "Quality.Epic"
	iconPath = "cwf_tygerclawssandevistan"
	fk< GameplayLogicPackage >[] OnEquip = 
	[
		{ 
			effectors =
			[
				{
					prereqRecord = { string stateName = "Sandevistan" bool isInState = true } : Prereqs.TimeDilationPSMPrereq
					effectorToApply =
					{ 
						statPoolUpdates = [ { statPoolType = "BaseStatPools.Stamina" statPoolValue = 15f } : StatPoolUpdate ] 
					} : Items.RestoreStatPoolOnKillEffector
				} : ApplyEffectorOnPlayer
			]
			UIData = 
			{ 
				localizedDescription = "LocKey#43434"
				floatValues = [ 15 ] 
			}

		} : GameplayLogicPackage
	]

	fk< CraftingPackage > CraftingData = 
	{
		fk< RecipeElement >[] craftingRecipe += 
		[
			{ ingredient = "Items.UncommonMaterial1" amount = 40 },
			{ ingredient = "Items.RareMaterial1" amount = 40 },
			{ ingredient = "Items.EpicMaterial1" amount = 40 }
		]
	}
}

ValentinosSandevistanFragment1 : SandevistanFragmentBase
{ 
	displayName = "LocKey#43435"
	quality = "Quality.Epic"
	iconPath = "cwf_valentinosandevistan"
	fk< GameplayLogicPackage >[] OnEquip = 
	[
		{ 
			effectors =
			[
				{
					prereqRecord = { string stateName = "Sandevistan" bool isInState = true } : Prereqs.TimeDilationPSMPrereq
					effectorToApply =
					{ 
						statPoolUpdates = [ { statPoolType = "BaseStatPools.Health" statPoolValue = 5f } : StatPoolUpdate ] 
					} : Items.HealOnKillEffector
				} : ApplyEffectorOnPlayer
			]
			UIData = 
			{ 
				localizedDescription = "LocKey#43436"
				floatValues = [ 5 ] 
			}

		} : GameplayLogicPackage
	]

	fk< CraftingPackage > CraftingData = 
	{
		fk< RecipeElement >[] craftingRecipe += 
		[
			{ ingredient = "Items.UncommonMaterial1" amount = 40 },
			{ ingredient = "Items.RareMaterial1" amount = 40 },
			{ ingredient = "Items.EpicMaterial1" amount = 40 }
		]
	}
}

[ notQueryable ]
ArasakaSandevistanFragment1 : SandevistanFragmentBase
{ 
	displayName = "LocKey#43437"
	quality = "Quality.Legendary"
	iconPath = "cwf_arasakasandevistan"
	
	fk< GameplayLogicPackage >[] OnEquip = 
	[
		{ 
			fk< Effector >[] effectors = 
			[
				{ 
					prereqRecord = { string stateName = "Sandevistan" bool isInState = true } : Prereqs.TimeDilationPSMPrereq
					fk< StatModifierGroup > statGroup =
					{
						statModifiers =
						[
							{ statType = "BaseStats.Visibility" modifierType = "AdditiveMultiplier" value = -0.7f } : ConstantStatModifier,
						]
					}
				} : ApplyStatGroupEffector
			]

			UIData = 
			{
				localizedDescription = "LocKey#43438"
				intValues = [70]
			}
		} : GameplayLogicPackage
	]

	fk< CraftingPackage > CraftingData = 
	{
		fk< RecipeElement >[] craftingRecipe += 
		[
			{ ingredient = "Items.UncommonMaterial1" amount = 50 },
			{ ingredient = "Items.RareMaterial1" amount = 50 },
			{ ingredient = "Items.EpicMaterial1" amount = 50 }
		]
	}
}
