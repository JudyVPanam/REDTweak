package Items
using RTDB, Prereqs
// =========================
// 			BASES
// =========================
[ notQueryable ]
ProjectileLauncherPartBase : Fragment
{
	bool isSingleInstance = false
	bool isPart = true
	fk< ParentAttachmentType > parentAttachmentType = "ParentAttachmentType.Slot"
	iconPath = "launcher_rounds_explosive"

	buyPrice += ["Price.LauncherFragment"]
	sellPrice += ["Price.LauncherFragment"]
}

[ notQueryable ]
ProjectileLauncherRoot : ProjectileLauncherPartBase
{
	bool isCustomizable = true 
	
	CName[] tags += [ "parentPart", "SkipActivityLog" ]
	fk< AttachmentSlot >[] placementSlots = [ "AttachmentSlots.CyberwareRoot" ]

	string friendlyName = "ProjectileLauncherRoot"
	CName appearanceName = "unholstered_launcher"
	fk< ItemsFactoryAppearanceSuffixBase >[] appearanceSuffixes = []

    buyPrice = []
	sellPrice = []
}


// =========================
// 			ROUND
// =========================
[ notQueryable ]
ProjectileLauncherRoundBase : ProjectileLauncherPartBase
{
	displayName = "LocKey#51198"
	CName[] tags += [ "itemPart" ]
	quality = "Quality.Rare"
	visualTags = ["BaseRound"]
	fk< AttachmentSlot >[] placementSlots = [ "AttachmentSlots.ProjectileLauncherRound" ]

	CName projectileTemplateName 							= "launcher_round" // All launcher rounds use the projectileLauncherRound script controller
	CName appearanceResourceName 							= "a0_006_ma__launcher_fragment"

	float lifetime											= 5.f   // lifetime of a projectile after being detached = launched

	CName quickActionlaunchMode								= "Tracking"   // Regular, Tracking
	CName chargeActionlaunchMode							= "Tracking"
	CName launchTrajectory									= ""           // Linear, Curved, Parabolic

	float quickActionChargeCost 							= 25.f // full weapon charge = 100, each launch drains the charge pool. Reaching 0 adds cooldown and prevents using the launcher for its duration.
	float chargeActionChargeCost 							= 50.f

	bool canTargetDevices 									= false
	bool canTargetVehicles 									= false

	// General launch parameters
	float startVelocity										= 10.f
	float startVelocityCharged								= 10.f

	// Parabolic launch parameters
	Vector3 gravitySimulation 								= ( 0.0f, 0.0f, 0.0f )		
	float angle												= 45.

	// Curved launch parameters
	float linearTimeRatio 									= 0.9   // Time of movement after which projectile will follow the target with linear speed.
	float interpolationTimeRatio 							= 0.2	// Time of movement after which projectile will follow the target with accelerated/curved speed.
	float returnTimeMargin 									= 0.1	// Time after collision expressed in seconds after which projectile will move with inertial movement
	float bendTimeRatio 									= 0.4	// Time of movement before which projectile will follow the target with accelerated/curved speed in the opposite way.
	float bendFactor 										= 2.6	// The strength with which the projectile will bend opposite way
	float halfLeanAngle 									= 50.
	float endLeanAngle										= 0.
	float angleInterpolationDuration 						= 0.

	// Additive projectile spiraling parameters
	bool applyAdditiveProjectileSpiraling 					= false
	float rampUpDistanceStart 								= 0.f	// Distance from projectile launch at which to start ramping on spiral effect
	float rampUpDistanceEnd 								= 0.f   // Distance from projectile launch at which to finish ramping on spiral effect
	float rampDownDistanceStart 							= 0.f	// Distance from target to start ramping down spiral effect
	float rampDownDistanceEnd 								= 0.f	// Distance from target to finish ramping down spiral effect
	float rampDownFactor 									= 0.f	// How much to scale down the spiral after rampdown distance
	bool randomizePhase 									= false // Whether to apply a random phase offset to the cycle
	bool randomizeDirection 								= false // Randomizes between clockwise & counterclockwise directions

	//collision action
	CName collisionAction									= "Stop"    // Stop, Bounce, StopAndStick, StopAndStickPerpendicular, Pierce
	CName collisionActionCharged							= "Stop"    // ^ projectile col. action after being charge launched
	float detonationDelay									= -1        // // if value is >= 0 and projectile collided with static geo and collision action is either StopAndStick or StopAndStickPerpendicular, projectile will detonate with a delay

	int maxBounceCount						                = 0		// how many times the projectile can bounce if collision action is set to 'Bounce'
	float energyLossFactor 				   					= 0.5f	// Sets the energy loss factor on collision = how hard the projectile will bounce off surfaces
	float shootCollisionEnableDelay 		  		   		= -1.f 	// if value is >= 0, a collision will be enabled on the projectile, allowing it do detonate upon reiceiving damage
	bool canStopAndStickOnHardSurfaces     					= false
	bool hideMeshOnCollision				  		    	= false 
	bool despawnOnCollision					  		    	= true
	CName onCollisionSound									= ""
	float onCollisionStimBroadcastRadius 					= -1.f  // if this is > 0, the projectile will send out a stim on detonating
	float onCollisionStimBroadcastLifetime 					= -1.f  // if this is > 0, the projectile stimuli will be active for the set lifetime
	CName onCollisionStimType 						        = ""    // Explosion, ProjectileDistraction, SoundDistraction. The type of stim that will be sent out on detonating

	string attack 				   							= ""    // quick launch attack
	string secondaryAttack		   							= ""    // charge launch attack

	fk< CraftingPackage > CraftingData = 
	{
		fk< RecipeElement >[] craftingRecipe += 
		[
			{ ingredient = "Items.UncommonMaterial1" amount = 15 },
			{ ingredient = "Items.RareMaterial1" amount = 15 },
		]
	}
}

ExplosiveDamageRound : ProjectileLauncherRoundBase
{
	displayName = "LocKey#23231"
	visualTags = ["ExplosiveRound"]
	iconPath = "cwf_explosivedamageround"
	
	lifetime			             = 3.f

	startVelocity 		             = 80.f
	startVelocityCharged    		 = 110.f

	// Curved launch parameters
	linearTimeRatio 			 	 = 0.9
	interpolationTimeRatio     		 = 0.2
	returnTimeMargin 		   	     = 0.1
	bendTimeRatio 			 		 = 0.4
	bendFactor 			     		 = 2.6
	halfLeanAngle 		    		 = 50.
	endLeanAngle					 = 0.
	angleInterpolationDuration 		 = 0.

	collisionAction		   		     = "Stop"
	collisionActionCharged			 = "Stop"
	detonationDelay					 = 1.5f

	onCollisionStimBroadcastRadius	 = 15.f
	onCollisionStimType	   			 = "Explosion"

	attack 				  			 = "Attacks.MissileProjectile"		
	secondaryAttack		   			 = "Attacks.MissileProjectileCharged"		

	OnEquip =
	[
		{
			UIData =
			{
				localizedDescription = "LocKey#23236"
			}
		}
	]
}

ElectricDamageRound : ProjectileLauncherRoundBase
{
	displayName = "LocKey#23232"
	visualTags = ["ElectricRound"]
	iconPath = "cwf_electricdamageround"

	lifetime			             = 2.0f

	startVelocity 		             = 60.f
	startVelocityCharged    		 = 35.f
	
	collisionAction		   		     = "Stop"
	collisionActionCharged			 = "Bounce"
	maxBounceCount					 = 2
	energyLossFactor 				 = 0.15f

	onCollisionStimBroadcastRadius	 = 10.f
	onCollisionStimType	   			 = "Explosion"

	attack 				  			 = "Attacks.EMPProjectile"		
	secondaryAttack		   			 = "Attacks.EMPProjectileCharged"	

	OnEquip =
	[
		{
			UIData =
			{
				localizedDescription = "LocKey#23237"
			}
		}
	]
}

ThermalDamageRound : ProjectileLauncherRoundBase
{
	displayName = "LocKey#23233"
	visualTags = ["ThermalRound"]
	iconPath = "cwf_thermaldamageround"

	startVelocity 		             = 60.f
	startVelocityCharged    		 = 90.f

	onCollisionStimBroadcastRadius	 = 10.f
	onCollisionStimType	   			 = "Explosion"

	attack 				  			 = "Attacks.ThermalProjectile"		
	secondaryAttack		   			 = "Attacks.ThermalProjectileCharged"	

	OnAttach = 
	[ 
		{
			effectors =
	        [
	        	// regular shots have a 20% chance of applying Burning
	        	{
	                prereqRecord = 
	                {
	                	conditions =
	                	[
	                		{ hitFlag = "WeaponFullyCharged" invert = true } : HitFlagHitPrereqCondition
	                	]
	                } : Prereqs.ProcessHitTriggered
	                statusEffect = "BaseStatusEffect.Burning"
	                float applicationChance = 0.2f
	            } : AddStatusEffectToAttackEffector

	            // charged shots have a 50% chance of applying Burning
	            {
	                prereqRecord = 
	                {
	                	conditions =
	                	[
	                		{ hitFlag = "WeaponFullyCharged" } : HitFlagHitPrereqCondition
	                	]
	                } : Prereqs.ProcessHitTriggered
	                statusEffect = "BaseStatusEffect.Burning"
	                float applicationChance = 0.5f
	            } : AddStatusEffectToAttackEffector
	        ]
		}
	]

	OnEquip =
	[
		{
			UIData =
			{
				localizedDescription = "LocKey#23238"
			}
		}
	]
}

ChemicalDamageRound : ProjectileLauncherRoundBase
{
	displayName = "LocKey#23234"
	visualTags = ["ChemicalRound"]
	iconPath = "cwf_chemicaldamageround"

	startVelocity 		             = 60.f
	startVelocityCharged    		 = 90.f
	
	onCollisionStimBroadcastRadius	 = 10.f
	onCollisionStimType	   			 = "Explosion"

	attack 				  			 = "Attacks.ChemicalProjectile"		
	secondaryAttack		   			 = "Attacks.ChemicalProjectileCharged"	

	OnAttach = 
	[ 
		{
			effectors =
	        [
	        	// regular shots have a 20% chance of applying Poisoned
	        	{
	                prereqRecord = 
	                {
	                	conditions =
	                	[
	                		{ hitFlag = "WeaponFullyCharged" invert = true } : HitFlagHitPrereqCondition
	                	]
	                } : Prereqs.ProcessHitTriggered
	                statusEffect = "BaseStatusEffect.Poisoned"
	                float applicationChance = 0.2f
	            } : AddStatusEffectToAttackEffector

	            // charged shots have a 50% chance of applying Poisoned
	            {
	                prereqRecord = 
	                {
	                	conditions =
	                	[
	                		{ hitFlag = "WeaponFullyCharged" } : HitFlagHitPrereqCondition
	                	]
	                } : Prereqs.ProcessHitTriggered
	                statusEffect = "BaseStatusEffect.Poisoned"
	                float applicationChance = 0.5f
	            } : AddStatusEffectToAttackEffector
	        ]
		}
	]

	OnEquip =
	[
		{
			UIData =
			{
				localizedDescription = "LocKey#23239"
			}
		}
	]
}

TranquilizerRound : ProjectileLauncherRoundBase
{
	displayName = "LocKey#23235"
	visualTags = ["TranquilizerRound"]
	iconPath = "cwf_tranquilizerround"

	startVelocity 		             = 40.f
	startVelocityCharged    		 = 70.f
	quickActionChargeCost 	         = 33.4f

	collisionAction		   		     = "StopAndStickPerpendicular"
	collisionActionCharged			 = "StopAndStickPerpendicular"
	onCollisionStimBroadcastRadius	 = 5.f
	onCollisionStimType	   			 = "SoundDistraction"

	detonationDelay					 = 0.1f

	attack 				  			 = "Attacks.TranquilizerProjectile"		
	secondaryAttack		   			 = "Attacks.TranquilizerProjectileCharged"	

	OnEquip =
	[
		"EquipmentGLP.NonLethal"
		{
			UIData =
			{
				localizedDescription = "LocKey#23240"
			}
		}
	]
}

// =========================
// 			WIRING
// =========================
[ notQueryable ]
ProjectileLauncherWiringBase : ProjectileLauncherPartBase
{
	displayName = "LocKey#51200"
	CName[] tags += [ "itemPart" ]
	quality = "Quality.Epic"
	visualTags = ["default"]
	fk< AttachmentSlot >[] placementSlots = [ "AttachmentSlots.ProjectileLauncherWiring" ]
	
	CName appearanceResourceName = "projectile_launcher_wiring"
}

NeoplasticPlating : ProjectileLauncherWiringBase
{
	displayName = "LocKey#40391"
	localizedDescription = "LocKey#40392"
	visualTags = [ "DefaultWiring" ]
	quality = "Quality.Rare"
	iconPath = "cwf_neoplasticplatinground"

	OnEquip = 
	[
		{
			stats =
			[
				{ statType = "BaseStats.CritChance"		modifierType = "Additive"	value = 10f	} : ConstantStatModifier
			]
			UIData = 
			{
				localizedDescription = "LocKey#51202"
				intValues = [10]
			}
		}
	]

	fk< CraftingPackage > CraftingData = 
	{
		fk< RecipeElement >[] craftingRecipe += 
		[
			{ ingredient = "Items.CommonMaterial1" amount = 18 },
			{ ingredient = "Items.RareMaterial1" amount = 7 }
		]
	}
}

MetalPlating : ProjectileLauncherWiringBase
{
	displayName = "LocKey#52628"
	localizedDescription = "LocKey#52630"
	quality = "Quality.Rare"
	iconPath = "cwf_metalplatinground"
	
	OnEquip = 
	[
		{
			stats =
			[
				{ statType = "BaseStats.ThermalResistance"		modifierType = "Additive"	value = 10f	} : ConstantStatModifier
				{ statType = "BaseStats.ChemicalResistance"		modifierType = "Additive"	value = 10f	} : ConstantStatModifier
				{ statType = "BaseStats.ElectricResistance"		modifierType = "Additive"	value = 10f	} : ConstantStatModifier
			]
			UIData = 
			{
				localizedDescription = "LocKey#40798"
				intValues = [10]
			}
		}
	]

	fk< CraftingPackage > CraftingData = 
	{
		fk< RecipeElement >[] craftingRecipe += 
		[
			{ ingredient = "Items.CommonMaterial1" amount = 18 },
			{ ingredient = "Items.RareMaterial1" amount = 7 }
		]
	}
}

TitaniumPlating : ProjectileLauncherWiringBase
{
	displayName = "LocKey#52629"
	localizedDescription = "LocKey#52631"
	quality = "Quality.Epic"
	iconPath = "cwf_titaniumplatinground"

	OnEquip = 
	[
		{
			stats =
			[
				{ statType = "BaseStats.Armor"		modifierType = "AdditiveMultiplier"	value = 0.07f	} : ConstantStatModifier
			]
			UIData = 
			{
				localizedDescription = "LocKey#51205"
				intValues = [7]
			}
		}
	]

	fk< CraftingPackage > CraftingData = 
	{
		fk< RecipeElement >[] craftingRecipe += 
		[
			{ ingredient = "Items.RareMaterial1" amount = 15 },
			{ ingredient = "Items.EpicMaterial1" amount = 7 }
		]
	}
}
