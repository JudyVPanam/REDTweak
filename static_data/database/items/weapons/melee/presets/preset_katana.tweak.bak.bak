package Items
using RTDB, BaseStats, KatanaAttacks


//------------------------------------------------------------------------------//
//                              Default Weapons		                            //
//------------------------------------------------------------------------------//
Preset_Katana_Default : Base_Katana
{
	displayName = "LocKey#40183"
	localizedDescription = "LocKey#40184"
	CName[] visualTags = ["Default"]
	CName appearanceResourceName = "Preset_Katana_Default"
	fk< UIIcon > hudIcon = "HUDWeaponIcons.Katana"
	fk< SlotItemPartPreset >[] slotPartListPreset =
	[
		{ slot = "AttachmentSlots.Grip" 	itemPartPreset = "Items.w_katana_grip" },
	]
	fk< CraftingPackage > CraftingData = 
	{
		fk< RecipeElement >[] craftingRecipe = 
		[
			{ ingredient = "Items.RareMaterial1" amount = 30 }
		]
	}
}

[ notQueryable ]
Preset_Katana_Arasaka_2020 : Preset_Katana_Default
{
	CName[] visualTags = ["Arasaka_2020"]
	localizedDescription = "LocKey#40185"
	fk< UIIcon > hudIcon = "HUDWeaponIcons.Katana"
	fk< CraftingPackage > CraftingData = 
	{
		fk< RecipeElement >[] craftingRecipe = 
		[
			{ ingredient = "Items.UncommonMaterial1" amount = 15 },
			{ ingredient = "Items.RareMaterial1" amount = 25 }
		]
	}
}

Preset_Katana_Neon : Preset_Katana_Default
{
	CName[] visualTags = ["Neon"]
	localizedDescription = "LocKey#40186"
	fk< UIIcon > hudIcon = "HUDWeaponIcons.Katana"
	fk< CraftingPackage > CraftingData = 
	{
		fk< RecipeElement >[] craftingRecipe = 
		[
			{ ingredient = "Items.UncommonMaterial1" amount = 15 },
			{ ingredient = "Items.RareMaterial1" amount = 25 }
		]
	}
}

Preset_Katana_Military : Preset_Katana_Default
{
	CName[] visualTags = ["Military"]
	localizedDescription = "LocKey#40187"
	fk< UIIcon > hudIcon = "HUDWeaponIcons.Katana"
	fk< CraftingPackage > CraftingData = 
	{
		fk< RecipeElement >[] craftingRecipe = 
		[
			{ ingredient = "Items.RareMaterial1" amount = 30 },
			{ ingredient = "Items.EpicMaterial1" amount = 30 }
		]
	}

	fk< StatModifierGroup >[] statModifierGroups = 
	[
		"Items.Base_Katana_RPG_Stats",
		"Items.Base_Katana_RPG_Randomized_Stats"
		"Items.Base_Melee_Elemental_Damage_Type_Randomization",
		"Items.Base_Melee_Elemental_Damage_Type_Min_Max",
		"Items.Base_Katana_Handling_Stats",
		"Items.Base_Katana_Misc_Stats"
	]
}

[ notQueryable ]
Preset_Katana_Training : Preset_Katana_Default
{
	CName[] visualTags = ["Training"]
}

//------------------------------------------------------------------------------//
//                              	Crafted Weapons                             //
//------------------------------------------------------------------------------//

[ notQueryable ]
Craftable_Common_Katana : Preset_Katana_Default
{
	quality = "Quality.Common"

	CraftingData = "Items.Big_Common_Crafting_Materials_Weapon"
}

[ notQueryable ]
Craftable_Uncommon_Katana : Craftable_Common_Katana
{
	quality = "Quality.Uncommon"

	CraftingData = "Items.Big_Uncommon_Crafting_Materials_Weapon"
}

[ notQueryable ]
Craftable_Rare_Katana : Craftable_Common_Katana
{
	quality = "Quality.Rare"

	CraftingData = "Items.Big_Rare_Crafting_Materials_Weapon"
}

[ notQueryable ]
Craftable_Epic_Katana : Craftable_Common_Katana
{
	quality = "Quality.Epic"

	CraftingData = "Items.Big_Epic_Crafting_Materials_Weapon"

}

[ notQueryable ]
Proficiency_Craftable_Legendary_Katana : Craftable_Common_Katana
{
	quality = "Quality.Legendary"

	CraftingData = "Items.Big_Legendary_Crafting_Materials_Weapon"

}

[ notQueryable ]
Craftable_Common_Katana_Military : Preset_Katana_Military
{
	quality = "Quality.Common"

	CraftingData = "Items.Big_Common_Crafting_Materials_Weapon"

}

[ notQueryable ]
Craftable_Uncommon_Katana_Military : Craftable_Common_Katana_Military
{
	quality = "Quality.Uncommon"

	CraftingData = "Items.Big_Uncommon_Crafting_Materials_Weapon"

}

//Added _1 here to resolve conflixts with tweak HASHES, stupid workaround, but the only one I've got.
[ notQueryable ]
Craftable_Rare_Katana_Military_1 : Craftable_Common_Katana_Military
{
	quality = "Quality.Rare"

	CraftingData = "Items.Big_Rare_Crafting_Materials_Weapon"

}

[ notQueryable ]
Craftable_Epic_Katana_Military : Craftable_Common_Katana_Military
{
	quality = "Quality.Epic"

	CraftingData = "Items.Big_Epic_Crafting_Materials_Weapon"

}

[ notQueryable ]
Craftable_Legendary_Katana_Military : Craftable_Common_Katana_Military
{
	quality = "Quality.Legendary"

	CraftingData = "Items.Big_Legendary_Crafting_Materials_Weapon"

}


//------------------------------------------------------------------------------//
//                                  Unique Weapons                              //
//------------------------------------------------------------------------------//

[ notQueryable ]
Preset_Katana_GoG : Preset_Katana_Default
{
	displayName = "LocKey#76935"
	visualTags = ["Witcher_Katana"]
	quality = "Quality.Rare"
	localizedDescription = "LocKey#76936"

	OnLooted =
	[
		{
			items = 
			[
				{ item = "Items.Recipe_Preset_Katana_GoG_Epic" }
			]
		}
	]

	bool scaleToPlayer = true
}

[ notQueryable ]
Preset_Katana_GoG_Epic : Preset_Katana_GoG
{
	quality = "Quality.Epic"
	CraftingData =
	{
		craftingRecipe = 
		[       
			{ ingredient = "Items.Preset_Katana_GoG" amount = 1 }

			{ ingredient = "Items.UncommonMaterial1" amount = 25 },
			{ ingredient = "Items.RareMaterial1" amount = 40 },
			{ ingredient = "Items.EpicMaterial1" amount = 50 },
			{ ingredient = "Items.LegendaryMaterial1" amount = 2 },
		]
	}

	OnLooted =
	[
		{
			items = 
			[
				{ item = "Items.Recipe_Preset_Katana_GoG_Legendary" }
			]
		}
	]
}

[ notQueryable ]
Preset_Katana_GoG_Legendary : Preset_Katana_GoG
{
	quality = "Quality.Legendary"
	CraftingData =
	{
		craftingRecipe = 
		[       
			{ ingredient = "Items.Preset_Katana_GoG_Epic" amount = 1 }

			{ ingredient = "Items.RareMaterial1" amount = 25 },
			{ ingredient = "Items.EpicMaterial1" amount = 40 },
			{ ingredient = "Items.LegendaryMaterial1" amount = 3 },
		]
	}
}

[ notQueryable ]
Recipe_Preset_Katana_GoG_Epic : ItemRecipe
{
	displayName = ""
	craftingResult = { item = "Items.Preset_Katana_GoG_Epic" }
}

[ notQueryable ]
Recipe_Preset_Katana_GoG_Legendary : ItemRecipe
{
	displayName = ""
	craftingResult = { item = "Items.Preset_Katana_GoG_Legendary" }
}

//------------------------------------------------------------------------------//
//                                  Cocktail  		                            //
//------------------------------------------------------------------------------//
[ notQueryable ]
Preset_Katana_Cocktail : Preset_Katana_Default
{
	displayName = "LocKey#40591"
	visualTags = ["Cocktail_Stick"]
	quality = "Quality.Rare"
	statModifiers +=
	[
		"Quality.IconicItem"
	]
	localizedDescription = "LocKey#40592"

	npcRPGData = 
	{
		statModifiers +=
		[
			"Quality.IconicItem"
		]
	} : Items.Base_Katana_NPC_Data

	slotPartListPreset +=
	[
		{ slot = "AttachmentSlots.IconicMeleeWeaponMod1"		itemPartPreset = "Items.CocktailStickWeaponMod" },
	]

	statModifierGroups =
	[
		//From base_katana
		"Items.Base_Katana_RPG_Stats",
		"Items.Base_Katana_RPG_Randomized_Stats"
		"Items.Base_Katana_Handling_Stats",
		"Items.Base_Katana_Misc_Stats"
		//From base_melee
        "Items.Base_Melee_Status_Effect_Application_Stats"
		"Items.Base_Melee_Physical_Damage_Type_Min_Max"
		"Items.Base_Melee_Physical_Damage_Type_Randomization",
		"Proficiencies.KenjutsuWeaponPassives"
	]

	CraftingData = "Items.Iconic_Big_Rare_Crafting_Materials_Weapon"

	OnLooted =
	[
		{
			items = 
			[
				{ item = "Items.Recipe_Preset_Katana_Cocktail_Epic" }
			]
		}
	]
}

[ notQueryable ]
Preset_Katana_Cocktail_Epic : Preset_Katana_Cocktail
{
	quality = "Quality.Epic"
	CraftingData =
	{
		craftingRecipe = 
		[       
			{ ingredient = "Items.Preset_Katana_Cocktail" amount = 1 }

			{ ingredient = "Items.UncommonMaterial1" amount = 35 },
			{ ingredient = "Items.RareMaterial1" amount = 30 },
			{ ingredient = "Items.EpicMaterial1" amount = 35 },
			{ ingredient = "Items.LegendaryMaterial1" amount = 2 },
		]
	}

	OnLooted =
	[
		{
			items = 
			[
				{ item = "Items.Recipe_Preset_Katana_Cocktail_Legendary" }
			]
		}
	]
}

[ notQueryable ]
Preset_Katana_Cocktail_Legendary : Preset_Katana_Cocktail
{
	quality = "Quality.Legendary"
	CraftingData =
	{
		craftingRecipe = 
		[
			{ ingredient = "Items.Preset_Katana_Cocktail_Epic" amount = 1 }

			{ ingredient = "Items.RareMaterial1" amount = 45 },
			{ ingredient = "Items.EpicMaterial1" amount = 40 },
			{ ingredient = "Items.LegendaryMaterial1" amount = 10 },
		]
	}
}

[ notQueryable ]
Recipe_Preset_Katana_Cocktail_Epic : ItemRecipe
{
	displayName = ""
	craftingResult = { item = "Items.Preset_Katana_Cocktail_Epic" }
}

[ notQueryable ]
Recipe_Preset_Katana_Cocktail_Legendary : ItemRecipe
{
	displayName = ""
	craftingResult = { item = "Items.Preset_Katana_Cocktail_Legendary" }
}

//------------------------------------------------------------------------------//
//                                  Hiromi  		                            //
//------------------------------------------------------------------------------//

[ notQueryable ]
Preset_Katana_Hiromi : Preset_Katana_Default
{
	
	displayName = "LocKey#40597"
	visualTags = ["Hiromi_Katana"]
	quality = "Quality.Rare"
	statModifiers +=
	[
		"Quality.IconicItem"
	]
	localizedDescription = "LocKey#40593"

	npcRPGData = 
	{
		statModifiers +=
		[
			"Quality.IconicItem"
		]
	} : Items.Base_Katana_NPC_Data

	slotPartListPreset +=
	[
		{ slot = "AttachmentSlots.IconicMeleeWeaponMod1"		itemPartPreset = "Items.HiromiKatanaWeaponMod" },
	]

	statModifierGroups =
	[
		//From base_katana
		"Items.Base_Katana_RPG_Stats",
		"Items.Base_Katana_RPG_Randomized_Stats"
		"Items.Base_Katana_Handling_Stats",
		"Items.Base_Katana_Misc_Stats"
		//From base_melee
        "Items.Base_Melee_Status_Effect_Application_Stats"
		"Items.Base_Melee_Elemental_Damage_Type_Min_Max"
		"Items.Base_Melee_Weapon_Damage_Type_Electric",
		"Proficiencies.KenjutsuWeaponPassives"
	]

	CraftingData = "Items.Iconic_Big_Rare_Crafting_Materials_Weapon"

	OnLooted =
	[
		{
			items = 
			[
				{ item = "Items.Recipe_Preset_Katana_Hiromi_Epic" }
			]
		}
	]
}

[ notQueryable ]
Preset_Katana_Hiromi_Epic : Preset_Katana_Hiromi
{
	quality = "Quality.Epic"
	CraftingData =
	{
		craftingRecipe = 
		[       
			{ ingredient = "Items.Preset_Katana_Hiromi" amount = 1 }

			{ ingredient = "Items.UncommonMaterial1" amount = 35 },
			{ ingredient = "Items.RareMaterial1" amount = 30 },
			{ ingredient = "Items.EpicMaterial1" amount = 35 },
			{ ingredient = "Items.LegendaryMaterial1" amount = 2 },
		]
	}

	OnLooted =
	[
		{
			items = 
			[
				{ item = "Items.Recipe_Preset_Katana_Hiromi_Legendary" }
			]
		}
	]
}

[ notQueryable ]
Preset_Katana_Hiromi_Legendary : Preset_Katana_Hiromi
{
	quality = "Quality.Legendary"
	CraftingData =
	{
		craftingRecipe = 
		[       
			{ ingredient = "Items.Preset_Katana_Hiromi_Epic" amount = 1 }

			{ ingredient = "Items.RareMaterial1" amount = 45 },
			{ ingredient = "Items.EpicMaterial1" amount = 40 },
			{ ingredient = "Items.LegendaryMaterial1" amount = 10 },
		]
	}
}

[ notQueryable ]
Recipe_Preset_Katana_Hiromi_Epic : ItemRecipe
{
	displayName = ""
	craftingResult = { item = "Items.Preset_Katana_Hiromi_Epic" }
}

[ notQueryable ]
Recipe_Preset_Katana_Hiromi_Legendary : ItemRecipe
{
	displayName = ""
	craftingResult = { item = "Items.Preset_Katana_Hiromi_Legendary" }
}

//------------------------------------------------------------------------------//
//                                  Takemura  		                            //
//------------------------------------------------------------------------------//

[ notQueryable ]
Preset_Katana_Takemura : Preset_Katana_Default
{
	displayName = "LocKey#40598"
	visualTags = ["Takemura_Katana"]
	quality = "Quality.Epic"
	localizedDescription = "LocKey#40594"

	slotPartListPreset +=
	[
		{ slot = "AttachmentSlots.IconicMeleeWeaponMod1"		itemPartPreset = "Items.TakemuraKatanaWeaponMod" },
	]

	statModifiers += 
    [
		"Quality.IconicItem"
        // Modifier to remove level requirement for using this gun.
        { statType = "BaseStats.Level" modifierType = "Multiplier" value = 0} : ConstantStatModifier
    ]
    bool scaleToPlayer = true

	npcRPGData = 
	{
		statModifiers +=
		[
			"Quality.IconicItem"
		]
	} : Items.Base_Katana_NPC_Data

	statModifierGroups =
	[
		//From base_katana
		"Items.Base_Katana_RPG_Stats",
		"Items.Base_Katana_RPG_Randomized_Stats"
		"Items.Base_Katana_Handling_Stats",
		"Items.Base_Katana_Misc_Stats"
		//From base_melee
        "Items.Base_Melee_Status_Effect_Application_Stats"
		"Items.Base_Melee_Physical_Damage_Type_Min_Max"
		"Items.Base_Melee_Physical_Damage_Type_Randomization",
		"Proficiencies.KenjutsuWeaponPassives"
	]

	CraftingData = "Items.Iconic_Big_Epic_Crafting_Materials_Weapon"

	OnLooted =
	[
		{
			items = 
			[
				{ item = "Items.Recipe_Preset_Katana_Takemura_Legendary" }
			]
		}
	]
}

[ notQueryable ]
Preset_Katana_Takemura_Legendary : Preset_Katana_Takemura
{
	quality = "Quality.Legendary"
	CraftingData =
	{
		craftingRecipe = 
		[       
			{ ingredient = "Items.Preset_Katana_Takemura" amount = 1 }

			{ ingredient = "Items.RareMaterial1" amount = 45 },
			{ ingredient = "Items.EpicMaterial1" amount = 40 },
			{ ingredient = "Items.LegendaryMaterial1" amount = 10 },
		]
	}
}

[ notQueryable ]
Recipe_Preset_Katana_Takemura_Legendary : ItemRecipe
{
	displayName = ""
	craftingResult = { item = "Items.Preset_Katana_Takemura_Legendary" }
}

//------------------------------------------------------------------------------//
//                                  Saburo  		                            //
//------------------------------------------------------------------------------//

[ notQueryable ]
Preset_Katana_Saburo : Preset_Katana_Default
{
	displayName = "LocKey#40599"
	visualTags = ["Saburo_Katana"]
	quality = "Quality.Rare"
	statModifiers +=
	[
		"Quality.IconicItem"
	]
	localizedDescription = "LocKey#40595"

	npcRPGData = 
	{
		statModifiers +=
		[
			"Quality.IconicItem"
		]
	} : Items.Base_Katana_NPC_Data

	slotPartListPreset +=
	[
		{ slot = "AttachmentSlots.IconicMeleeWeaponMod1"		itemPartPreset = "Items.SaburoKatanaWeaponMod" },
	]

	statModifierGroups =
	[
		//From base_katana
		"Items.Base_Katana_RPG_Stats",
		"Items.Base_Katana_RPG_Randomized_Stats"
		"Items.Base_Katana_Handling_Stats",
		"Items.Base_Katana_Misc_Stats"
		//From base_melee
        "Items.Base_Melee_Status_Effect_Application_Stats"
		"Items.Base_Melee_Physical_Damage_Type_Min_Max"
		"Items.Base_Melee_Physical_Damage_Type_Randomization",
		"Proficiencies.KenjutsuWeaponPassives"

	]

	CraftingData = "Items.Iconic_Big_Rare_Crafting_Materials_Weapon"

	OnLooted =
	[
		{
			items = 
			[
				{ item = "Items.Recipe_Preset_Katana_Saburo_Epic" }
			]
		}
	]
}

[ notQueryable ]
Preset_Katana_Saburo_Epic : Preset_Katana_Saburo
{
	quality = "Quality.Epic"
	CraftingData =
	{
		craftingRecipe = 
		[       
			{ ingredient = "Items.Preset_Katana_Saburo" amount = 1 }

			{ ingredient = "Items.UncommonMaterial1" amount = 35 },
			{ ingredient = "Items.RareMaterial1" amount = 30 },
			{ ingredient = "Items.EpicMaterial1" amount = 35 },
			{ ingredient = "Items.LegendaryMaterial1" amount = 2 },
		]
	}

	OnLooted =
	[
		{
			items = 
			[
				{ item = "Items.Recipe_Preset_Katana_Saburo_Legendary" }
			]
		}
	]
}

[ notQueryable ]
Preset_Katana_Saburo_Legendary : Preset_Katana_Saburo
{
	quality = "Quality.Legendary"
	CraftingData =
	{
		craftingRecipe = 
		[       
			{ ingredient = "Items.Preset_Katana_Saburo_Epic" amount = 1 }

			{ ingredient = "Items.RareMaterial1" amount = 45 },
			{ ingredient = "Items.EpicMaterial1" amount = 40 },
			{ ingredient = "Items.LegendaryMaterial1" amount = 10 },
		]
	}
}

[ notQueryable ]
Recipe_Preset_Katana_Saburo_Epic : ItemRecipe
{
	displayName = ""
	craftingResult = { item = "Items.Preset_Katana_Saburo_Epic" }
}

[ notQueryable ]
Recipe_Preset_Katana_Saburo_Legendary : ItemRecipe
{
	displayName = ""
	craftingResult = { item = "Items.Preset_Katana_Saburo_Legendary" }
}


//------------------------------------------------------------------------------//
//                                  Surgeon  		                            //
//------------------------------------------------------------------------------//

[ notQueryable ]
Preset_Katana_Surgeon : Preset_Katana_Default
{
	
	displayName = "LocKey#40600"
	visualTags = ["Surgeon_Katana"]
	quality = "Quality.Rare"
	statModifiers +=
	[
		"Quality.IconicItem"
	]
	localizedDescription = "LocKey#40596"

	npcRPGData = 
	{
		statModifiers +=
		[
			"Quality.IconicItem"
		]
	} : Items.Base_Katana_NPC_Data
	
	bool scaleToPlayer = true

	slotPartListPreset +=
	[
		{ slot = "AttachmentSlots.IconicMeleeWeaponMod1"		itemPartPreset = "Items.SurgeonKatanaWeaponMod" },
	]

	statModifierGroups =
	[
		//From base_katana
		"Items.Base_Katana_RPG_Stats",
		"Items.Base_Katana_RPG_Randomized_Stats"
		"Items.Base_Katana_Handling_Stats",
		"Items.Base_Katana_Misc_Stats"
		//From base_melee
        "Items.Base_Melee_Status_Effect_Application_Stats"
		"Items.Base_Melee_Elemental_Damage_Type_Min_Max"
		"Items.Base_Melee_Weapon_Damage_Type_Electric",
		"Proficiencies.KenjutsuWeaponPassives"
	]

	CraftingData = "Items.Iconic_Big_Rare_Crafting_Materials_Weapon"

	OnLooted =
	[
		{
			items = 
			[
				{ item = "Items.Recipe_Preset_Katana_Surgeon_Epic" }
			]
		}
	]
}

[ notQueryable ]
Preset_Katana_Surgeon_Epic : Preset_Katana_Surgeon
{
	quality = "Quality.Epic"
	CraftingData =
	{
		craftingRecipe = 
		[       
			{ ingredient = "Items.Preset_Katana_Surgeon" amount = 1 }

			{ ingredient = "Items.UncommonMaterial1" amount = 35 },
			{ ingredient = "Items.RareMaterial1" amount = 30 },
			{ ingredient = "Items.EpicMaterial1" amount = 35 },
			{ ingredient = "Items.LegendaryMaterial1" amount = 2 },
		]
	}

	OnLooted =
	[
		{
			items = 
			[
				{ item = "Items.Recipe_Preset_Katana_Surgeon_Legendary" }
			]
		}
	]
}

[ notQueryable ]
Preset_Katana_Surgeon_Legendary : Preset_Katana_Surgeon
{
	quality = "Quality.Legendary"
	CraftingData =
	{
		craftingRecipe = 
		[       
			{ ingredient = "Items.Preset_Katana_Surgeon_Epic" amount = 1 }

			{ ingredient = "Items.RareMaterial1" amount = 45 },
			{ ingredient = "Items.EpicMaterial1" amount = 40 },
			{ ingredient = "Items.LegendaryMaterial1" amount = 10 },
		]
	}
}

[ notQueryable ]
Recipe_Preset_Katana_Surgeon_Epic : ItemRecipe
{
	displayName = ""
	craftingResult = { item = "Items.Preset_Katana_Surgeon_Epic" }
}

[ notQueryable ]
Recipe_Preset_Katana_Surgeon_Legendary : ItemRecipe
{
	displayName = ""
	craftingResult = { item = "Items.Preset_Katana_Surgeon_Legendary" }
}
