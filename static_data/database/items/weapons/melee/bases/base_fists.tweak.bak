package Items
using RTDB, BaseStats, BaseStatPools, FistsAttacks, WeaponFxPackage

[ notQueryable ]
Base_Fists : MeleeWeapon
{
	fk< ItemType > itemType = "ItemType.Wea_Fists"
	iconPath = "fist_radial_short"
	evolution = "WeaponEvolution.Blunt"
	

	CName dropObject = ""

	tags += ["SkipActivityLog"]

	string friendlyName = "w_melee_fists"


	fk< EquipmentArea > equipArea = "EquipmentArea.BaseFists"
	
	fk< Quality > quality = "Quality.Common"
	
	fk< WeaponFxPackage > fxPackage = "WeaponFxPackage.FistFxPackage"

	fk< SlotItemPartListElement >[] slotPartList = 
	[
		{ slot = "AttachmentSlots.Grip" 	fk< ItemPartListElement >[] itemPartList = [ 
		{ item = "Items.w_knuckles_grip" }
		 ] }
	]

	fk< StatModifierGroup >[] statModifierGroups += 
	[
		"Items.Base_Fists_RPG_Stats",
		"Items.Base_Fists_Handling_Stats",
		"Items.Base_Fists_Misc_Stats"
		"Proficiencies.BrawlingWeaponPassives"
	]

	fk< Attack >[] attacks += [ 
				{ string[] hitFlags = [ "Nonlethal" ] } : FistsComboAttack1
				{ string[] hitFlags = [ "Nonlethal" ] } : FistsComboAttack2
				{ string[] hitFlags = [ "Nonlethal" ] } : FistsComboAttack3
				{ string[] hitFlags = [ "Nonlethal" ] } : FistsComboAttack4
				{ string[] hitFlags = [ "Nonlethal" ] } : FistsComboAttack5
				{ string[] hitFlags = [ "Nonlethal" ] } : FistsFinalAttack1
				{ string[] hitFlags = [ "Nonlethal" ] } : FistsFinalAttack2
				{ string[] hitFlags = [ "Nonlethal" ] } : FistsFinalAttack3
				{ string[] hitFlags = [ "Nonlethal" ] } : FistsFinalAttack4
				{ string[] hitFlags = [ "Nonlethal" ] } : FistsFinalAttack5
				{ string[] hitFlags = [ "Nonlethal" ] } : FistsFinalAttack6
				{ string[] hitFlags = [ "Nonlethal" ] } : FistsStrongAttack1
				{ string[] hitFlags = [ "Nonlethal" ] } : FistsStrongAttack2
				{ string[] hitFlags = [ "Nonlethal" ] } : FistsStrongAttack3
				{ string[] hitFlags = [ "Nonlethal" ] } : FistsBlockAttack
				{ string[] hitFlags = [ "Nonlethal" ] } : FistsJumpAttack
				{ string[] hitFlags = [ "Nonlethal" ] } : FistsSprintAttack
				{ string[] hitFlags = [ "Nonlethal" ] } : FistsCrouchAttack
				{ string[] hitFlags = [ "Nonlethal" ] } : FistsDeflectAttack
				"NPCAttacks.GrabAttack"
				"NPCAttacks.SpecialGrabAttackNetwatcher"
	]
	
	audioWeaponConfiguration = "audio_melee_metadata_fists_normal"

	enableNpcRPGData = true

	npcRPGData =
	{
		statModifierGroups +=
		[
		]
	} : Base_Fists_NPC_Data
	
	
	float weaponNearPlane			 = 12.88f
	float weaponFarPlane			 = 50.0f
	float weaponEdgesSharpness		 = 0.0f
	float weaponVignetteIntensity	 = 0.0f
	float weaponVignetteRadius		 = 0.0f
	float weaponVignetteCircular	 = 0.0f
	float weaponBlurIntensity		 = 1.0f
}

Base_Fists_RPG_Stats : StatModifierGroup
{
	fk< StatModifier >[] statModifiers = 
	[
		{ statType = "BaseStats.CanWeaponDeflect" 		modifierType = "Additive" 			value = 1 } : ConstantStatModifier,
		{ statType = "BaseStats.BlockFactor" 	        modifierType = "Additive"           value = 7f	} : ConstantStatModifier
	]	
}

Base_Fists_Handling_Stats : StatModifierGroup
{
	fk< StatModifier >[] statModifiers = 
	[
		{ statType = "BaseStats.Range"						modifierType = "Additive"				value = 1.5f } : ConstantStatModifier
		{ statType = "BaseStats.HitReactionFactor" 			modifierType = "Multiplier"				value = 1.f } : ConstantStatModifier
		{ statType = "BaseStats.HitWoundsFactor" 			modifierType = "Multiplier" 			value = 0.f	} : ConstantStatModifier
		{ statType = "BaseStats.HitDismembermentFactor"		modifierType = "Multiplier" 			value = 0.f	} : ConstantStatModifier
		{ statType = "BaseStats.AttacksNumber"				modifierType = "Additive"				value = 3 } : ConstantStatModifier
		{ statType = "BaseStats.BlockFactor" 	       		modifierType = "Additive"           	value = 1.f	} : ConstantStatModifier
		{ statType = "BaseStats.AttacksNumber"				modifierType = "Additive"				id = "brawling_passives" column = "to_fists_attack_number"	refStat = "BaseStats.Brawling" refObject = "Player" } : CurveStatModifier
		"Proficiencies.AttackSpeedBrawlingBonus"
		{ statType = "BaseStats.MeleeAttackDuration"		modifierType = "Additive"				value = 0.4f } : ConstantStatModifier
		
		{ float value = 10.f 	} 	: KnockdownImpulseModifier
		{ float value = 6.f 	} 	: PhysicalImpulseModifier
	]
}

Base_Fists_Misc_Stats : StatModifierGroup
{
	fk< StatModifier >[] statModifiers = 
	[
		{ statType = "BaseStats.CanWeaponDash"	        	modifierType = "Additive"				value = 1 } : ConstantStatModifier
		{ statType = "BaseStats.CanWeaponInfinitlyCombo"	modifierType = "Additive"				value = 1 } : ConstantStatModifier
		"BaseStats.EquipDurationModifier",
		"BaseStats.UnequipDurationModifier",
		{ statType = "BaseStats.ZoomLevel"					modifierType = "Additive"			value = 0.0f	} : ConstantStatModifier,
		{ statType = "BaseStats.EquipDuration_First"		modifierType = "Additive"			value = 0.5f	} : ConstantStatModifier,
	]
}

Base_Fists_NPC_Stats : StatModifierGroup
{
	statModifiers = 
	[
		{ statType = "BaseStats.Range"					modifierType = "Additive"	value = 1.0f } : ConstantStatModifier
		{ value = 6.f 	} 	: PhysicalImpulseModifier
		{ statType = "BaseStats.StaminaCostToBlock" 			modifierType = "Additive"	value = 20.0f	} : ConstantStatModifier

		{ value = 0.0f 		} : EquipItemTime_GangModifier
		{ value = 0.533f 	} : EquipAnimationDuration_GangModifier
		{ value = 0.9f 		} : EquipActionDuration_GangModifier
		{ value = 0.0f 		} : UnequipItemTime_GangModifier
		{ value = 0.467f 	} : UnequipAnimationDuration_GangModifier
		{ value = 1.033f 	} : UnequipDuration_GangModifier

		{ value = 0.0f 		} : EquipItemTime_CorpoModifier
		{ value = 0.533f 	} : EquipAnimationDuration_CorpoModifier
		{ value = 0.09 		} : EquipActionDuration_CorpoModifier
		{ value = 0.0f 		} : UnequipItemTime_CorpoModifier
		{ value = 0.467f 	} : UnequipAnimationDuration_CorpoModifier
		{ value = 1.033f 	} : UnequipDuration_CorpoModifier
	]
}

Base_Fists_NPC_Data : Base_NPC_RPG_Data
{
	statModifierGroups +=
	[
		"Items.Base_Fists_NPC_Stats"
	]
}


w_knuckles_grip : ItemWeaponRoot<Item>
{
	string friendlyName = "w_knuckles_grip"
	CName appearanceResourceName = "w_knuckles_grip"
}