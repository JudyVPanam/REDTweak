package Items
using RTDB, BaseStats, Prereqs, Perks

//------------------------------------------------------------------------------

[ notQueryable ]
Base_Defender : Base_Power_Lightmachinegun
{
    buyPrice += ["Price.ConstitutionalArms", "Price.MediumManufactureQuality"]
    sellPrice += ["Price.ConstitutionalArms", "Price.MediumManufactureQuality"]

	displayName = "LocKey#836"
	localizedDescription= "LocKey#837"
	fk< UIIcon > hudIcon = "HUDWeaponIcons.Defender"
	string friendlyName = "w_lmg_constitutional_defender"

	fk< SlotItemPartListElement >[] slotPartList = 
	[
		{ slot = "AttachmentSlots.Receiver" 		fk< ItemPartListElement >[] itemPartList = [ { item = "Items.w_lmg__constitutional_defender__rcv1" } ] },
		{ slot = "AttachmentSlots.Barrel" 			fk< ItemPartListElement >[] itemPartList = [ { item = "Items.w_lmg__constitutional_defender__base1"}  ] },
		{ slot = "AttachmentSlots.Magazine" 		fk< ItemPartListElement >[] itemPartList = [ { item = "Items.w_lmg__constitutional_defender__mag_std" }] },
		{ slot = "AttachmentSlots.MagazineEmpty" 	fk< ItemPartListElement >[] itemPartList = [ { item = "Items.w_lmg__constitutional_defender__mag_stdr" }] },
		"Items.RiflePossibleScopesList"
		{ slot = "AttachmentSlots.GenericWeaponMod1" } : GenericPowerModList
		{ slot = "AttachmentSlots.GenericWeaponMod2" } : GenericPowerModList
		{ slot = "AttachmentSlots.GenericWeaponMod3" } : GenericPowerModList
		{ slot = "AttachmentSlots.GenericWeaponMod4" } : GenericPowerModList
		{ slot = "AttachmentSlots.PowerWeaponModRare" } : RareDedicatedPowerModList
        { slot = "AttachmentSlots.PowerWeaponModEpic" } : EpicDedicatedPowerModList
        { slot = "AttachmentSlots.PowerWeaponModLegendary" } : LegendaryDedicatedPowerModList
	]

	CName audioName = "wea_set_defender"
	CName[] tags += [ "Preload", "Weapon", "LMG" ]
	fk< TriggerMode >[] triggerModes = [ "TriggerMode.FullAuto" ]
	fk< TriggerMode > primaryTriggerMode = "TriggerMode.FullAuto"
	fk< ItemCategory > itemCategory = "ItemCategory.Weapon"
	fk< ItemType > itemType = "ItemType.Wea_LightMachineGun"
	fk< Item > ammo = "Ammo.RifleAmmo"

	fk< Crosshair > crosshair = "Crosshairs.Power_Defender"
	
	fk< StatModifier >[] statModifiers += [
		"BaseStats.EquipDurationModifier",
		"BaseStats.UnequipDurationModifier",
		"BaseStats.SlideWhenLeaningOutOfCoverModifier",
		"BaseStats.BlockLocomotionWhenLeaningOutOfCoverModifier",
	]

	fk< StatPool >[] statPools =
	[
		"BaseStatPools.WeaponOverheat"
	]
	
	enableNpcRPGData = true

	npcRPGData =
	{
		statModifierGroups +=
		[
			"Items.Base_Defender_NPC_Technical_Stats"
		]
	} : Base_Power_Lightmachinegun_NPC_Data

	statModifierGroups +=
	[
		"Items.Base_Defender_Technical_Stats"
		"Items.Base_Defender_Handling_Stats"
		"Items.Base_Defender_Recoil_Stats"
		"Items.Base_Defender_Spread_Stats"
		"Items.Base_Defender_Aim_Stats"
		"Items.Base_Defender_Constant_Stats"
	]
	
	fk< WeaponFxPackage > fxPackage = "WeaponFxPackage.PowerLargeCaliber"
	
	/////////BlurControl/////////
	float weaponNearPlane			 = 0.27f
	float weaponFarPlane			 = 0.6f
	float weaponEdgesSharpness		 = 0.4958f
	float weaponVignetteIntensity	 = 0.1357f
	float weaponVignetteRadius		 = 0.6482f
	float weaponVignetteCircular	 = 0.0f
	float weaponBlurIntensity		 = 1.0f

	/////////BlurAimingControl/////////
	float weaponNearPlane_aim			 	= 0.19f
	float weaponFarPlane_aim			 	= 0.5f
	float weaponEdgesSharpness_aim		 	= 0.4958f
	float weaponVignetteIntensity_aim	 	= 0.1357f
	float weaponVignetteRadius_aim			= 0.6814f
	float weaponVignetteCircular_aim 		= 0.1745f
	float weaponBlurIntensity_aim		 	= 1.f
	////////////////////////////////////////////////////

	float baseReloadTime = 3.8f
	float baseEmptyReloadTime = 5.3f

	float uninterruptibleReloadStart = 2.9f
	float uninterruptibleEmptyReloadStart = 2.9f

	OnAttach += [ "Items.DefenderCrouchPackage", "EquipmentGLP.BorgWeaponHeavy"]
}

DefenderCrouchPackage : GameplayLogicPackage
{
	stackable = false
	effectors =
	[
		{
			prereqRecord = "Perks.IsPlayerCrouching"
			statGroup = 	
			{
				statModifiers = 
				[
					{ statType = "BaseStats.RecoilKickMin" modifierType = "Multiplier" value = 0.9f	} : ConstantStatModifier
					{ statType = "BaseStats.RecoilKickMax" modifierType = "Multiplier" value = 0.9f	} : ConstantStatModifier
					{ statType = "BaseStats.SwayTraversalTime" modifierType = "Multiplier" value = 3.0f } : ConstantStatModifier

				]
			} : StatModifierGroup
		} : ApplyStatGroupOnWeapon
		{
			prereqRecord = "Prereqs.UsingCoverPSMPrereq"
			statGroup = 	
			{
				statModifiers = 
				[
					{ statType = "BaseStats.RecoilKickMin" modifierType = "Multiplier" value = 0.9f	} : ConstantStatModifier
					{ statType = "BaseStats.RecoilKickMax" modifierType = "Multiplier" value = 0.9f	} : ConstantStatModifier
					{ statType = "BaseStats.RecoilScaleMax" modifierType = "Multiplier" value = 0.25f} : ConstantStatModifier
					{ statType = "BaseStats.SwaySideMaximumAngleDistance" modifierType = "Multiplier" value = 0.5f} : ConstantStatModifier
					{ statType = "BaseStats.SwaySideMaximumAngleDistance" modifierType = "Multiplier" value = 0.5f} : ConstantStatModifier
					{ statType = "BaseStats.SwayTraversalTime" modifierType = "Multiplier" value = 2.0f } : ConstantStatModifier
				]
			} : StatModifierGroup
		} : ApplyStatGroupOnWeapon
	]
}

//------------------------------------------------------------------------------
//-------------------------------- STAT GROUPS ---------------------------------
//------------------------------------------------------------------------------

Base_Defender_Technical_Stats : StatModifierGroup
{
	statModifiers =
	[
		{ value = 1.f 		} : NumShotsToFireModifier
		{ value = 0.0909f 	} : CycleTimeModifier
		{ value = 100.f 	} : MagazineCapacityModifier
		{ value = 1.f 		} : ProjectilesPerShotModifier
		"BaseStats.ProjectilesPerShotCombinedModifier"
		{ value = 100.f 	} : OverheatModifier
		{ statType = "BaseStats.Strength"					modifierType = "Additive"	value = 6.f } : ConstantStatModifier
	]
}

Base_Defender_NPC_Technical_Stats : StatModifierGroup
{
	statModifiers =
	[
		{ value = 1.f 		} : NumShotsToFireModifier
		{ value = 0.045f 	} : CycleTimeModifier
		{ value = 65.f 	} : MagazineCapacityModifier
		{ value = 1.f 		} : ProjectilesPerShotModifier
		"BaseStats.ProjectilesPerShotCombinedModifier"
		{ value = 100.f 	} : OverheatModifier
	]
}

Base_Defender_Handling_Stats : StatModifierGroup
{
	statModifiers =
	[
		{ value = 3.8f 		} : ReloadTimeModifier
		{ value = 5.3f 		} : EmptyReloadTimeModifier
		{ value = 1.2f 		} : ZoomLevelModifier
		{ value = 3.5f 		} : EquipDuration_FirstModifier
	]
}

Base_Defender_Recoil_Stats : StatModifierGroup
{
	statModifiers =
	[
		{ value = -35.f 	} : RecoilDirModifier
		{ value = 7.5f 		} : RecoilAngleModifier
		{ value = 0.1f 		} : RecoilHoldDurationModifier
		{ value = 0.017f 	} : RecoilTimeModifier
		{ value = 0.5f 		} : RecoilRecoveryTimeModifier
		{ value = 1.f 		} : RecoilEnableLinearXModifier
		{ value = 1.f 		} : RecoilEnableLinearYModifier
		{ value = 1.f 		} : RecoilEnableLinearXADSModifier
		{ value = 1.f 		} : RecoilEnableLinearYADSModifier	
		{ value = 1.f 		} : RecoilEnableScaleYModifier //Enables scaling recoil in Y direction
		{ value = 1.f 		} : RecoilEnableScaleXModifier //Enables scaling recoil in X direction
		{ value = 1.5f 		} : RecoilScaleTimeModifier // How long does it take for recoil to scale up. Starts scaling immediately on firing
		{ value = 2.0f 		} : RecoilScaleMaxModifier // How much higher is the recoil at max scale
		{ value = 0.1f 		} : PreFireTimeModifier
		{ value = 35.0f 	} : RecoilAlternateDirModifier // alternate direction
		{ value = 1.0f 		} : RecoilDirPlanCycleRandRangeDirModifier //when do you switch directions?
		{ value = 0.5f 		} : LinearDirectionUpdateMinModifier //minimum time range to potentially make the switch between directions. Consider rate of fire and magazine size
		{ value = 0.7f 		} : LinearDirectionUpdateMaxModifier //maximum time range to potentially make the switch between directions. Consider rate of fire and magazine size

	]
}

Base_Defender_Spread_Stats : StatModifierGroup
{
	statModifiers =
	[
		{ value = 0.2f		} : SpreadMinXModifier
		{ value = 0.125f 	} : SpreadMinYModifier
		{ value = 0.4f 		} : SpreadAdsDefaultXModifier
		{ value = 0.25f 	} : SpreadAdsDefaultYModifier
		{ value = 0.f 		} : SpreadAdsMinXModifier
		{ value = 0.f 		} : SpreadAdsMinYModifier
		{ value = 1.f 		} : SpreadUseInAdsModifier
		{ value = 4.f 		} : SpreadResetSpeedModifier
		{ value = 0.1f 		} : SpreadResetTimeThresholdModifier
		{ value = 0.f 		} : SpreadRandomizeOriginPointModifier
		{ value = 1.f 		} : SpreadUseCircularSpreadModifier
	]
}

Base_Defender_Aim_Stats : StatModifierGroup
{
	statModifiers =
	[
		{ statType = "BaseStats.AimFOV" 		modifierType = "Additive" 	value = 20		} : ConstantStatModifier // FOV
		{ statType = "BaseStats.AimOffset" 		modifierType = "Additive" 	value = 0.02f	} : ConstantStatModifier // OFFSET
	]
}

Base_Defender_Constant_Stats : StatModifierGroup
{
	statModifiers =
	[
		{ statType = "BaseStats.SpreadDefaultX" 			modifierType = "Additive" 	value = 4.f 	} : ConstantStatModifier
		{ statType = "BaseStats.SpreadAdsDefaultX" 			modifierType = "Additive" 	value = 0.2f 	} : ConstantStatModifier
		{ statType = "BaseStats.SpreadMaxX" 				modifierType = "Additive" 	value = 8.0f 	} : ConstantStatModifier
		{ statType = "BaseStats.SpreadAdsMaxX" 				modifierType = "Additive" 	value = 1.5f 	} : ConstantStatModifier

		{ statType = "BaseStats.SpreadChangePerShot" 		modifierType = "Additive" 	value = 0.2f	} : ConstantStatModifier
		{ statType = "BaseStats.SpreadAdsChangePerShot" 	modifierType = "Additive" 	value = 0.05f 	} : ConstantStatModifier

		{ statType = "BaseStats.RecoilKickMin" 				modifierType = "Additive" 	value = 0.3f 	} : ConstantStatModifier
		{ statType = "BaseStats.RecoilKickMax" 				modifierType = "Additive" 	value = 0.4f 	} : ConstantStatModifier
	]
}



//------------------------------------------------------------------------------

w_lmg__constitutional_defender__rcv1 : WeaponRifleReceiverBase< Item >
{
	string friendlyName = ""
	CName appearanceResourceName = "w_lmg__constitutional_defender__rcv1_01"
}
w_lmg__constitutional_defender__base1 : WeaponRifleBarrelBase< Item >
{
	string friendlyName = ""
	CName appearanceResourceName = "w_lmg__constitutional_defender__base1_01"
}

w_lmg__constitutional_defender__mag_std : WeaponMagazineBase
{
	string friendlyName = ""
	CName appearanceResourceName = "w_lmg__constitutional_defender__mag_std_01"
}

w_lmg__constitutional_defender__mag_stdr : WeaponMagazineBaseEmpty
{
	string friendlyName = ""
	CName appearanceResourceName = "w_lmg__constitutional_defender__mag_stdr_01"
}

