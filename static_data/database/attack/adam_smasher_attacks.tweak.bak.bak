package Attacks
using RTDB
// ROCKET

AdamSmasherRocketProjectilePackage : RangedAttackPackage
{
    defaultFire = "Attacks.AdamSmasherRocketProjectileDefault"
}

AdamSmasherRocketProjectileDefault : RangedAttack
{
  	playerAttack = "Attacks.Adam_Smasher_Rocket_Projectile"
    playerTimeDilated = "Attacks.Adam_Smasher_Rocket_Projectile"

    NPCAttack = "Attacks.Adam_Smasher_Rocket_Projectile"
    NPCTimeDilated = "Attacks.Adam_Smasher_Rocket_Projectile"
}


// Basic projectile bullet
Adam_Smasher_Rocket_Projectile : BaseProjectile
{	
	projectileTemplateName = "explosive"

	// NPCs weapon damage needs to be independent of the weapon, based only on their power level
    fk< StatModifier >[] statModifiers =
	[
		"AttackModifier.PhysicalEnemyDamage",
	]
}

// =================================
//		SHOTGUN ATTACKS PACKAGE
// =================================
SmasherShotgunBulletPackage : RangedAttackPackage
{
    defaultFire = "Attacks.SmasherShotgunPhysicalBullet"
    chargeFire = "Attacks.SmasherShotgunPhysicalBullet"
}

// Hitscan bullet that does physical damage
SmasherShotgunPhysicalBullet : RangedAttack
{
    playerAttack = "Attacks.SmasherShotgunBulletEffect"
    playerTimeDilated = "Attacks.SmasherShotgunBulletProjectile"

    NPCAttack = "Attacks.SmasherShotgunBulletEffect"
    NPCTimeDilated = "Attacks.SmasherShotgunBulletProjectile"
}

SmasherShotgunBulletEffect : Bullet_GameEffect
{
    hitFlags = [ "FriendlyFire" ]

    // NPCs weapon damage needs to be independent of the weapon, based only on their power level
    fk< StatModifier >[] statModifiers =
	[
		"Attacks.SmasherShotgunDamage",
	]
}

SmasherShotgunBulletProjectile : Bullet_Projectile
{
    // NPCs weapon damage needs to be independent of the weapon, based only on their power level
    fk< StatModifier >[] statModifiers =
	[
		"Attacks.SmasherShotgunDamage",
	]
}

SmasherShotgunDamage : CombinedStatModifier
{ 
    statType = "BaseStats.PhysicalDamage"
    modifierType = "Additive" 
    refStat = "BaseStats.DPS" 
    refObject = "Root" 
    opSymbol = "*" 
    value = 0.125f	
}


// =================================
//		MINIGUN ATTACKS PACKAGE
// =================================
SmasherMinigunBulletPackage : RangedAttackPackage
{
    defaultFire = "Attacks.SmasherMinigunPhysicalBullet"
    chargeFire = "Attacks.SmasherMinigunPhysicalBullet"
}

// Hitscan bullet that does physical damage
SmasherMinigunPhysicalBullet : RangedAttack
{
    playerAttack = "Attacks.SmasherMinigunBulletEffect"
    playerTimeDilated = "Attacks.SmasherMinigunBulletProjectile"

    NPCAttack = "Attacks.SmasherMinigunBulletEffect"
    NPCTimeDilated = "Attacks.SmasherMinigunBulletProjectile"
}

SmasherMinigunBulletEffect : Bullet_GameEffect
{
    hitFlags = [ "FriendlyFire" ]

    // NPCs weapon damage needs to be independent of the weapon, based only on their power level
    fk< StatModifier >[] statModifiers =
	[
		"Attacks.SmasherMinigunDamage",
	]
}

SmasherMinigunBulletProjectile : Bullet_Projectile
{
    // NPCs weapon damage needs to be independent of the weapon, based only on their power level
    fk< StatModifier >[] statModifiers =
	[
		"Attacks.SmasherMinigunDamage",
	]
}

SmasherMinigunDamage : CombinedStatModifier
{ 
    statType = "BaseStats.PhysicalDamage"
    modifierType = "Additive" 
    refStat = "BaseStats.DPS" 
    refObject = "Root" 
    opSymbol = "*" 
    value = 0.05f	
}

