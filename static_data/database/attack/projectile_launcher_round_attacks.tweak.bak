package Attacks
using RTDB


BaseProjectileAoEAttack : Attack_GameEffect
{	
	attackType = "AttackType.Explosion"
	effectName = "projectile_aoe"
	range = 1.0f
	damageType = "DamageTypes.Physical"
}

MissileProjectile : BaseProjectileAoEAttack
{
	effectTag = "explosion_round"
	range = 2.0f
	
	fk< StatModifier >[] statModifiers = 
	[
		{ statType = "BaseStats.PhysicalDamage" modifierType = "Additive" refStat = "BaseStats.Health" refObject = "Root" opSymbol = "*" value = 0.90f	} : CombinedStatModifier
		{ statType = "BaseStats.HitDismembermentFactor"		modifierType = "AdditiveMultiplier"	value = 0.4f } : ConstantStatModifier,
	]

	fk< StatusEffectAttackData >[] statusEffects = 
	[
		{ statusEffect = "BaseStatusEffect.ExplosionStun" applyImmediately = true }
	]	
}

MissileProjectileCharged : MissileProjectile
{
	effectTag = "explosion_round_charged"
	range = 4.0f

	fk< StatModifier >[] statModifiers = 
	[
		{ statType = "BaseStats.PhysicalDamage" modifierType = "Additive" refStat = "BaseStats.Health" refObject = "Root" opSymbol = "*" value = 1.90f	} : CombinedStatModifier
		{ statType = "BaseStats.HitDismembermentFactor"		modifierType = "AdditiveMultiplier"	value = 0.8f } : ConstantStatModifier,
	]
}

EMPProjectile : BaseProjectileAoEAttack
{
	effectTag = "emp_round"
	range = 3.0f
	damageType = "DamageTypes.Electric"

	fk< StatModifier >[] statModifiers = 
	[
		{ statType = "BaseStats.ElectricDamage" modifierType = "Additive" refStat = "BaseStats.Health" refObject = "Root" opSymbol = "*" value = 0.70f	} : CombinedStatModifier
	]
}

EMPProjectileCharged : EMPProjectile
{
	effectTag = "emp_round_charged"
	range = 6.0f

	fk< StatModifier >[] statModifiers = 
	[
		{ statType = "BaseStats.ElectricDamage" modifierType = "Additive" refStat = "BaseStats.Health" refObject = "Root" opSymbol = "*" value = 0.70f	} : CombinedStatModifier
	]

	fk< StatusEffectAttackData >[] statusEffects = 
	[
		{ statusEffect = "BaseStatusEffect.Stun" applyImmediately = true }
	]	
}

ThermalProjectile : BaseProjectileAoEAttack
{
	effectTag = "thermal_round"
	range = 4.0f
	damageType = "DamageTypes.Thermal"

	fk< StatModifier >[] statModifiers = 
	[
		{ statType = "BaseStats.ThermalDamage" modifierType = "Additive" refStat = "BaseStats.Health" refObject = "Root" opSymbol = "*" value = 0.50f	} : CombinedStatModifier
	]
}

ThermalProjectileCharged : ThermalProjectile
{
	effectTag = "thermal_round_charged"
	range = 8.0f

	hitFlags = [ 'WeaponFullyCharged' ]

	fk< StatModifier >[] statModifiers = 
	[
		{ statType = "BaseStats.ThermalDamage" modifierType = "Additive" refStat = "BaseStats.Health" refObject = "Root" opSymbol = "*" value = 0.80f	} : CombinedStatModifier
	]
}


ChemicalProjectile : BaseProjectileAoEAttack
{
	effectTag = "chemical_round"
	range = 4.0f
	damageType = "DamageTypes.Chemical"

	fk< StatModifier >[] statModifiers = 
	[
		{ statType = "BaseStats.ChemicalDamage" modifierType = "Additive" refStat = "BaseStats.Health" refObject = "Root" opSymbol = "*" value = 0.70f	} : CombinedStatModifier
	]
}

ChemicalProjectileCharged : ChemicalProjectile
{
	effectTag = "chemical_round_charged"
	range = 8.0f

	hitFlags = [ 'WeaponFullyCharged' ]

	fk< StatModifier >[] statModifiers = 
	[
		{ statType = "BaseStats.ChemicalDamage" modifierType = "Additive" refStat = "BaseStats.Health" refObject = "Root" opSymbol = "*" value = 0.90f	} : CombinedStatModifier
	]
}

TranquilizerProjectile : BaseProjectileAoEAttack
{
	effectTag = "tranquilizer_round"
	range = 0.1f

	fk< StatModifier >[] statModifiers = 
	[
		{ statType = "BaseStats.PhysicalDamage" modifierType = "Additive" refStat = "BaseStats.Health" refObject = "Root" opSymbol = "*" value = 0.1f	} : CombinedStatModifier
	]

	fk< StatusEffectAttackData >[] statusEffects = 
	[
		{ statusEffect = "BaseStatusEffect.TranquilizerUnconscious" applyImmediately = true }
	]
}

TranquilizerProjectileCharged : TranquilizerProjectile
{
	effectTag = "tranquilizer_round_charged"
	range = 0.1f
}

ProjectileLauncherAttackHelper : Attack_Projectile
{
	projectileTemplateName = "launcher_round"
}