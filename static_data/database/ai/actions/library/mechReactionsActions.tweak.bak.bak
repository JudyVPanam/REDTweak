package MechReactionsActions
using RTDB, ItemHandling, AIActionTarget

//////////// CONDITION ///////////////////////

IsMech : AIActionCondition
{
	npcType = { allowedNPCTypes = [ "NPCType.Mech" ] }	
}

NotIsMech : AINPCTypeCond
{
	invert = true allowedNPCTypes = [ "NPCType.Mech" ]
}

AIMechReactionSequence : AIActionSequence
{
	activationCondition = "MechReactionsActions.IsMech"
}

AIMechReactionSelector : AIActionSelector
{
	activationCondition = "MechReactionsActions.IsMech"
}

//////////// DECORATORS ///////////////////////

// TURN AT
TurnAtReaction : AIMechReactionSelector
{	
	actions =
	[
		"MechReactionsActions.RotateToStimTarget"
		"MechReactionsActions.Success"
	]
}

EndTurnAtReaction : AIMechReactionSequence
{	
	actions =
	[
		"MechReactionsActions.IgnoreReactionResults"
		"MechReactionsActions.Success"
	]
}

// CALL GUARD
CallGuardInitialReaction : AIMechReactionSequence
{	
	actions =
	[
		"MechReactionsActions.CallGuard"
		"MechReactionsActions.Success"
	]
}

CallGuardRunAway : AIMechReactionSequence
{	
	actions =
	[
		"MechReactionsActions.Success"
	]
}

CallGuardWait : AIMechReactionSequence
{	
	actions =
	[
		"MechReactionsActions.Success"
	]
}

// INVESTIGATE
InvestigateInitialReaction : AIMechReactionSequence
{	
	actions =
	[
		"MechReactionsActions.StartReaction"
		"MechReactionsActions.RotateToStimSource"
		"MechReactionsActions.Success"
	]
}

InvestigateInProgress : AIMechReactionSequence
{		
	actions =
	[
		"MechReactionsActions.ApplyScanningDiodes"
		"MechReactionsActions.MoveCloserToStimTarget"
		"MechReactionsActions.Scan"
		"MechReactionsActions.Success"
	]
}

InvestigateComplete : AIMechReactionSelector
{		
	actions =
	[
		"MechReactionsActions.IgnoreReactionResults"
		"MechReactionsActions.Success"
	]
}

// INVESTIGATE DEVICE
InvestigateDeviceInitialReaction : AIMechReactionSequence
{
	actions =
	[
		"MechReactionsActions.StartReaction"
		"MechReactionsActions.RotateToStimTarget"
		"MechReactionsActions.Success"
	]
}

InvestigateDeviceExecution : AIMechReactionSequence
{		
	actions =
	[
		"MechReactionsActions.ApplyScanningDiodes"
		"MechReactionsActions.MoveToInvestigationSpot"
		"MechReactionsActions.RotateToStimSource"
		"MechReactionsActions.Scan"
	]
}

InvestigateDeviceComplete : AIMechReactionSequence
{		
	actions =
	[
		"MechReactionsActions.IgnoreReactionResults"
		"MechReactionsActions.Success"
	]
}

// FORCE COMBAT
ForceCombatInitialReaction : AIMechReactionSequence
{		
	actions =
	[
		"MechReactionsActions.StartReaction"
		"MechReactionsActions.Success"
	]
}

// REPRIMAND
ReprimandFollowMove : AIMechReactionSequence
{
	actions =
	[
		"MechReactionsActions.MoveToStimTargetInSecurityArea"
		"MechReactionsActions.RotateToStimTarget"
		"MechReactionsActions.Success"
	]
}

SearchHiddenTarget : AIMechReactionSequence
{
	actions =
	[
		"MechReactionsActions.MoveToStimSourceSearch"
		"MechReactionsActions.SearchLookAround"
		"MechReactionsActions.Success"
	]
}

TargetCompliesReaction : AIMechReactionSelector
{
	actions =
	[
		"MechReactionsActions.TargetComplies"
		"MechReactionsActions.Success"
	]
}

EnterCombatWithTarget : AIMechReactionSelector
{
	actions =
	[
		"MechReactionsActions.ChangeAttitudeToHostile"
		"MechReactionsActions.Success"
	]
}

IsNonCombatTypeCheck : AIMechReactionSelector
{
	actions =
	[
		"MechReactionsActions.IsNonCombatType"
		"MechReactionsActions.Success"
	]
}

ReprimandFinalWarning : AIMechReactionSelector
{
	actions =
	[
		"MechReactionsActions.FinalWarning"
		"MechReactionsActions.Success"
	]
}

ReprimandAskToLeave : AIMechReactionSelector
{
	actions =
	[
		"MechReactionsActions.AskToLeave"
		"MechReactionsActions.Success"
	]
}

ReprimandSuccessful : AIMechReactionSelector
{
	actions =
	[
		"MechReactionsActions.ReprimandConcluded"
		"MechReactionsActions.Success"
	]
}

ReprimandAssist : AIMechReactionSelector
{
	actions =
	[
		"MechReactionsActions.ReprimandAssistTrackTarget"
		"GenericArchetype.Success"
	]
}

RotateToTarget : AIMechReactionSelector
{
	actions =
	[
		"MechReactionsActions.RotateToStimTarget"
		"MechReactionsActions.Success"
	]
}

// ASK TO HOLSTER
AskToHolsterReaction : AIMechReactionSelector
{
	actions =
	[
		"MechReactionsActions.AskToHolsterWeapon"
		"MechReactionsActions.Success"
	]
}

// INVESTIGATE BODY
InvestigateBody : AIMechReactionSequence
{
	actions =
	[
		"MechReactionsActions.StartReaction"
		"MechReactionsActions.MoveCloserToStimTarget"
		"MechReactionsActions.Scan"
		"MechReactionsActions.Success"
	]
}

/////////////////////////////////////////////////////////////////
//////////////////////// PANIC REACTIONS ////////////////////////
/////////////////////////////////////////////////////////////////
GrenadeLandedInitialReaction : AIMechReactionSequence
{
	actions =
	[
		"MechReactionsActions.StartReaction"
		"GenericArchetype.Success"
	]
}

GrenadeLandedContinuedReaction : AIMechReactionSequence
{
	actions =
	[
		"GenericArchetype.Success"
	]
}

ExplosionReaction : AIMechReactionSequence
{
	actions =
	[
		"MechReactionsActions.Success"
	]
}

DeviceExplosionReaction : AIMechReactionSequence
{
	actions =
	[
		"MechReactionsActions.Success"
	]
}

LandingVeryHardReaction : AIMechReactionSequence
{
	actions =
	[
		"MechReactionsActions.Success"
	]
}

//////////// ACTIONS ////////////////////////

Scan : AIAction
{
	startup = { duration = 0.633f }
	loop = { duration = 6.667f }
	recovery = { duration = 0.667f }

	subActions = 
	[
		{ name = "ScanLong" } : AISubActionQueueAIEvent
		{ name = "StopScanning" delay = -1.f } : AISubActionQueueAIEvent
	]
	
	animData = { animFeature = "MechScan" animSlot = { useRootMotion = false } }
}

ReprimandConcluded : AIAction
{
	loop = { duration = 1.2f }
	
	recoverySubActions = 
	[ 
		{ name = "ReprimandSuccessful" } : AISubActionQueueAIEvent 
	]

	subActions = 
	[ 
		{ name = "StopScanning" delay = -1.f } : AISubActionQueueAIEvent
	]
}

ReprimandAssistTrackTarget : AIAction
{
	loop =
	{
		movePolicy =
		{ 
			target				= "AIActionTarget.StimTarget" 
			strafingTarget		= "AIActionTarget.StimTarget" 
			tolerance 			= 47.f
			distance 			= 50.f
			ignoreLoSPrecheck 	= true
			dynamicTargetUpdateTimer = 1.f
			avoidThreatRange 	= 0.f
		}
	}

	subActions = 
	[ 
		{ lightPreset = "MechanicalLightPreset.LookingForIntruder" delay = 0.25f } : AISubActionActivateLightPreset
	]

	lookats =
	[
		{ preset = "LookatPreset.MinotaurChassis"		target = "AIActionTarget.StimTarget" }
		{ preset = "LookatPreset.MinotaurLeftWeapon"	target = "AIActionTarget.StimTarget" }
		{ preset = "LookatPreset.MinotaurRightWeapon"	target = "AIActionTarget.StimTarget" }
	]
}

AskToHolsterWeapon : AIAction
{
	activationCondition =
	{
		target = { target = "AIActionTarget.StimTarget" }
	}
	
	startup = { duration = 1.733f }
	loop = { duration = 1.7f }
	recovery = { duration = 1.733f }
	
	animData = { animFeature = "AskToHolsterWeapon" animSlot = { useRootMotion = false } }

	subActions =
	[
		{ lightPreset = "MechanicalLightPreset.ReprimandInitialWarning" } : AISubActionActivateLightPreset
	]
	
	lookats =
	[
		{ preset = "LookatPreset.Reaction_EyesHeadWithBodyAttached" target = "AIActionTarget.StimTarget" }
	]
}

ChangeAttitudeToHostile : AIAction
{
	activationCondition =
	{
		target = { target = "AIActionTarget.StimTarget" minDetectionValue = 0.0f isActive = 1 }
	}
	
	loop = { duration = 0.5f }
	
	animData = { animFeature = "NonCombatAim" }

	lookats =
	[
		{ preset = "LookatPreset.DroneHorizontal"  target = { targetSlot = "Head"  } : AIActionTarget.StimTarget     },
		{ preset = "LookatPreset.DroneVertical"  target = { targetSlot = "Head"  } : AIActionTarget.StimTarget     }
	]
	
	subActions = 
	[ 
		{ target = "AIActionTarget.StimTarget" attitude = "AIA_Hostile" delay = 0.25f } : AISubActionChangeAttitude
	]

	animationWrapperOverrides = [ "combatLocomotion" ]
}

IsNonCombatType : AIAction
{
	activationCondition	=
	{
		reaction = 
		[
			{ preset = "ReactionPresets.Civilian_Neutral" }
		]
	}
}

FinalWarning : AIAction
{	
	startup = { duration = 0.1f }

	loop = 
	{ 
		movePolicy 	= 
		{ 
			target 				= "AIActionTarget.StimTarget" 
			strafingTarget 		= "AIActionTarget.StimTarget"
			movementType 		= "Walk" 
			ignoreLoSPrecheck 	= true
			tolerance 			= 1.f 
			distance 			= 5.f
			dynamicTargetUpdateTimer = 1.f
			avoidThreatRange 	= 0.f
		}
	}
	
	startupSubActions = 
	[ 
		{ lightPreset = "MechanicalLightPreset.ReprimandFinalWarning" } : AISubActionActivateLightPreset
	]
	
	lookats =
	[
		{ preset = "LookatPreset.MinotaurChassis"		target = "AIActionTarget.StimTarget" }
		{ preset = "LookatPreset.MinotaurLeftWeapon"	target = "AIActionTarget.StimTarget" }
		{ preset = "LookatPreset.MinotaurRightWeapon"	target = "AIActionTarget.StimTarget" }
	]

	animationWrapperOverrides = [ "MechLocomotion_Reprimand" ]
}

TargetComplies : AIAction
{
	startup = { duration = 0.1f }
	loop = {}
	
	startupSubActions = 
	[ 
		{ lightPreset = "MechanicalLightPreset.ReprimandTargetComplies" } : AISubActionActivateLightPreset
	]

	subActions = 
	[ 
		{ lightPreset = "MechanicalLightPreset.Idle" delay = -1.f } : AISubActionActivateLightPreset
	]
}

AskToLeave : AIAction
{
	startup = { duration = 0.1f }

	loop = 
	{ 
		duration = 3.f 
		movePolicy 	= 
		{ 
			target 				= "AIActionTarget.StimTarget" 
			strafingTarget 		= "AIActionTarget.StimTarget"
			movementType 		= "Walk" 
			ignoreLoSPrecheck 	= true
			tolerance 			= 1.f 
			distance 			= 5.f
			dynamicTargetUpdateTimer = 1.f
			avoidThreatRange 	= 0.f
		}
	}
	
	startupSubActions = 
	[ 
		{ lightPreset = "MechanicalLightPreset.ReprimandInitialWarning" } : AISubActionActivateLightPreset
	]
	
	lookats =
	[
		{ preset = "LookatPreset.MinotaurChassis"		target = "AIActionTarget.StimTarget" }
		{ preset = "LookatPreset.MinotaurLeftWeapon"	target = "AIActionTarget.StimTarget" }
		{ preset = "LookatPreset.MinotaurRightWeapon"	target = "AIActionTarget.StimTarget" }
	]
}

SearchLookAround : AIAction
{
	startup = { duration = 2.1f }
	loop = { duration = 2.1f }
	recovery = { duration = 2.167f }
	
	animData = { animFeature = "LookAround" animSlot = {} }
	
	subActions = 
	[ 
		{ name = "stlh_search" delay = 0.25f } : AISubActionPlayVoiceOver 
	]
}

MoveToStimSourceSearch : AIAction
{	
	activationCondition	=
	{
		spatialAND = 
		[
			{ target = "AIActionTarget.StimSource" }
		]
		
		restrictedMovementArea =
		[
			{ target = "AIActionTarget.StimSource" }
		]
		
		calculatePath =
		[
			{ target = "AIActionTarget.StimSource" }
		]
	}
	
	loop = 
	{ 
		changeNPCState = { upperBodyState = "Aim" }
		movePolicy = 
		{ 
			target 			= "AIActionTarget.StimSource" 
			movementType 	= "Walk"
			dontUseStart	= true			
		}	
		
		toNextPhaseConditionCheckInterval = 0.5f
		toNextPhaseCondition =
		[
			{	
				spatialAND = 
				[
					{ 
						target		= "AIActionTarget.StimSource" 	
						distance 	= ( -1, 1.f )
					}
				]
			}
		]
	}
}

MoveCloserToStimTarget : AIAction
{	
	activationCondition	=
	{
		spatialAND = 
		[
			{ 
				target	= "AIActionTarget.StimTarget" 	
			}
		]
		
		calculatePath =
		[
			{ target = "AIActionTarget.StimSource" }
		]
	}
	
	loop = 
	{ 	
		movePolicy = 
		{ 
			target 			= "AIActionTarget.StimTarget" 
			movementType 	= "Walk"	
		}

		toNextPhaseConditionCheckInterval = 0.5f
		toNextPhaseCondition =
		[
			{	
				spatialAND = 
				[
					{ 
						target		= "AIActionTarget.StimTarget" 	
						distance 	= ( -1, 2.5f )
					}
				]
			}
		]
	}
}

MoveToStimTargetInSecurityArea : AIAction
{	
	activationCondition	=
	{
		spatialAND = 
		[
			{ target = "AIActionTarget.StimTarget" distance = ( 9.f, -1.f ) }
		]
		
		tresspassing =
		[
			{ target = "AIActionTarget.StimTarget" } 
		]
	}
	
	loop = 
	{ 
		movePolicy = 
		{ 
			target = "AIActionTarget.StimTarget" 
			movementType = "Walk"
			dontUseStart	= true
		}	
		
		toNextPhaseConditionCheckInterval = 0.2f
		toNextPhaseCondition =
		[
			{	
				spatialOR = 
				[
					{ target = "AIActionTarget.StimTarget" distance = ( -1.f, 8.f ) }
				]
			}
			
			{		
				tresspassing =
				[
					{ target = "AIActionTarget.StimTarget" invert = true } 
				]	
			}
		]
	}
}

RotateToStimTarget : AIAction
{
	activationCondition	=
	{
		spatialAND = 
		[
			{ target = "AIActionTarget.StimTarget" coneAngle = ( 45.f, -1.f ) }
		]
	}

	loop = 
	{
		movePolicy =
		{ 
			strafingTarget	= "AIActionTarget.StimTarget" 
			deadAngle = 0.1f;
		}
		
		toNextPhaseConditionCheckInterval = 0.5f
		toNextPhaseCondition =
		[
			{
				spatialAND = 
				[
					{ target = "AIActionTarget.StimTarget" coneAngle = ( -1.f, 15.f ) }
				]
			}
		]
	}

	loopSubActions =
	[
		{ lightPreset = "MechanicalLightPreset.ReactToStimuli" } : AISubActionActivateLightPreset
	]
	
	lookats =
	[
		// TODO: Mech LookAts
	]
}

MoveToInvestigationSpot : AIAction
{	
	activationCondition	=
	{
		spatialAND = 
		[
			{ target = "AIActionTarget.StimSource" }
		]
		
		calculatePath =
		[
			{ target = "AIActionTarget.StimSource" }
		]
	}
	
	loop = 
	{ 
		movePolicy = 
		{ 
			target 			= "AIActionTarget.StimSource" 
			movementType 	= "Walk"
			dontUseStart	= true
		}	
		
		toNextPhaseConditionCheckInterval = 0.5f
		toNextPhaseCondition =
		[
			{	
				spatialAND = 
				[
					{ 
						target		= "AIActionTarget.StimSource" 	
						distance 	= ( -1, 0.3f )
					}
				]
			}	
		]
	}
}

RotateToStimSource : AIAction
{
	activationCondition	=
	{
		spatialAND = 
		[
			{ target = "AIActionTarget.StimSource" coneAngle = ( 45.f, -1.f ) }
		]
	}

	loop = 
	{
		movePolicy =
		{ 
			strafingTarget	= "AIActionTarget.StimSource" 
			deadAngle = 5f;
		}
		
		toNextPhaseConditionCheckInterval = 0.5f
		toNextPhaseCondition =
		[
			{
				spatialAND = 
				[
					{ target = "AIActionTarget.StimSource" coneAngle = ( -1.f, 30.f ) }
				]
			}
		]
	}
}

CallGuard : AIAction
{
	loop = { duration = 1.f }
	recovery = { duration = 0.2f }
	
	/*animData = { animFeature = "PanicFront" animSlot = {} }*/
	
	subActions = 
	[ 
		{ lightPreset = "MechanicalLightPreset.CallGuards" } : AISubActionActivateLightPreset
	]
	
	recoverySubActions = 
	[ 
		{ stimSource = "AIActionTarget.Owner" stimType = "StimTypes.Combat" } : AISubActionTriggerStim 
	]
}

Success : AIAction
{
	loop = { duration = 0.0000000000001f }
}

StartReaction : AIAction
{
	startup = { duration = 0.7f }

	subActions = 
	[
		{ lightPreset = "MechanicalLightPreset.ReactToStimuli" } : AISubActionActivateLightPreset
	]
}

ApplyScanningDiodes : AIAction
{
	subActions = 
	[ 
		{ lightPreset = "MechanicalLightPreset.ScanInProgress" } : AISubActionActivateLightPreset
	]
}

IgnoreReactionResults : AIAction
{
	loop = { duration = 1.f }
	
	subActions = 
	[ 
		{ lightPreset = "MechanicalLightPreset.ScanResultsIgnored" } : AISubActionActivateLightPreset
	]
}