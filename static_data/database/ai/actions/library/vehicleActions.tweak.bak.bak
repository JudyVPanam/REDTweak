package VehicleActions
using RTDB, ItemHandling, LookatPreset, GenericArchetype, FollowerActions, AIActionTarget

//FileOwner: Patryk.Fiutowski

Success : AIAction {}

// "VehicleActions.CombatTarget"
CombatTarget : AIActionTarget.CombatTarget 
{
	trackingMode = "TargetTracking.RealPosition"
}

VehicleStateDecorator : AIAction
{
	loop = 
	{ 
		changeNPCState = { hitReactionMode	= "Unstoppable" }  
		duration = -1.f
	}
}

EnterVehicleSelector : AIActionSmartComposite
{
	type = "AISmartCompositeType.Selector"
	
	nodes =
	[
		"VehicleActions.GunnerEquipRifle"
		"VehicleActions.EnterVehicleWithAnim"
	]
}

EnterVehicleWithAnim : AIAction
{
	loop =
	{
		duration = 2.1f // doesn't controll anim duration; just logical pause in behavior
	}

	subActions = 
	[
		{ attachmentSlot = "AttachmentSlots.WeaponRight" } : AISubActionForceUnequip
		{ attachmentSlot = "AttachmentSlots.WeaponLeft" } : AISubActionForceUnequip
	]
}

CombatExitVehicleWithAnim : AIAction
{
	activationCondition =
	{
		state = 
		[ 
			{ target = "AIActionTarget.Owner" inStates = [ "VehicleWindow" ] } // find out why combat check is not working :(
		]
	}

	loop =
	{
		duration = 1.5f //exit anim duration; it sends signal to workspot, so it doesn't scale the anim :( it's just logical dealy before anim is interrupted
	}

	subActions =
	[	
 		{ delay = -1 name = "InstantUnmount" } : AISubActionQueueAIEvent
	]
}

ExitVehicleWithAnim : AIAction
{
	loop =
	{
		duration = -1.0f //exit anim duration; it sends signal to workspot, so it doesn't scale the anim :( it's just logical dealy before anim is interrupted
	}

	subActions =
	[	
 		{ delay = -1 name = "InstantUnmount" } : AISubActionQueueAIEvent
	]
}

InstantUnmount : AIAction
{
	loop = 
	{ 
		duration = 0.1f
	}

	animData =
	{
		animFeature = "GetOffWindow"
	}


	subActions =
	[
		{ name = "InstantUnmount" } : AISubActionQueueAIEvent
	]
}

FollowerMovingToVehicle : AIAction
{
	disableAction = true

	activationCondition =
	{
		vehicleOR =
		[
			{
				vehicle = "AIActionTarget.FriendlyTarget"
			}
		]
	}

	loop = { duration = 2.f } //timeout
}

IdleInVehicleComposite : AIActionSmartComposite
{
	type = "AISmartCompositeType.Selector"

	repeat = 0

	nodes =
	[
		"VehicleActions.GunnerEquipRifle"
		"VehicleActions.Success"
	]
}

AlertedInVehicleComposite : AIActionSmartComposite
{
	type = "AISmartCompositeType.Selector"

	repeat = 0

	nodes =
	[
		"VehicleActions.DoNothingIfFastReaction"
		"VehicleActions.UnmountVehicleInAlerted"
		"VehicleActions.Success"
	]
}

FollowerInVehicleComposite : AIActionSmartComposite
{
	type = "AISmartCompositeType.Selector"

	repeat = 0

	nodes =
	[
		"FollowerActions.ExitVehicle"
		"VehicleActions.Success"
	]
}

GetOffWindow : AIAction
{
	activationCondition =
	{
		condition =
		{
			AND =
			[
				{ target = "AIActionTarget.Owner" inStates = [ "VehicleWindow" ] } : AIStateCond
			]
		} : AIActionAND
	}

	loop = 
	{ 
		duration = 1.0f
	}

	animData =
	{
		animFeature = "GetOffWindow"
	}

	loopSubActions = 
	[
		{ attachmentSlot = "AttachmentSlots.WeaponLeft"		animationTime = 0.6f } : AISubActionForceUnequip
		{ attachmentSlot = "AttachmentSlots.WeaponRight"	animationTime = 0.6f } : AISubActionForceUnequip
	]
}

CanFightInVehicle : AIAction
{
	//P.F. for now all NPCs always can fight
	/*activationCondition =
	{
		inventoryOR =
		[
			{ itemCategory = "ItemCategory.Weapon" itemTag = "RangedWeapon" }
		]
	}*/
}

CanDriverFightInVehicle : AIAction
{
	//P.F. for now all NPCs always can fight
	/*
	activationCondition =
	{
		inventoryOR =
		[
			{ itemType = "ItemType.Wea_Handgun" }
			{ itemType = "ItemType.Wea_Revolver" }
		]
	}*/
}

UnequipWeaponOnExit : AIAction
{
	tickets = [] //Removing ticket requirement from vehicle combat equip actions, after consulting with Patryk

	activationCondition =
	{
		slotOR =
		[
			{ slot = "AttachmentSlots.WeaponLeft" }
			{ slot = "AttachmentSlots.WeaponRight" }
		]
	}

	loop = { duration = 0.6f changeNPCState = { upperBodyState = "Equip" } }
	
	loopSubActions = 
	[
		{ attachmentSlot = "AttachmentSlots.WeaponLeft"		animationTime = 0.6f } : AISubActionForceUnequip
		{ attachmentSlot = "AttachmentSlots.WeaponRight"	animationTime = 0.6f } : AISubActionForceUnequip
	]
}

PassengerEquipWeapon : AIActionSmartComposite
{
	type = "AISmartCompositeType.Selector"

	nodes =
	[
		"VehicleActions.PassengerSportEquipAnyHandgun"
		"VehicleActions.PassengerSportFailSafeEquipHandgun"
		"VehicleActions.GunnerEquipRifle"
		"VehicleActions.EquipAnyRifleFromInventory"
		"VehicleActions.FailSafeEquipRifle"
	] 
}

EquipAbstract : AIAction
{
	tickets = []

	startup = { duration = 0.1f changeNPCState = { upperBodyState = "Equip" } }

	startupSubActions = 
	[
		{ attachmentSlot = "AttachmentSlots.WeaponRight" } : AISubActionForceUnequip
		{ attachmentSlot = "AttachmentSlots.WeaponLeft" } : AISubActionForceUnequip
	]
}

EquipAnyRifleFromInventory : EquipAbstract
{
	tickets = []

	activationCondition =
	{
		slotOR =
		[
			{ invert = true slot = "AttachmentSlots.WeaponRight" itemType = "ItemType.Wea_Rifle" }
			{ slot = "AttachmentSlots.WeaponLeft" }
		]

		inventoryOR =
		[
			{ itemType = "ItemType.Wea_Rifle" }
		]
	}

	loop =
	{
		duration = 1.2f
		changeNPCState = { upperBodyState = "Equip" }
	}

	loopSubActions = 
	[
		{ attachmentSlot = "AttachmentSlots.WeaponRight" itemType = "ItemType.Wea_Rifle" animationTime = 1.2f delay = 0.3f } : AISubActionForceEquip
	]
}

FailSafeEquipRifle : VehicleActions.EquipAbstract
{
	tickets = []

	activationCondition =
	{
		slotOR =
		[
			{ invert = true slot = "AttachmentSlots.WeaponRight" itemType = "ItemType.Wea_Rifle" }
			{ slot = "AttachmentSlots.WeaponLeft" }
		]
	}

	loop =
	{
		duration = 1.2f
		changeNPCState = { upperBodyState = "Equip" }
	}

	loopSubActions = 
	[
		{ attachmentSlot = "AttachmentSlots.WeaponRight" itemID = "Items.Preset_Base_Copperhead" animationTime = 1.2f delay = 0.3f } : AISubActionForceEquip
	]
}

GunnerEquipRifle : VehicleActions.EquipAbstract
{
	tickets = []

	activationCondition =
	{
		condition = 
		{
			AND =
			[
				{ invert = true } : AIDriverCond
				{ hasTags = [ 'FastReaction' ] } : AIVehicleCond
			]
		} : AIActionAND
	}

	loop =
	{
		duration = 0.001f
	}

	loopSubActions = 
	[
		{ attachmentSlot = "AttachmentSlots.WeaponRight" itemID = "Items.Preset_Base_Copperhead" delay = -1.f } : AISubActionForceEquip
	]
}


DriverEquipWeapon : AIActionSmartComposite
{
	type = "AISmartCompositeType.Selector"

	nodes =
	[
		"VehicleActions.MotorcycleDriverEquipAnyHandgun"
		"VehicleActions.MotorcycleDriverFailSafeEquipHandgun"
		"VehicleActions.DriverEquipAnyHandgun"
		"VehicleActions.DriverFailSafeEquipHandgun"
	]
}

MotorcycleDriverEquipAnyHandgun : VehicleActions.EquipAbstract
{
	tickets = []

	activationCondition =
	{
		inventoryOR =
		[
			{ itemType = "ItemType.Wea_Handgun" }
		]
		
		slotOR =
		[
			{ invert = true slot = "AttachmentSlots.WeaponRight" itemType = "ItemType.Wea_Handgun" }
			{ slot = "AttachmentSlots.WeaponLeft" }
		]

		vehicleAND = 
		[
			{ vehicle = "AIActionTarget.MountedVehicle" hasTags = [ "Motorcycle" ] }
		]
	}

	loop = { duration = 1.2f changeNPCState = { upperBodyState = "Equip" } }

	loopSubActions = 
	[
		{ attachmentSlot = "AttachmentSlots.WeaponRight" itemType = "ItemType.Wea_Handgun" animationTime = 1.2f delay = 0.3f } : AISubActionForceEquip
	]
}

MotorcycleDriverFailSafeEquipHandgun : VehicleActions.EquipAbstract
{
	tickets = []
	
	activationCondition =
	{
		slotOR =
		[
			{ slot = "AttachmentSlots.WeaponLeft" }
			{ invert = true slot = "AttachmentSlots.WeaponRight" itemType = "ItemType.Wea_Handgun" }
		]

		vehicleAND = 
		[
			{ vehicle = "AIActionTarget.MountedVehicle" hasTags = [ "Motorcycle" ] }
		]
	}

	loop = { duration = 1.2f changeNPCState = { upperBodyState = "Equip" } }

	loopSubActions = 
	[
		{ attachmentSlot = "AttachmentSlots.WeaponRight" itemID = "Items.Preset_Base_Lexington" animationTime = 1.2f delay = 0.3f } : AISubActionForceEquip
	]
}

DriverEquipAnyHandgun : VehicleActions.EquipAbstract
{
	tickets = []

	activationCondition =
	{
		inventoryOR =
		[
			{ itemType = "ItemType.Wea_Handgun" }
		]
		
		slotOR =
		[
			{ invert = true slot = "AttachmentSlots.WeaponLeft" itemType = "ItemType.Wea_Handgun" }
			{ slot = "AttachmentSlots.WeaponRight" }
		]
	}

	loop = { duration = 1.2f changeNPCState = { upperBodyState = "Equip" } }

	loopSubActions = 
	[
		{ attachmentSlot = "AttachmentSlots.WeaponLeft" itemType = "ItemType.Wea_Handgun" animationTime = 1.2f delay = 0.3f } : AISubActionForceEquip
	]
}

DriverFailSafeEquipHandgun : VehicleActions.EquipAbstract
{
	tickets = []
	
	activationCondition =
	{
		slotOR =
		[
			{ invert = true slot = "AttachmentSlots.WeaponLeft" itemType = "ItemType.Wea_Handgun" }
			{ slot = "AttachmentSlots.WeaponRight" }
		]
	}

	loop = { duration = 1.2f changeNPCState = { upperBodyState = "Equip" } }

	loopSubActions = 
	[
		{ attachmentSlot = "AttachmentSlots.WeaponLeft" itemID = "Items.Preset_Base_Lexington" animationTime = 1.2f delay = 0.3f } : AISubActionForceEquip
	]
}

PassengerSportEquipAnyHandgun : VehicleActions.EquipAbstract
{
	tickets = []

	activationCondition =
	{
		inventoryOR =
		[
			{ itemType = "ItemType.Wea_Handgun" }
		]
		
		slotOR =
		[
			{ invert = true slot = "AttachmentSlots.WeaponRight" itemType = "ItemType.Wea_Handgun" }
			{ slot = "AttachmentSlots.WeaponLeft" }
		]

		vehicleAND = 
		[
			{ vehicle = "AIActionTarget.MountedVehicle" hasTags = [ "Sport" ] }
		]
	}

	loop = { duration = 1.2f changeNPCState = { upperBodyState = "Equip" } }

	loopSubActions = 
	[
		{ attachmentSlot = "AttachmentSlots.WeaponRight" itemType = "ItemType.Wea_Handgun" animationTime = 1.2f delay = 0.3f } : AISubActionForceEquip
	]
}

PassengerSportFailSafeEquipHandgun : VehicleActions.EquipAbstract
{
	tickets = []
	
	activationCondition =
	{
		slotOR =
		[
			{ invert = true slot = "AttachmentSlots.WeaponRight" itemType = "ItemType.Wea_Handgun" }
			{ slot = "AttachmentSlots.WeaponLeft" }
		]

		vehicleAND = 
		[
			{ vehicle = "AIActionTarget.MountedVehicle" hasTags = [ "Sport" ] }
		]
	}

	loop = { duration = 1.2f changeNPCState = { upperBodyState = "Equip" } }

	loopSubActions = 
	[
		{ attachmentSlot = "AttachmentSlots.WeaponRight" itemID = "Items.Preset_Base_Lexington" animationTime = 1.2f delay = 0.3f } : AISubActionForceEquip
	]
}

UnmountVehicleInCombatCondition : AIActionCondition
{
	condition = 
	{
		OR =
		[
			{
				AND = 
				[
					"Condition.IsFollower"
					{ invert = true vehicle = "AIActionTarget.FriendlyTarget" } : AIVehicleCond
				]

			}  : AIActionAND
			
			{
				AND = 
				[
					"Condition.NotIsFollower"
					{ invert = true vehicle = "VehicleActions.CombatTarget"} : AIVehicleCond
					{ target = "VehicleActions.CombatTarget" distance = ( -1.f, 10.f ) } : AISpatialCond
				]

			}  : AIActionAND
			
		]
	} : AIActionOR
}

MountVehicleInCombatCondition : AIActionCondition
{
	spatialAND =
	[
		{ target = "VehicleActions.CombatTarget"	distance = ( 15.f, -1.f ) }
		{ target = "AIActionTarget.AssignedVehicle"	distance = ( -1.f, 10.f ) }
	]

	vehicleOR =
	[
		{ vehicle = "AIActionTarget.AssignedVehicle" }
	]

	target =
	{
		invert = true //not a follower
		target = "AIActionTarget.FriendlyTarget"
	}

	squadAND = [ { invert = true hasTickets = [ "AITicketType.Equip" ] } ]

	commandAND = 
	[ 
		{ invert = true hasCommands = [ "AIThrowGrenadeCommand" ] } 
		{ invert = true hasCommands = [ "AIMoveCommand" ] }
		{ invert = true hasCommands = [ "AIUseWorkspotCommand" ] }
		{ invert = true hasCommands = [ "AIUseCoverCommand" ] }
	]

	cooldown = 
	[ 
		{ 
			cooldowns = [ { name = "VehicleUnmount" } ]
		}
	]
}

UnmountVehicleIfCannotFight : UnmountVehicleInCombat
{
	activationCondition =
	{
		spatialAND =
		[
			{ target = "VehicleActions.CombatTarget" distance = ( -1.f, 8.f ) }
		]

		inventoryAND =
		[
			{ invert = true itemCategory = "ItemCategory.Weapon" itemTag = "RangedWeapon" }
			{ itemCategory = "ItemCategory.Weapon" itemTag = "MeleeWeapon" }
		]

		vehicleAND =
		[
			{ invert = true vehicle = "VehicleActions.CombatTarget" }
			{ vehicle = "AIActionTarget.Owner" currentSpeed = ( -1, 0.1f ) }
		]
	}
}

UnmountVehicleInAlerted : UnmountVehicle
{
	activationCondition =
	{
		condition = 
		{
			OR =
			[
				{ currentSpeed = (-1, 0.5f ) } : AIVehicleCond
				{} : AIDriverCond
			]
		} : AIActionOR
	}
}

UnmountVehicleInCombat : UnmountVehicle
{
	activationCondition = "VehicleActions.UnmountVehicleInCombatCondition"

	subActions +=
	[
		{ delay = 0.f cooldowns = [ { name = "VehicleUnmount" duration = 5.f } ] } : AISubActionStartCooldown
	]
}


UnmountVehicle : AIAction
{
	loop = 
	{
		repeat = 0
		duration = 1.f
	}

	loopSubActions =
	[
		{ name = "ExitVehicle" } : AISubActionQueueAIEvent
	]

	subActions =
	[
		
		{ delay = 0.f cooldowns = [ { name = "VehicleUnmount" duration = 5.f } ] } : AISubActionStartCooldown
	]
}

MountVehicleInCombat : AIAction
{
	disableAction = true
	activationCondition = "VehicleActions.MountVehicleInCombatCondition"
}

MountVehicleInMeleeCombat : AIAction
{
	activationCondition = 
	{
		target =
		{
			target = "AIActionTarget.AssignedVehicle"
		}

		squadAND = [ { invert = true hasTickets = [ "AITicketType.Equip" ] } ]

		commandAND = 
		[ 
			{ invert = true hasCommands = [ "AIThrowGrenadeCommand" ] } 
			{ invert = true hasCommands = [ "AIMoveCommand" ] }
			{ invert = true hasCommands = [ "AIUseWorkspotCommand" ] }
			{ invert = true hasCommands = [ "AIUseCoverCommand" ] }
		]

		cooldown = 
		[ 
			{ 
				cooldowns = [ { name = "VehicleUnmount" } ]
			}
		]

		vehicleAND =
		[
			{ vehicle = "VehicleActions.CombatTarget" }
		]
	}
}


CombatInVehicleComposite : AIActionSmartComposite
{
	gracefulInterruptionConditionCheckInterval = 0.5f
	gracefulInterruptionCondition =
	[
		"VehicleActions.UnmountVehicleInCombatCondition"
	]
	
	nodes =
	[
		"VehicleActions.MotorcycleShootingSelector"
		"VehicleActions.DriverShootingSelector"
		"VehicleActions.PassengerShootingSelector"
		"VehicleActions.ReloadSelector"
		"GenericArchetype.Success"
	]
}

CombatInVehicleDecorator : AIActionSmartComposite
{
	repeat = 0
	nodes =
	[
		"VehicleActions.CombatLookatAction"
	]
}

CombatLookatAction : AIAction
{
	lookats =
	[
		"VehicleActions.EyesHeadLookat"
	]

	loop = {}
}

////////////////////////////////////////////////////////
//////////////////////////SHOOTING//////////////////////
////////////////////////////////////////////////////////

MotorcycleShootingSelector : AIActionSelector
{
	activationCondition =
	{
		condition = 
		{
			AND =
			[
				{ vehicle = "AIActionTarget.MountedVehicle" hasTags = [ "Motorcycle" ] } : AIVehicleCond
			]
		} : AIActionAND
	}

	actions =
	[
		//"VehicleActions.MotorcycleSideSwitch"
		//"VehicleActions.FailSafeEquipHandgun"
		"VehicleActions.MotorcycleDriverShootLeft"
		"VehicleActions.MotorcycleDriverShootRight"
	]
}

FailSafeEquipHandgun : VehicleActions.EquipAbstract
{
	tickets = []
	
	activationCondition =
	{
		slotOR =
		[
			{ slot = "AttachmentSlots.WeaponLeft" }
		]
	}

	loop = { duration = 1.2f changeNPCState = { upperBodyState = "Equip" } }

	loopSubActions = 
	[
		{ attachmentSlot = "AttachmentSlots.WeaponRight" itemID = "Items.Preset_Base_Lexington" animationTime = 1.2f delay = 0.3f } : AISubActionForceEquip
	]
}

DriverShootingSelector : AIActionSelector
{
	activationCondition =
	{
		condition = 
		{
			AND =
			[
				{ } : AIDriverCond
				{ invert = true vehicle = "AIActionTarget.MountedVehicle" hasTags = [ "Motorcycle" ] } : AIVehicleCond
			]
		} : AIActionAND
	}

	actions =
	[
		"VehicleActions.DriverShoot"
	]
}

PassengerShootingSelector : AIActionSelector
{
	activationCondition =
	{
		condition = 
		{
			AND =
			[
				{ invert = true } : AIDriverCond
				{ invert = true vehicle = "AIActionTarget.MountedVehicle" hasTags = [ "Motorcycle" ] } : AIVehicleCond
			]
		} : AIActionAND
	}

	actions =
	[
		"VehicleActions.ShootFromVehicleToVehicle"
		"VehicleActions.ShootFromVehicleToTargetOnFoot"
	]
}

MotorcycleSideSwitch : AIAction
{
	activationCondition =
	{
		condition = 
		{
			AND =
			[
				"VehicleActions.SideSwitchCooldown"
				{ vehicle = "AIActionTarget.MountedVehicle" hasTags = [ "Motorcycle" ] } : AIVehicleCond
				{ target = "VehicleActions.CombatTarget" coneAngle = ( 90.f, -1.f ) } : AISpatialCond
			]
		} : AIActionAND
	}

	animData =
	{
		animFeature = "SideSwitch"
	}

	loop =
	{
		duration = 1.3f
	}
}

//VehicleActions.SideSwitchCooldown
SideSwitchCooldown : AICooldownCond
{
    cooldowns = [ { name = "SideSwitch" } ]

	invert = true
}

MotorcycleDriverShoot : ShootFromVehicleAbstract
{
	lookats =
	[
		"VehicleActions.RightHandLookat"
	]

	startup	= 
	{ 
		duration = 0.1f	
		changeNPCState = { upperBodyState = "Shoot" locomotionMode = "Static" }
	}

	subActions =
	[
		{ delay = -1.f cooldowns = [ { name = "SideSwitch" duration = 0.1f } ] } : AISubActionStartCooldown
	]

	loopSubActions = 
	[
		{
			weaponSlots = [ "AttachmentSlots.WeaponRight" , "AttachmentSlots.WeaponLeft" ]
			target = "VehicleActions.CombatTarget"
			pauseCondition = 
			[
				{
					friendlyFire = {}
				}
			]

		} : AISubActionShootWithWeapon
	]
}

MotorcycleDriverShootLeft : MotorcycleDriverShoot
{
	animData =
	{
		animFeature = "ShootAction"
		animVariation = 1;
	}

	activationCondition =
	{
		spatialAND	= 
		[
			{ target = "VehicleActions.CombatTarget" angleDirection = -1 }
		]

		slotOR =
		[
			{ slot = "AttachmentSlots.WeaponRight"	itemCategory = "ItemCategory.Weapon" itemTag = "RangedWeapon" }
			{ slot = "AttachmentSlots.WeaponLeft"	itemCategory = "ItemCategory.Weapon" itemTag = "RangedWeapon" }
		]
		
		ammoCountOR =
		[
			{ weaponSlot = "AttachmentSlots.WeaponRight"	min = 1 }
			{ weaponSlot = "AttachmentSlots.WeaponLeft"		min = 1 }
		]
		
		target = 
		{
			target = "VehicleActions.CombatTarget"
		}

		squadAND = [ { invert = true hasTickets = [ "AITicketType.Equip" ] } ]
	}

	loop = 
	{ 
		changeNPCState = { upperBodyState = "Shoot" locomotionMode = "Static" }
		toNextPhaseCondition = 
		[
			{
				ammoCountOR =
				[
					{ invert = true weaponSlot = "AttachmentSlots.WeaponRight"	min = 1 }
					{ invert = true weaponSlot = "AttachmentSlots.WeaponLeft"	min = 1 }
				]
			}

			{
				signalOR = 
				[
					{
						name = "CombatTargetChanged"
					}
				]
			}

			{
				spatialAND	= 
				[
					{ target = "VehicleActions.CombatTarget" coneAngle = ( -1.f, 315.f ) angleDirection = 1 }
				]
			}
		]
	}
}

MotorcycleDriverShootRight : MotorcycleDriverShoot
{
	animData =
	{
		animFeature = "ShootAction"
		animVariation = 2;
	}

	activationCondition =
	{
		spatialAND	= 
		[
			{ target = "VehicleActions.CombatTarget" angleDirection = 1 }
		]

		slotOR =
		[
			{ slot = "AttachmentSlots.WeaponRight"	itemCategory = "ItemCategory.Weapon" itemTag = "RangedWeapon" }
			{ slot = "AttachmentSlots.WeaponLeft"	itemCategory = "ItemCategory.Weapon" itemTag = "RangedWeapon" }
		]
		
		ammoCountOR =
		[
			{ weaponSlot = "AttachmentSlots.WeaponRight"	min = 1 }
			{ weaponSlot = "AttachmentSlots.WeaponLeft"		min = 1 }
		]
		
		target = 
		{
			target = "VehicleActions.CombatTarget"
		}

		squadAND = [ { invert = true hasTickets = [ "AITicketType.Equip" ] } ]
	}

	loop = 
	{ 
		changeNPCState = { upperBodyState = "Shoot" locomotionMode = "Static" }
		toNextPhaseCondition = 
		[
			{
				ammoCountOR =
				[
					{ invert = true weaponSlot = "AttachmentSlots.WeaponRight"	min = 1 }
					{ invert = true weaponSlot = "AttachmentSlots.WeaponLeft"	min = 1 }
				]
			}

			{
				signalOR = 
				[
					{
						name = "CombatTargetChanged"
					}
				]
			}

			{
				spatialAND	= 
				[
					{ target = "VehicleActions.CombatTarget" coneAngle = ( -1.f, 260.f ) angleDirection = -1 }
				]
			}
		]
	}
}

DriverShoot : ShootFromVehicleAbstract
{
	activationCondition =
	{
		spatialOR	= 
		[
			{ target = "VehicleActions.CombatTarget" coneAngle = ( -1, 180.f ) }
			{ target = "VehicleActions.CombatTarget" angleDirection = -1 coneAngle = ( -1, 360.f ) }
		]

		slotOR =
		[
			{ slot = "AttachmentSlots.WeaponLeft"	itemCategory = "ItemCategory.Weapon" itemTag = "RangedWeapon" }
		]
		
		ammoCountOR =
		[
			{ weaponSlot = "AttachmentSlots.WeaponLeft"		min = 1 }
		]
		
		target = 
		{
			target = "VehicleActions.CombatTarget"
		}

		squadAND = [ { invert = true hasTickets = [ "AITicketType.Equip" ] } ]
	}

	startup	= 
	{ 
		duration = 0.3f	
		changeNPCState = { upperBodyState = "Shoot" locomotionMode = "Static" }
	}

	loop = 
	{ 
		changeNPCState = { upperBodyState = "Shoot" locomotionMode = "Static" }
		toNextPhaseCondition = 
		[
			{
				ammoCountOR =
				[
					{ invert = true weaponSlot = "AttachmentSlots.WeaponLeft"	min = 1 }
				]
			}

			{
				signalOR = 
				[
					{
						name = "CombatTargetChanged"
					}
				]
			}

			{
				spatialAND =
				[
					{ target = "VehicleActions.CombatTarget" coneAngle = ( 180.f, -1.f ) }
					{ target = "VehicleActions.CombatTarget" angleDirection = 1 coneAngle = ( -1, 360.f ) }
				]
			}
		]
	}
	
	recovery = 
	{
		duration = 0.3f	
		changeNPCState = { upperBodyState = "Shoot" locomotionMode = "Static" }
	}
}

ShootFromVehicleToVehicle : ShootFromVehicleAbstract
{
	activationCondition =
	{
		slotOR =
		[
			{ slot = "AttachmentSlots.WeaponRight"	itemCategory = "ItemCategory.Weapon" itemTag = "RangedWeapon" }
			{ slot = "AttachmentSlots.WeaponLeft"	itemCategory = "ItemCategory.Weapon" itemTag = "RangedWeapon" }
		]
		
		ammoCountOR =
		[
			{ weaponSlot = "AttachmentSlots.WeaponRight"	min = 1 }
			{ weaponSlot = "AttachmentSlots.WeaponLeft"		min = 1 }
		]
		
		target = 
		{
			target = "VehicleActions.CombatTarget"
		}

		//squadAND = [ { invert = true hasTickets = [ "AITicketType.Equip" ] } ]

		vehicleAND =
		[
			{ vehicle = "VehicleActions.CombatTarget" }
		]
	}
}

ShootFromVehicleToTargetOnFoot : ShootFromVehicleAbstract
{
	activationCondition =
	{
		slotOR =
		[
			{ slot = "AttachmentSlots.WeaponRight"	itemCategory = "ItemCategory.Weapon" itemTag = "RangedWeapon" }
			{ slot = "AttachmentSlots.WeaponLeft"	itemCategory = "ItemCategory.Weapon" itemTag = "RangedWeapon" }
		]
		
		ammoCountOR =
		[
			{ weaponSlot = "AttachmentSlots.WeaponRight"	min = 1 }
			{ weaponSlot = "AttachmentSlots.WeaponLeft"		min = 1 }
		]

		target = { target = "VehicleActions.CombatTarget" maxVisibilityToTargetDistance = 5.f }

		//squadAND = [ { invert = true hasTickets = [ "AITicketType.Equip" ] } ]

		vehicleAND =
		[
			{ invert = true vehicle = "VehicleActions.CombatTarget" }
			{ invert = true vehicle = "AIActionTarget.MountedVehicle" hasTags = [ "Motorcycle" ] }
		]
	}
}

ShootFromVehicleAbstract : AIAction
{
	startup	= 
	{ 
		duration = 0.833f	
		changeNPCState = { upperBodyState = "Shoot" locomotionMode = "Static" }

		toNextPhaseConditionCheckInterval = 0.1f
		toNextPhaseCondition = 
		[
			"VehicleActions.FastReactionCondition"
		]
	}

	loop = 
	{ 
		changeNPCState = { upperBodyState = "Shoot" locomotionMode = "Static" }
		toNextPhaseCondition = 
		[
			{
				ammoCountOR =
				[
					{ invert = true weaponSlot = "AttachmentSlots.WeaponRight"	min = 1 }
					{ invert = true weaponSlot = "AttachmentSlots.WeaponLeft"	min = 1 }
				]
			}

			{
				signalOR = 
				[
					{
						name = "CombatTargetChanged"
					}
				]
			}
		]
	}
	
	recovery = 
	{
		duration = 1.066f
		changeNPCState = { upperBodyState = "Shoot" locomotionMode = "Static" }
	}

	loopSubActions = 
	[
		{
			weaponSlots = [ "AttachmentSlots.WeaponRight" , "AttachmentSlots.WeaponLeft" ]
			target = { targetSlot = "Head" } : VehicleActions.CombatTarget
			delay = 0.0f
			pauseCondition = 
			[
				{
					friendlyFire = {}
				}
			]

		} : AISubActionShootWithWeapon
	]
	
	animData =
	{
		animFeature = "ShootAction"
	}
	
	lookats =
	[
		"VehicleActions.BodyAttachedLookat"
		"VehicleActions.RightHandRifleLookat"
		"VehicleActions.RightHandLookat"
		"VehicleActions.LeftHandLookat"
	]
}

////////////////////////////////////////////////////////
//////////////////////////RELOADS///////////////////////
////////////////////////////////////////////////////////

ReloadSelector : AIActionSelector
{
	activationCondition =
	{
		condition = 
		{
			OR =
			[
				{
					AND =
					[
						{ slot = "AttachmentSlots.WeaponRight"	itemCategory = "ItemCategory.Weapon"	} : AISlotCond
						{ weaponSlot = "AttachmentSlots.WeaponRight" percentage = ( -1.f, 0.3f )		} : AIAmmoCountCond
					]
				} : AIActionAND

				{
					AND =
					[
						{ slot = "AttachmentSlots.WeaponLeft"	itemCategory = "ItemCategory.Weapon"	} : AISlotCond
						{ weaponSlot = "AttachmentSlots.WeaponLeft" percentage = ( -1.f, 0.3f )			} : AIAmmoCountCond
					]
				} : AIActionAND
			]
		} : AIActionOR
	}

	actions =
	[
		"VehicleActions.ReloadMotorcycle"
		"VehicleActions.ReloadDriver"
		"VehicleActions.ReloadPassenger"
	]
}

ReloadMotorcycle : AIAction
{
	activationCondition =
	{
		condition = 
		{
			AND =
			[
				{ vehicle = "AIActionTarget.MountedVehicle" hasTags = [ "Motorcycle" ] } : AIVehicleCond
			]
		} : AIActionAND
	}

	startup		= { duration = 0.320f } : VehicleActions.ReloadStartup
	loop		= { duration = 0.400f } : VehicleActions.ReloadLoop
	recovery	= { duration = 0.800f } : VehicleActions.ReloadRecovery
	
	loopSubActions = 
	[
		{ weaponSlot = "AttachmentSlots.WeaponRight"	duration = 0.3f } : AISubActionReloadWeapon
		{ weaponSlot = "AttachmentSlots.WeaponLeft"		duration = 0.3f } : AISubActionReloadWeapon
	]
	
	animData = { animFeature = "ReloadAction" }
}

ReloadDriver : AIAction
{
	activationCondition =
	{
		condition = 
		{
			AND =
			[
				{} : AIDriverCond
				{ invert = true vehicle = "AIActionTarget.MountedVehicle" hasTags = [ "Motorcycle" ] } : AIVehicleCond
			]
		} : AIActionAND
	}

	//startup		= { duration = 0.400f } : VehicleActions.ReloadStartup
	loop		= { duration = 1.3f } : VehicleActions.ReloadLoop
	//recovery	= { duration = 0.400f } : VehicleActions.ReloadRecovery
	
	loopSubActions = 
	[
		{ weaponSlot = "AttachmentSlots.WeaponRight"	duration = 0.3f } : AISubActionReloadWeapon
		{ weaponSlot = "AttachmentSlots.WeaponLeft"		duration = 0.3f } : AISubActionReloadWeapon
	]
	
	animData = { animFeature = "ReloadAction" }
}

ReloadPassenger : AIAction
{
	activationCondition =
	{
		condition = 
		{
			AND =
			[
				{ invert = true } : AIDriverCond
				{ invert = true vehicle = "AIActionTarget.MountedVehicle" hasTags = [ "Motorcycle" ] } : AIVehicleCond
			]
		} : AIActionAND
	}

	//startup		= { duration = 0.800f } : VehicleActions.ReloadStartup
	loop		= { duration = 2.0f } : VehicleActions.ReloadLoop
	//recovery	= { duration = 0.800f } : VehicleActions.ReloadRecovery
	
	loopSubActions = 
	[
		{ weaponSlot = "AttachmentSlots.WeaponRight"	duration = 1.f } : AISubActionReloadWeapon
		{ weaponSlot = "AttachmentSlots.WeaponLeft"		duration = 1.f } : AISubActionReloadWeapon
	]
	
	animData = { animFeature = "ReloadAction" }
}

ReloadStartup : AIActionPhase
{
	duration = 0.32f
	changeNPCState = { upperBodyState = "Reload" }
}

ReloadLoop : AIActionPhase
{ 
	duration = 0.4f 
	repeat = 0
	changeNPCState = { upperBodyState = "Reload" } 
	notRepeatPhaseCondition = 
	[
		{
			condition = 
			{
				OR =
				[
					{
						AND =
						[
							{ slot = "AttachmentSlots.WeaponRight"	itemCategory = "ItemCategory.Weapon"	} : AISlotCond
							{ weaponSlot = "AttachmentSlots.WeaponRight" percentage = ( 1.f, -1.f )		} : AIAmmoCountCond
						]
					} : AIActionAND

					{
						AND =
						[
							{ slot = "AttachmentSlots.WeaponLeft"	itemCategory = "ItemCategory.Weapon"	} : AISlotCond
							{ weaponSlot = "AttachmentSlots.WeaponLeft" percentage = ( 1.f, -1.f )			} : AIAmmoCountCond
						]
					} : AIActionAND
				]
			} : AIActionOR
		}
	]
}

ReloadRecovery : AIActionPhase
{
	duration = 0.8f
	changeNPCState = { upperBodyState = "Reload" }
}

////////////////////////////////////////////////////////
//////////////////////////LOOKATS///////////////////////
////////////////////////////////////////////////////////

EyesHeadLookat : AIActionLookAtData
{
	preset = "LookatPreset.VehicleCombatLookAtPreset"
	target = { targetSlot = "Head" } : VehicleActions.CombatTarget
}

BodyAttachedLookat : AIActionLookAtData
{
	activationCondition =
	{
		slotAND =
		[
			{ slot = "AttachmentSlots.WeaponRight" itemType = "ItemType.Wea_Rifle" }
		]
	}

	preset =
	{
		softLimitDegrees = 360.000000
		hardLimitDegrees = 360.000000
	} : LookatPreset.Gang_Rifle_Body_LootAt
	target = { targetSlot = "Head" } : VehicleActions.CombatTarget
}

RightHandRifleLookat : AIActionLookAtData
{
	activationCondition =
	{
		slotOR =
		[
			{ slot = "AttachmentSlots.WeaponRight"  itemType = "ItemType.Wea_Rifle"  }
		]
	}
	preset =
	{
		softLimitDegrees = 360.000000
		hardLimitDegrees = 360.000000
	} : LookatPreset.Gang_Rifle_Arms_LootAt
	target = "VehicleActions.CombatTarget"
}


RightHandLookat : AIActionLookAtData
{
	activationCondition =
	{
		slotOR =
		[
			{ slot = "AttachmentSlots.WeaponRight" itemType = "ItemType.Wea_Handgun"	}
			{ slot = "AttachmentSlots.WeaponRight" itemType = "ItemType.Wea_Revolver"	}
		]
	}
	preset =
	{
		softLimitDegrees = 360.000000
		hardLimitDegrees = 360.000000
		followingSpeedFactorOverride = 7200.f
		transitionSpeed = 7200.f
	} : LookatPreset.RightHand
	target = "VehicleActions.CombatTarget"
}

LeftHandLookat : AIActionLookAtData
{
	activationCondition =
	{
		slotOR =
		[
			{ slot = "AttachmentSlots.WeaponLeft" }
		]
	}
	preset =
	{
		softLimitDegrees = 360.000000
		hardLimitDegrees = 360.000000
	} : LookatPreset.LeftHand
	target = "VehicleActions.CombatTarget"
}

DoNothingIfFastReaction : AIAction
{
	activationCondition = "VehicleActions.FastReactionCondition"

	loop = {}
}

FastReactionCondition : AIActionCondition
{
	vehicleOR =
	[
		{ hasTags = [ 'FastReaction' ] } : AIVehicleCond
	]
}