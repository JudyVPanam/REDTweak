package SpecialActions
using RTDB

HealStatusEffectsSequence: AIActionSmartComposite
{
	type = "AISmartCompositeType.Sequence"

	nodes =
	[
		//"SpecialActions.ForceUnequip"
		"SpecialActions.HealStatusEffects"
	]
}

ForceUnequip : AIAction
{
	tickets = [ "AITicketType.Equip" ]
	
	loop = { duration = 1.333f changeNPCState = { upperBodyState = "Equip" } }
	
	loopSubActions = 
	[
		{ attachmentSlot = "AttachmentSlots.WeaponRight" animationTime = 1.333f delay = -1.f } : AISubActionForceUnequip
		{ attachmentSlot = "AttachmentSlots.WeaponLeft" animationTime = 1.333f delay = -1.f } : AISubActionForceUnequip
	]
}

HealStatusEffects : AIAction
{
	activationCondition =
	{	
		condition = 
		{
			OR =
			[
				{ statusEffect = "BaseStatusEffect.CrippledLegLeft" } : AIStatusEffectCond
				{ statusEffect = "BaseStatusEffect.CrippledLegRight" } : AIStatusEffectCond
				{ statusEffect = "BaseStatusEffect.CrippledArmLeft" } : AIStatusEffectCond
				{ statusEffect = "BaseStatusEffect.CrippledHandLeft" } : AIStatusEffectCond
				{ statusEffect = "BaseStatusEffect.CrippledArmRight" } : AIStatusEffectCond
				{ statusEffect = "BaseStatusEffect.CrippledHandRight" } : AIStatusEffectCond
			]
		} : AIActionOR
	}

	startup		= { duration = 1.66f  changeNPCState = { upperBodyState = "Taunt" } }
	loop		= { duration = 1.66f  changeNPCState = { upperBodyState = "Taunt" } }
	recovery	= { duration = 1.93f  changeNPCState = { upperBodyState = "Taunt" } }

	animData = { animFeature = "Heal_StatusEffect" }

	allowBlendDuration = 0.567f

	loopSubActions = 
	[
		{ 
			statusEffects = 
			[ 
				"BaseStatusEffect.CrippledLegLeft", 
				"BaseStatusEffect.CrippledLegRight", 
				"BaseStatusEffect.CrippledArmLeft",
				"BaseStatusEffect.CrippledHandLeft", 
				"BaseStatusEffect.CrippledArmRight", 
				"BaseStatusEffect.CrippledHandRight" 
			] 
			apply = false remove = true  
		} : AISubActionStatusEffect

		{
			statPool = "BaseStatPools.Health"
			amount = 100.f
			perc = true
		} : AISubActionModifyStatPool
	]
}

HealWoundedState : AIAction
{
	activationCondition =
	{	
		condition = 
		{
			OR =
			[
				{ statusEffect = "BaseStatusEffect.CrippledLegLeft" } : AIStatusEffectCond
				{ statusEffect = "BaseStatusEffect.CrippledLegRight" } : AIStatusEffectCond
				{ statusEffect = "BaseStatusEffect.CrippledArmLeft" } : AIStatusEffectCond
				{ statusEffect = "BaseStatusEffect.CrippledHandLeft" } : AIStatusEffectCond
				{ statusEffect = "BaseStatusEffect.CrippledArmRight" } : AIStatusEffectCond
				{ statusEffect = "BaseStatusEffect.CrippledHandRight" } : AIStatusEffectCond
			]
		
		} : AIActionOR
	}

	startup		= { duration = 2.f useDurationFromAnimSlot  = false changeNPCState = { upperBodyState = "Taunt" } }
	loop		= { duration = 2.f useDurationFromAnimSlot  = false changeNPCState = { upperBodyState = "Taunt" } }
	recovery	= { duration = 1.7f useDurationFromAnimSlot  = false changeNPCState = { upperBodyState = "Taunt" } }

	animData = { animFeature = "Heal_StatusEffect" animVariation = 1 }

	recoverySubActions = 
	[
		{ 
			apply = false remove = true  
			statusEffects = 
			[ 
				"BaseStatusEffect.CrippledLegLeft" , "BaseStatusEffect.CrippledLegRight", 
				"BaseStatusEffect.CrippledArmLeft" , "BaseStatusEffect.CrippledHandLeft", 
				"BaseStatusEffect.CrippledArmRight", "BaseStatusEffect.CrippledHandRight" 
			] 			
		} : AISubActionStatusEffect
		//{ delay = 0.1f cooldowns = [ { name = "HealWoundedState" duration = 1.f } ] } : AISubActionStartCooldown		
	]

	allowBlendDuration 		= 0.8f
}

CombatStimCooldown : AIActionCooldown
{
	name = "CombatStim"
	duration = 150.f 
}

UseCombatStimConsumable : AIAction
{
	cooldowns = [ "SpecialActions.CombatStimCooldown" ]

	activationCondition =
	{	
		ability = [ { abilities = [ "Ability.CanUseCombatStims" ] } ]

		statPoolOR =
		[
			{ statPool = "BaseStatPools.Health" percentage = ( -1.f, 33.f ) }
		]

		workspot = { isInWorkspot = 0 }
		
		goToCover = { desiredCover = "AIActionTarget.CurrentCover" isCoverSelected = 0 isEnteringOrLeavingCover = 0 }

		squadAND = 
		[ 
			{ invert = true hasTickets = [ "AITicketType.Equip" ] } 
			{ invert = true hasTickets = [ "AITicketType.TakeCover" ] } 
			{ invert = true hasTickets = [ "AITicketType.CatchUp" ] }  
			{ invert = true hasTickets = [ "AITicketType.Sync" ] }  
		]

		condition = 
		{
			AND =
			[
				{ invert = true statusEffect = "BaseStatusEffect.CrippledLegLeft" } : AIStatusEffectCond
				{ invert = true statusEffect = "BaseStatusEffect.CrippledLegRight" } : AIStatusEffectCond
				{ invert = true statusEffect = "BaseStatusEffect.CrippledArmLeft" } : AIStatusEffectCond
				{ invert = true statusEffect = "BaseStatusEffect.CrippledHandLeft" } : AIStatusEffectCond
				{ invert = true statusEffect = "BaseStatusEffect.CrippledArmRight" } : AIStatusEffectCond
				{ invert = true statusEffect = "BaseStatusEffect.CrippledHandRight" } : AIStatusEffectCond
				{ invert = true statusEffect = "BaseStatusEffect.DismemberedLegLeft" } : AIStatusEffectCond
				{ invert = true statusEffect = "BaseStatusEffect.DismemberedLegRight" } : AIStatusEffectCond
				{ invert = true statusEffect = "BaseStatusEffect.DismemberedArmLeft" } : AIStatusEffectCond
				{ invert = true statusEffect = "BaseStatusEffect.DismemberedHandLeft" } : AIStatusEffectCond
				{ invert = true statusEffect = "BaseStatusEffect.DismemberedArmRight" } : AIStatusEffectCond
				{ invert = true statusEffect = "BaseStatusEffect.DismemberedHandRight" } : AIStatusEffectCond
			]
		
		} : AIActionAND
	}

	startup		= { duration = 1.f }
	loop		= { duration = 1.4f }
	recovery	= { duration = 1.f }

	animData = { animFeature = "Heal_StatusEffect" }

	allowBlendDuration = 0.567f

	loopSubActions = 
	[
		{ 
			statusEffects = 
			[ 
				"BaseStatusEffect.CombatStim"
			] 
			apply = true remove = false  
		} : AISubActionStatusEffect
	]

	recoverySubActions = 
	[
		{ name = "buff" delay = 0.f } : AISubActionPlayVoiceOver
	]
}

CoverUseCombatStimConsumable : AIAction
{
	cooldowns = [ "SpecialActions.CombatStimCooldown" ]

	activationCondition =
	{	
		ability = [ { abilities = [ "Ability.CanUseCombatStims" ] } ]

		hit = { hitTimeout = 15.f }
		
		goToCover = { desiredCover = "AIActionTarget.CurrentCover" isCoverSelected = 1 isEnteringOrLeavingCover = 0 }
	}

	startup		= { duration = 1.f }
	loop		= { duration = 1.4f }
	recovery	= { duration = 1.f }

	animData = { animFeature = "Heal_StatusEffect" }

	loopSubActions = 
	[
		{ 
			statusEffects = 
			[ 
				"BaseStatusEffect.CombatStim"
			] 
			apply = true remove = false  
		} : AISubActionStatusEffect
	]

	recoverySubActions = 
	[
		{ name = "buff" delay = 0.f } : AISubActionPlayVoiceOver
	]
}

CallReinforcements : AIAction
{
	activationCondition	=
	{
		condition = 
		{
			AND =
			[
				"Condition.AbilityCanCallReinforcements"
				"Condition.NotCommNoise"

				
				//"Condition.NotTicketEquip"
				//"Condition.NotTicketTakeCover"
				//"Condition.NotTicketCatchUp"
				//"Condition.NotTicketSync"
			]
		} : AIActionAND

		statPoolAND = 
		[
			{ invert = true statPool = "BaseStatPools.CallReinforcementProgress" percentage = ( 100.f, -1.f ) },
			{ invert = true statPool = "BaseStatPools.CallReinforcementProgress" isIncreasing = 1 }
		]
		
		goToCover = { desiredCover = "AIActionTarget.CurrentCover" isCoverSelected = 0 isEnteringOrLeavingCover = 0 }

		squadAND = 
		[ 
			{ invert = true hasTickets = [ "AITicketType.Equip" ] } 
			{ invert = true hasTickets = [ "AITicketType.TakeCover" ] } 
			{ invert = true hasTickets = [ "AITicketType.CatchUp" ] }  
			{ invert = true hasTickets = [ "AITicketType.Sync" ] }  
		]

		state = 
		[
			{
				invert 			= true
				target			= "AIActionTarget.Owner"
				inStates		= ["Attack", "Parry", "Taunt", "Block"]	
				checkAllTypes	= true
			}
		]

		spatialAND = 
		[
			{ 
				target		= "AIActionTarget.CombatTarget" 	
				distance 	= ( 5.f, -1.f )
			}
		]
	}

	startup = { duration = 0.53f }
	loop = 
	{
		duration 	= 0.867f 	
		repeat 		= 3
		toNextPhaseConditionCheckInterval = 0.25f
		toNextPhaseCondition =
		[
			{
				statPoolAND = 
				[
					{ statPool = "BaseStatPools.CallReinforcementProgress" percentage = ( 100.f, -1.f ) }
				]
			}
		]
	}
	recovery = { duration = 0.56f }
	
	subActions =
	[
		{ delay = 1.2f duration = 10.f } : AISubActionCallReinforcements,
		{ name = "InterruptMeleeAction" duration = 0.1f delay = -1.f } : AISubActionSendSignal
	]

	animData = { animFeature = "CallSquad" }
}

CoverCallReinforcements : CallReinforcements
{
	activationCondition	=
	{
		ability	= 
		[ 
			{ abilities = [ "Ability.CanCallReinforcements" ] } 
		]

		statusEffectAND =
		[
			{ invert = true statusEffect = "BaseStatusEffect.CommsNoise" }
            { invert = true statusEffect = "BaseStatusEffect.CommsNoiseLevel2" }
            { invert = true statusEffect = "BaseStatusEffect.CommsNoiseLevel3" }
            { invert = true statusEffect = "BaseStatusEffect.CommsNoiseLevel4" } 
		]

		statPoolAND = 
		[
			{ invert = true statPool = "BaseStatPools.CallReinforcementProgress" percentage = ( 100.f, -1.f ) },
			{ invert = true statPool = "BaseStatPools.CallReinforcementProgress" isIncreasing = 1 }
		]

		goToCover = { desiredCover = "AIActionTarget.CurrentCover" isCoverSelected = 1 isEnteringOrLeavingCover = 0 }

		squadAND = [ { invert = true hasTickets = [ "AITicketType.Equip" ] } ]

		state = 
		[
			{
				invert 			= true
				target			= "AIActionTarget.Owner"
				inStates		= ["Attack", "Parry", "Taunt", "Block"]	
				checkAllTypes	= true
			}
		]

		spatialAND = 
		[
			{ 
				target		= "AIActionTarget.CombatTarget" 	
				distance 	= ( 5.f, -1.f )
			}
		]
	}
}

ThrowWeapon : AIAction
{
	disableAction = false
	
	ability = "Ability.CanThrowWeapon"
	
	activationCondition =
	{	
		spatialAND = 
		[
			{ target = "AIActionTarget.CombatTarget" distance = ( 5.f, -1.f ) coneAngle = ( -1.f, 360.f ) }
		]
		
		slotAND	= 
		[
			{ invert = true slot = "AttachmentSlots.WeaponRight" itemType = "ItemType.Wea_Fists"  }
		]
		
		canThrow = { target = "AIActionTarget.CombatTarget" weaponSlot = "AttachmentSlots.WeaponRight" }
	}
	
	initCooldowns = [ { name = "ThrowWeapon" duration = 10.f } ]
	cooldowns = [ { name = "ThrowWeapon" duration = 10.f } ]
	
	startup		= { duration = 0.5f }
	loop		= { duration = 1.0f }
	recovery	= { duration = 0.5f }

	animData = { animFeature = "ThrowWeapon" animSlot = { useRootMotion = false } }

	loopSubActions = 
	[
		{ attachmentSlot = "AttachmentSlots.WeaponRight" delay = 0.25f dropItemOnInterruption = true target = "AIActionTarget.CombatTarget" } : AISubActionThrowItem
	]

	recoverySubActions = 
	[
		{ attachmentSlot = "AttachmentSlots.WeaponRight" itemCategory = "ItemCategory.Weapon" itemType = "ItemType.Wea_Fists" } : AISubActionForceEquip
	]
}

EnableBerserker : AIAction
{
	disableAction = false
	
	ability = "Ability.HasBerserk"
	
	activationCondition =
	{
		statusEffectAND =
		[
			{ invert = true statusEffect = "BaseStatusEffect.Berserker" } 
		]
		
		weakSpotAND =
		[
			{ invert = true weakspot = "Weakspots.Juice_Dispenser_Back_Weakspot" }
		]
	}
	
	startupSubActions = 
	[
		{ attachmentSlot = "AttachmentSlots.WeaponRight" itemCategory = "ItemCategory.Weapon" itemType = "ItemType.Wea_Fists" } : AISubActionForceEquip
	]

	loopSubActions	= 
	[
		{ statusEffects = [ "BaseStatusEffect.Berserker" ] remove = false } : AISubActionStatusEffect
	]	
	
	startup		= { duration = 1.667f }
	loop		= { duration = 1.667f }
	recovery	= { duration = 1.9f }

	animData = { animFeature = "EnableBerserker" animVariation = 0  animSlot = { useRootMotion = false } }
}

// GRAB
MeleeGrabSequenceDefinition : AIActionSequence
{
	actions = 
	[
		"MeleeActions.SingleMeleeAttackCharge02TauntDefinition",
		"SpecialActions.SpecialGrabDefinition"
	]
}

MeleeGrabSequenceConditionDefinition : AIAction
{
	ability = "Ability.CanGrab"
	
	cooldowns = [ { name = "SpecialGrab" duration = 30.f } ]
	
	activationCondition =
	{
		spatialAND = 
		[
			{ target = "AIActionTarget.CombatTarget" distance = ( -1, -1.f ) }
		]
		
		slotAND	= 
		[
			{ slot = "AttachmentSlots.WeaponRight" itemType = "ItemType.Wea_Fists"  }
		]
	}
}
SpecialGrabDefinition : AIAction
{	
	cooldowns = [ { name = "SpecialGrab" duration = 30.f } ]
	
	loop =
	{
		movePolicy = { target = "AIActionTarget.CombatTarget" movementType = "Sprint" dontUseStop = true }

		toNextPhaseCondition =
		[
			{
				spatialAND = 
				[
					{ target = "AIActionTarget.CombatTarget" distance = ( -1.f, 5.f ) }
				]
			}
		]
	}

	recovery	= { duration = 0.5f  movePolicy = { target = "AIActionTarget.CombatTarget" movementType = "Sprint" dontUseStop = true } }
	
	startupSubActions = 
	[
		{ upperBodyState = "Attack" } : AIActionChangeNPCState
	]
	
	recoverySubActions = 
	[
		{ weaponSlots = [ "AttachmentSlots.WeaponRight" ] attackTime = 0.f attackDuration = 0.3f attack = "NPCAttacks.GrabAttack" attackRange = 4.f } : AISubActionAttackWithWeapon
	]
}

PickWeaponSequenceDefinition : AIActionSequence
{
	actions = [ "SpecialActions.GoToWeaponDefinition", "SpecialActions.PickWeaponDefinition"  ]

	hasMemory = false
}

GoToWeaponDefinition : AIAction
{
	ability = "Ability.CanPickUpWeapon"
	
	activationCondition =
	{
		spatialAND = 
		[
			{ target = "AIActionTarget.TargetItem" distance = ( -1.f 30.f ) }
		]
		
		slotAND	= 
		[
			{ slot = "AttachmentSlots.WeaponRight" itemType = "ItemType.Wea_Fists"  }
		]
		
		statusEffectAND =
		[
			{ invert = true statusEffect = "BaseStatusEffect.Berserker" } 
		]
	}
	
	cooldowns = [ { name = "ThrowWeapon" duration = 25.f } ]
	
	loop =
	{
		movePolicy = { target = "AIActionTarget.TargetItem" movementType = "Sprint" tolerance = 3.0 ignoreNavigation = true }
		toNextPhaseCondition =
		[
			{
				spatialOR = 
				[
					{ target = "AIActionTarget.TargetItem" distance = ( -1.f 1.5f ) }
					{ target = "AIActionTarget.TargetItem" distance = ( 15.f, -1.f ) }
				]
			}
		]
	}
}

PickWeaponDefinition : AIAction
{	
	ability = "Ability.CanPickUpWeapon"
	
	activationCondition =
	{
		spatialAND = 
		[
			{ target = "AIActionTarget.TargetItem" distance = ( -1.f 3.f ) }
		]
		
		slotAND	= 
		[
			{ slot = "AttachmentSlots.WeaponRight" itemType = "ItemType.Wea_Fists"  }
		]
		
		statusEffectAND =
		[
			{ invert = true statusEffect = "BaseStatusEffect.Berserker" } 
		]
	}
	
	animData = { animFeature = "PickUpWeapon" animSlot = { useRootMotion = false } }
	
	startup = 	{ duration = 1.3f  }
	loop =		{ duration = 1.3f  }
	recovery =	{ duration = 0.767f }
	
	loopSubActions = 
	[
		{ delay = 0.5f attachmentSlot = "AttachmentSlots.WeaponRight" itemType = "ItemType.Wea_Hammer" } : AISubActionForceEquip
	]
}

EnableTiredDefinition : AIAction
{	
	activationCondition =
	{
		slotAND	= 
		[
			{ slot = "AttachmentSlots.WeaponRight" itemType = "ItemType.Wea_Hammer"  }
		]	
		
		//statusEffectAND =
		//[
		//	{ invert = true statusEffect = "BaseStatusEffect.Exhausted" } 
		//]
	}
	
	initCooldowns = [ { name = "Tired" duration = 30.f } ]
	cooldowns = [ { name = "Tired" duration = 30.f } ]

	loop = 
	{ 
		duration = 5.967 
		changeNPCState = { upperBodyState = "Attack" }
	}

	animData = { animFeature = "Tired" animVariation = 0  animSlot = { useRootMotion = false } }
	
	/*subActions = 
	[
		{ statusEffects = [ "BaseStatusEffect.Exhausted" ] } : AISubActionStatusEffect
	]*/
}

SpecialTaunt01Definition : AIAction 
{
	startup		= { duration = 0.5f changeNPCState = { upperBodyState = "Attack" } }
	loop		= { duration = 0.567f changeNPCState = { upperBodyState = "Attack" } }
	recovery	= { duration = 0.6f changeNPCState = { upperBodyState = "Attack" } }

	animData = { animFeature = "Taunt" animSlot = {} }
}
SpecialTaunt02Definition : AIAction 
{
	startup		= { duration = 0.5f changeNPCState = { upperBodyState = "Attack" } }
	loop		= { duration = 0.567f changeNPCState = { upperBodyState = "Attack" } }
	recovery	= { duration = 0.6f changeNPCState = { upperBodyState = "Attack" } }

	animData = { animFeature = "Taunt" animSlot = {} }
}
SpecialTaunt03Definition : AIAction 
{
	startup		= { duration = 0.5f changeNPCState = { upperBodyState = "Attack" } }
	loop		= { duration = 0.567f changeNPCState = { upperBodyState = "Attack" } }
	recovery	= { duration = 0.6f changeNPCState = { upperBodyState = "Attack" } }

	animData = { animFeature = "Taunt" animSlot = {} }
}

TauntHiddenPlayer : AIAction
{
	cooldowns = [ { name = "TauntHiddenPlayer" duration = 5.f }	]

	activationCondition =
	{
		condition =
		{
			AND = 
			[
				"Condition.NotMinAccuracyValue0dot85"
				"Condition.NotIsFollower"
			]
		} : AIActionAND
	}

	subActions =
	[
		{ name = "taunt_hidden_player" } : AISubActionPlayVoiceOver
	]
}

ActivateKerenzikovComposite : AIActionSmartComposite
{
	type = "AISmartCompositeType.Selector"
	repeat = 0

	nodes =
	[
		"SpecialActions.ActivateKerenzikovDefinition"
		"GenericArchetype.Success"
	]
}

ActivateKerenzikovDefinition : AIAction
{
	activationCondition	=
	{
		condition = 
		{
			AND = 
			[
				"Condition.AbilityHasKerenzikov"
				"Condition.NotStatusEffectSandevistanBuff"
				"Condition.TargetPOVBelow90deg"
				"Condition.NotTicketEquip"
				"Condition.NotMeleeStatesConditions"
				"Condition.NotInCover"
				"Condition.NotIsEnteringCover"
				"Condition.NotIsUsingOffMeshLink"
			]
		} : AIActionAND
	}

	//loop		= { duration = 1.7f changeNPCState = { hitReactionMode = "Unstoppable" upperBodyState = "Taunt" } }
	loop		= { duration = 0.f changeNPCState = { upperBodyState = "Taunt" } }

	//animData = { weaponOverride = 2 animFeature = "KerenzikovActivation" animVariation = 2 animSlot = { loopSlide = "DashAndDodgeActions.StepsSlideData" } }

	subActions	= 
	[
		{ statusEffects = [ "BaseStatusEffect.SandevistanBuff" ] remove = false } : AISubActionStatusEffect
		{ name = "InterruptMeleeAction" duration = 0.1f delay = -1.f } : AISubActionSendSignal
	]
}

ActivateSandevistanDefinition : AIAction
{
	activationCondition	=
	{
		condition = 
		{
			AND = 
			[
				"Condition.AbilityHasSandevistan"
				"Condition.NotStatusEffectSandevistanBuffHumanoidRanged"
				"Condition.TargetPOVBelow90deg"
				"Condition.NotTicketEquip"
				"Condition.HasRangedWeapon"
				"Condition.NotMeleeStatesConditions"
				"Condition.NotInCover"
				"Condition.NotIsEnteringCover"
				"Condition.NotIsUsingOffMeshLink"
			]
		} : AIActionAND
	}

	//startup = { duration = 0.1f changeNPCState = { upperBodyState = "Taunt" } }
	loop = { duration = 0.1f changeNPCState = { upperBodyState = "Taunt" } }
	//recovery = { duration = 0.1f changeNPCState = { upperBodyState = "Taunt" } }

	animData = { weaponOverride = 1	animFeature = "KerenzikovActivation" animSlot = { loopSlide = "DashAndDodgeActions.StepsSlideData" } }

	subActions	= 
	[
		{ statusEffects = [ "BaseStatusEffect.SandevistanBuffHumanoidRanged" ] remove = false } : AISubActionStatusEffect
		{ name = "InterruptMeleeAction" duration = 0.1f delay = -1.f } : AISubActionSendSignal
	]
}


RecoverFromRagdoll : AIActionSelector
{
	actions =
	[
		"SpecialActions.RecoverToDefeated"
		"SpecialActions.RecoverToUnconscious"

		// the orientation designations here are in relation to the NPC's entity orientation, which might be different to what you might expect it to be visually
		// while ragdolling, the NPC's entity orientation is set in such a way that up is always world up, and forward is pointed from the hips *away* from the head (hipsPos - headPos)
		"SpecialActions.RecoverToStandBack"
		"SpecialActions.RecoverToStandLeft"
		"SpecialActions.RecoverToStandRight"
		"SpecialActions.RecoverToStandFront"
	]

	disableActionsLimit = true
}

// if the NPC is outside of the navmesh, we will need to slide them back to the navmesh
// due to this, they cannot be using root motion when playing the recovery animation
// unfortunately, the best way to do that is to simply duplicate actions
SlideRecoverFromRagdoll : AIActionSelector
{
	actions =
	[
		"SpecialActions.RecoverToDefeated"
		"SpecialActions.RecoverToUnconscious"

		// the orientation designations here are in relation to the NPC's entity orientation, which might be different to what you might expect it to be visually
		// while ragdolling, the NPC's entity orientation is set in such a way that up is always world up, and forward is pointed from the hips *away* from the head (hipsPos - headPos)
		"SpecialActions.SlideRecoverToStandBack"
		"SpecialActions.SlideRecoverToStandLeft"
		"SpecialActions.SlideRecoverToStandRight"
		"SpecialActions.SlideRecoverToStandFront"
	]
	
	disableActionsLimit = true
}

RecoverToDefeated : AIAction
{
	failIfAnimationNotStreamedIn = false

	activationCondition	=
	{
		condition =
		{
			OR =
			[
				"Condition.StatusEffectDefeated"
				"Condition.StatusEffectDefeatedWithRecover"
			]
		} : AIActionOR
	}

	startup	= { duration = 2.133f }
	loop	= { duration = 8.167f }
	
	animData = 
	{ 
		animFeature = "SE_RagdollDefeatedRecovery" 
		animSlot = 
		{		
			usePoseMatching = true
			resetRagdollOnStart = true
		} 
		ragdollOnDeath	= true 
		animVariation  = 1
	}
}

RecoverToUnconscious : AIAction
{
	failIfAnimationNotStreamedIn = false

	activationCondition	=
	{
		condition =
		{
			OR =
			[
				"Condition.StatusEffectUnconscious"
				"Condition.InStatesUnconscious"
			]
		} : AIActionOR
	}

	loop = { duration = 3.333f }
	
	animData = 
	{ 
		animFeature = "RecoverFromRagdollToUnconscious" 
		animSlot = 
		{ 
			useRootMotion = true			
			usePoseMatching = true
			resetRagdollOnStart = true
		} 
		ragdollOnDeath	= true
	}
}

RecoverToStandFront : AIAction
{
	failIfAnimationNotStreamedIn = false

	loop	 = 
	{ 
		duration = 1.8f
		toNextPhaseCondition = 
		[
			{
				statusEffectAND =
				[
					{ statusEffect = "BaseStatusEffect.Defeated" } 
				]
			}

			{
				statPoolOR =
				[
					{ statPool = "BaseStatPools.Health" percentage = ( -1.f, 0.f ) }
				]
			}
		]
	}

	animData = 
	{
		animFeature = "StandUpFromRagdollFront"
		ragdollOnDeath	= true 
		animSlot = 
		{ 	
			usePoseMatching = true
			resetRagdollOnStart = true
		} 
	}
}

SlideRecoverToStandFront : RecoverToStandFront
{
	animData = 
	{
		animFeature = "StandUpFromRagdollFront"
		ragdollOnDeath	= true 
		animSlot = 
		{ 	
			useRootMotion = false
			usePoseMatching = true
			resetRagdollOnStart = true
		} 
	}
}

RecoverToStandLeft : RecoverToStandFront
{
	activationCondition =
	{
		condition =
		{
			AND =
			[
				"Condition.StimTarget"
				"Condition.StimTargetBelow40m"
				"Condition.StimTargetAbove90degLeft"
				"Condition.StimTargetBelow270degLeft"
			]
		} : AIActionAND
	}

	animData = 
	{
		animFeature = "StandUpFromRagdollLeft"
		ragdollOnDeath	= true 
		animSlot = 
		{ 	
			usePoseMatching = true
			resetRagdollOnStart = true
		} 
	}
}

SlideRecoverToStandLeft : RecoverToStandLeft
{
	animData = 
	{
		animFeature = "StandUpFromRagdollLeft"
		ragdollOnDeath	= true 
		animSlot = 
		{ 	
			useRootMotion = false
			usePoseMatching = true
			resetRagdollOnStart = true
		} 
	}
}

RecoverToStandRight : RecoverToStandFront
{
	activationCondition =
	{
		condition =
		{
			AND =
			[
				"Condition.StimTarget"
				"Condition.StimTargetBelow40m"
				"Condition.StimTargetAbove90degRight"
				"Condition.StimTargetBelow270degRight"
			]
		} : AIActionAND
	}
	
	animData = 
	{
		animFeature = "StandUpFromRagdollRight"
		ragdollOnDeath	= true 
		animSlot = 
		{ 		
			usePoseMatching = true
			resetRagdollOnStart = true
		} 
	}
}

SlideRecoverToStandRight : RecoverToStandRight
{	
	animData = 
	{
		animFeature = "StandUpFromRagdollRight"
		ragdollOnDeath	= true 
		animSlot = 
		{ 	
			useRootMotion = false
			usePoseMatching = true
			resetRagdollOnStart = true
		} 
	}
}

RecoverToStandBack : RecoverToStandFront
{
	activationCondition =
	{
		condition =
		{
			AND =
			[
				"Condition.StimTarget"
				"Condition.StimTargetBelow40m"
				"Condition.StimTargetAbove270deg"
			]
		} : AIActionAND
	}
	
	animData = 
	{
		animFeature = "StandUpFromRagdollBack"
		ragdollOnDeath	= true 
		animSlot = 
		{	
			usePoseMatching = true
			resetRagdollOnStart = true
		} 
	}
}

SlideRecoverToStandBack : RecoverToStandBack
{
	activationCondition =
	{
		condition =
		{
			AND =
			[
				"Condition.StimTarget"
				"Condition.StimTargetBelow40m"
				"Condition.StimTargetAbove270deg"
			]
		} : AIActionAND
	}
	
	animData = 
	{
		animFeature = "StandUpFromRagdollBack"
		ragdollOnDeath	= true 
		animSlot = 
		{	
			useRootMotion = false
			usePoseMatching = true
			resetRagdollOnStart = true
		} 
	}
}

//---------------------------------------------------------------------------------------------------------------
// RECOVER FROM KNOCKDOWN
//---------------------------------------------------------------------------------------------------------------
 
RecoverFromKnockdownHitReaction : AIAction
{
	failIfAnimationNotStreamedIn = false

	loop	 = { duration = 1.8f }

	animData = 
	{ 
		animFeature = "RecoverFromKnockdown"
		animVariationSubAction = {} : AISubActionHitData
		ragdollOnDeath	= true 
		animSlot = 
		{ 
			useRootMotion = true			
		} 
	}
}


//---------------------------------------------------------------------------------------------------------------

InterruptMeleeActionOnSignal : AIAction
{
	loop =
	{
		toNextPhaseCondition = 
		[
			{
				signalOR = 
				[
					"MeleeActions.InterruptMeleeAction"
				]
			}
		]
	}
}
