package TutorialMeleeAtkStrong
using RTDB
  
Map : ActionMap
{   
    defaultMap =
    {
        map = 
        [
            { node = "GenericArchetype.MeleeCloseAttackLight01Condition"	isOverriddenBy = "" }
            { node = "GenericArchetype.MeleeCloseAttackLight02Condition"	isOverriddenBy = "" }

            { node = "GenericArchetype.MeleeAttackLight01Condition"         isOverriddenBy = "" }
            { node = "GenericArchetype.MeleeAttackLight02Condition"         isOverriddenBy = "" }
            { node = "GenericArchetype.MeleeAttackLight03Condition"         isOverriddenBy = "" }

            { node = "GenericArchetype.MeleeStrongSingle01Condition"        isOverriddenBy = "TutorialMeleeAtkStrong.TutorialMeleeStrongSingle01Condition" }
            { node = "GenericArchetype.MeleeStrongSingle02Condition"        isOverriddenBy = "TutorialMeleeAtkStrong.TutorialMeleeStrongSingle02Condition" } 

            { node = "GenericArchetype.MeleeChargeSingle01Condition"        isOverriddenBy = "" }
            { node = "GenericArchetype.MeleeChargeSingle02Condition"        isOverriddenBy = "" }
            { node = "GenericArchetype.MeleeChargeSingle03Condition"        isOverriddenBy = "" }

            { node = "GenericArchetype.MeleeChargeJump01Condition"          isOverriddenBy = "" } 
            { node = "GenericArchetype.MeleeChargeJump02Condition"          isOverriddenBy = "" } 

            { node = "GenericArchetype.MeleeBlockState"                     isOverriddenBy = "" }
            { node = "GenericArchetype.MeleeParryState"                     isOverriddenBy = "" }
      
			{ node = "CombatDecorator.MeleeCombatCloseDistanceDecorator"    isOverriddenBy = "TutorialMeleeAtkStrong.TutorialMeleeCombatCloseDistanceDecorator" }
			{ node = "CombatDecorator.MeleeCombatSafeDistanceDecorator"    	isOverriddenBy = "" }
			{ node = "CombatDecorator.MeleeTicketDecorator"    				isOverriddenBy = "TutorialMeleeAtkStrong.TutorialMeleeTicketDecorator" }
			{ node = "CombatDecorator.MeleeTicketNoRequestCondition"    	isOverriddenBy = "" }
			{ node = "MeleeActions.MeleeBlockDodge01ConditionDefinition" 	isOverriddenBy = "" }

			{ node = "DashAndDodgeActions.DodgeMeleeContactSelectorConditionDefinition" 	isOverriddenBy = "TutorialMeleeAtkStrong.TutorialDodgeMeleeContactSelectorConditionDefinition" }

            { node = "MovementActions.MeleeMoveToCombatContact"             isOverriddenBy = "TutorialMeleeAtkStrong.TutorialMeleeMoveToCombatContact" }
            { node = "CombatDecorator.MeleeApproachTicketDecorator"         isOverriddenBy = "TutorialMeleeAtkStrong.TutorialMeleeApproachTicketDecorator" }
        ]
    } 
}  

TutorialMeleeStrongSingle01Condition : AIAction
{
	activationCondition	=
	{
		condition = 
		{
			AND = 
			[
				{ target = "AIActionTarget.CombatTarget" } : AICalculatePathCond

				{
					OR = 
					[
						{ invert = true target = "AIActionTarget.CombatTarget" inStates = [ "Block" ] checkAllTypes = true } : AIStateCond
						{ target = "AIActionTarget.CombatTarget"  distance = ( 2.f, -1.f ) } : AISpatialCond
					]
				} : AIActionOR
			]
		} : AIActionAND
	}
}

TutorialMeleeStrongSingle02Condition : AIAction
{
	activationCondition	=
	{
		condition = 
		{
			AND = 
			[
				{ target = "AIActionTarget.CombatTarget" } : AICalculatePathCond

				{
					OR = 
					[
						{ invert = true target = "AIActionTarget.CombatTarget" inStates = [ "Block" ] checkAllTypes = true } : AIStateCond
						{ target = "AIActionTarget.CombatTarget"  distance = ( 2.f, -1.f ) } : AISpatialCond
					]
				} : AIActionOR
			]
		} : AIActionAND
	}
}

TutorialMeleeTicketDecorator : AIAction
{
	tickets = [ "AITicketType.Melee" ]	
	revokingTicketCompletesAction = false;

	activationCondition		=
	{
	}

	loop = 
	{
	}
}

TutorialDodgeMeleeContactSelectorConditionDefinition : AIAction
{
    ability = "Ability.HasDodge"

	activationCondition		= "DashAndDodgeActions.GenericMeleeDodgeConditionDefinition" 

	loop		= {}
	
	cooldowns = 
	[
		{ name = "DodgeMeleeContactCooldown" duration = 2.f }
	]	

	subActions	= 
	[
		{ delay = 0.1f cooldowns = [ { name = "DodgeMeleeContactCooldown" duration = 2.f } ] } : AISubActionStartCooldown
	]
}

TutorialMeleeApproachTicketDecorator: AIAction
{
    activationCondition	=
	{

	}

    loop	= 
	{
        duration = -1.0f
    }
}

TutorialMeleeMoveToCombatContact : AIAction
{
	activationCondition	=
	{

	}
	
	loop	= 
	{
		movePolicy = 
		{ 
			target 			= "AIActionTarget.CombatTarget" 
			strafingTarget 	= "AIActionTarget.CombatTarget"
			movementType 	= "Walk" 
			tolerance 		= 1.f 
			distance 		= 2.f 
			avoidThreatRange = 0.f
			avoidThreatCost	= 0.f
		}
	}
}


TutorialMeleeCombatCloseDistanceDecorator : AIAction
{
	activationCondition =
	{
		
	}

	loop =
	{
		toNextPhaseConditionCheckInterval = 0.1f
        toNextPhaseCondition =
		[
			{
			
				squadOR = [ { ticketsConditionCheck = [ "AITicketType.Melee" ] } ]
				
				state = 
				[
					{
						invert = true
						target	= "AIActionTarget.Owner"
						inStates	= ["Attack", "Defend", "Parry", "Taunt", "Equip"]	
						checkAllTypes	= true	
					}
				]
				calculatePath = 
				[ 
					{ target = "AIActionTarget.CombatTarget" tolerance	= 1.f } 
				]
				movementAND = [ { isUsingOffMeshLink = 0 } ]
			}
        ]
	}
}