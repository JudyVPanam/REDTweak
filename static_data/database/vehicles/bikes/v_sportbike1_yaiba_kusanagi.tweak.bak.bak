package Vehicle
using RTDB, BaseStats, Driving

v_sportbike1_yaiba_kusanagi : Vehicle_2w_Default 
{
	model = "Vehicle.Kusanagi"

	entityTemplatePath = "base\vehicles\sportbike\v_sportbike1_yaiba_kusanagi_basic_01.ent"

	visualTags = [ "Standard" ]

	manufacturer = "Vehicle.Yaiba"

	displayName = "LocKey#1620"

	vehicleUIData = "Vehicle.VehicleYaibaKusanagiUIData"
	
	fk< UIIcon > icon = "UIIcon.yaiba_kusanagi_basic_player_01"

	tags += [ 'player_bike' ]

	enableDestruction = true

	fxWheelsDecals = "Vehicle.FxWheels_Radial_M"

	destruction =
	{
		gridDimensions = ( 0.5f, 2.0f, 0.4f )
		gridLocalOffset = ( 0.0f, 0.0f, 0.2f )
		
	} : VehicleDestructionParamsDefault_2w

	vehDriver_FPPCameraParams = "Vehicle.KusanagiDriverFPPCameraParamsDefault"

	player_audio_resource = "v_mbike_yaiba_asmx_kusanagi"
	traffic_audio_resource = "v_mbike_yaiba_asmx_kusanagi_traffic"

	destroyedAppearance = "yaiba_kusanagi_basic_burnt_default_01"

	vehWheelDimensionsSetup = "Vehicle.VehicleWheelDimensionsDefaultYaibaKusanagi"
	vehDriveModelData = "Vehicle.VehicleDriveModelDataDefaultYaibaKusanagi"
	
	vehEngineData = 
	{
		engineMaxTorque = 185.f // Nm
		
		wheelsResistanceRatio = 15.f

		fk< VehicleGear >[] gears = [
			{ minSpeed = 0.f  maxSpeed = 3.f minEngineRPM = 980.f maxEngineRPM = 8448.f torqueMultiplier = 0.9f }, //R

			{ minSpeed = 0.f  maxSpeed = 12.f  minEngineRPM = 980.f maxEngineRPM = 8300.f torqueMultiplier = 1.2f }, //1
			{ minSpeed = 10.f  maxSpeed = 18.f minEngineRPM = 3900.f maxEngineRPM = 7744.f torqueMultiplier = 0.85f }, //2
			{ minSpeed = 16.f maxSpeed = 23.5f minEngineRPM = 4100.f maxEngineRPM = 7920.f torqueMultiplier = 0.64f }, //3
			{ minSpeed = 22.f maxSpeed = 29.f minEngineRPM = 4200.f maxEngineRPM = 8106.f torqueMultiplier = 0.51f }, //4
			{ minSpeed = 27.f maxSpeed = 33.f minEngineRPM = 4300.f maxEngineRPM = 8297.f torqueMultiplier = 0.4f }, //5
			{ minSpeed = 30.f maxSpeed = 36.f minEngineRPM = 4400.f maxEngineRPM = 8300.f torqueMultiplier = 0.3f }, //6
			{ minSpeed = 33.f maxSpeed = 42.f minEngineRPM = 4500.f maxEngineRPM = 8800.f torqueMultiplier = 0.23f }  //7
		]

		gearCurvesPath = "base\gameplay\vehicles\curves\yaiba_kusanagi_gears.curveset"

		minRPM = 980.f //engine stalling
		maxRPM = 8800.f //redline

		gearChangeTime = 0.2f

		flyWheelMomentOfInertia = 0.2f

		resistanceTorque = 100.f //"Brake" torque applied when there's no gas input
	} : VehicleEngineData_2_Default
}

VehicleWheelDimensionsDefaultYaibaKusanagi : VehicleWheelDimensionsSetup
{
	frontPreset =
	{
		tireRadius = 0.305f
		rimRadius = 0.30f
		tireWidth = 0.14f
	} : VehicleWheelDimensionsPreset_Default

	backPreset = 
	{
		tireRadius = 0.345f
		rimRadius = 0.30f
		tireWidth = 0.25f
	} : VehicleWheelDimensionsPreset_Default
}

VehicleDriveModelDataDefaultYaibaKusanagi : VehicleDriveModelDataDefault_2w
{
	total_mass = 500.0f
	chassis_mass = 500.f

	//Note that the Kusanagi actually has a rearward weight balance with 0 set for Y here. Its collider sits far back compared to its 
	//wheelbase so 0 is not true 50:50 weight distribution when it comes to wheel loads in game.
	center_of_mass_offset = ( 0.0f, 0.18f, 0.12f )

	momentOfInertia = ( 254.0f, 48.0f, 228.0f )

	maxWheelTurnDeg = 54.f
	wheelTurnMaxAddPerSecond = 135.f
	wheelTurnMaxSubPerSecond = 135.f

	bikeTiltPID = [ 2.5f, 0.2f, 0.0f ]
	bikeTiltSpeed = 105.f
	bikeTiltReturnSpeed = 105.f
	bikeMaxTilt = 36.f

	handbrakeBrakingTorque = 4000.0f

	burnOutRotationModifier = 0.0007f
	
	airResistanceFactor = 0.8f

	wheelSetup = 
	{
		frontPreset =
		{
			maxBrakingTorque = 1000.0f

			frictionMulLongitudinal = 0.89f
			frictionMulLateral = 0.9f
			tireLongitudinalSlipEffectsMul = 0.75f
			tireLateralSlipEffectsMul = 0.75f

			springStiffness = 13.0f
			springDamping = 2800
			springReboundDamping = 2500

			springBoundDampingLowRate = 2000.0f
			springReboundDampingLowRate = 2000.0f
			springDampingLowRateCompression = 0.15f
			springDampingHighRateCompression = 0.30f

			visualSuspensionDroop = 0.1f
			logicalSuspensionCompressionLength = 0.25f

			wheelsVerticalOffset = 0.02f

		} : VehicleWheelDrivingPreset_2_Front

		backPreset = 
		{
			maxBrakingTorque = 3800.0f

			frictionMulLongitudinal = 0.9f
			frictionMulLateral = 0.9f
			tireLongitudinalSlipEffectsMul = 0.8f
			tireLateralSlipEffectsMul = 0.95f

			springStiffness = 11.0f
			springDamping = 2800
			springReboundDamping = 2200

			springBoundDampingLowRate = 1800.0f
			springReboundDampingLowRate = 1700.0f
			springDampingLowRateCompression = 0.20f
			springDampingHighRateCompression = 0.40f

			visualSuspensionDroop = 0.1f
			logicalSuspensionCompressionLength = 0.35f

			wheelsVerticalOffset = 0.01f

		} : VehicleWheelDrivingPreset_2_Rear

	} : VehicleWheelDrivingSetup_2_Default

	driveHelpers =
	[
		{ type = "Vehicle.HandbrakeFrictionModifier_HelperType"	blendOutTime = 0.4f } : HandbrakeFrictionModifier_2w,
		{ type = "Vehicle.RotationLimiter_HelperType" } : RotationLimiterHelper_2w,
		{ type = "Vehicle.DriveWheelsAccelerateNoise_HelperType" accelerationBoost = 3.4f }	: DriveWheelsAccelerateNoise_2w,
	]
}

KusanagiDriverFPPCameraParamsDefault : VehicleFPPCameraParams
{
	float	lookat_pitch_forward_offset			= 10.0f
	float	lookat_pitch_forward_down_ratio		= 10.0f
	float	lookat_yaw_left_offset				= 0.15f
	float	lookat_yaw_left_up_offset			= 0.2f
	float	lookat_yaw_right_offset				= 0.15f
	float	lookat_yaw_right_up_offset			= 0.2f
	float	lookat_yaw_offset_active_angle		= 30.0f
	float	is_paralax							= 0.0f
	float	paralax_radius						= 0.0f
	float	paralax_forward_offset				= 0.0f

	float	is_forward_offset     				= 0.0f
	float	forward_offset_value  				= 0.0f
	float	upperbody_pitch_weight				= 0.0f
	float	upperbody_yaw_weight				= 0.0f
	float	is_pitch_off						= 0.0f
	float	is_yaw_off							= 0.0f
}

v_sportbike1_yaiba_kusanagi_quest : v_sportbike1_yaiba_kusanagi
{
	savable = true

	tags += [ 'Immortal' ]

	vehDefaultState =
	{
	    DisableAllInteractions = true
	}
}

v_sportbike1_yaiba_kusanagi_tyger : v_sportbike1_yaiba_kusanagi
{
	visualTags = [ "TygerClaws" ]
	
	player_audio_resource = "v_mbike_yaiba_asmx_kusanagi_mizuchi"
	traffic_audio_resource = "v_mbike_yaiba_asmx_kusanagi_mizuchi_traffic"
	destroyedAppearance = "yaiba_kusanagi_basic_burnt_animals_01"

	displayName = "LocKey#23291"
}

v_sportbike1_yaiba_kusanagi_animal : v_sportbike1_yaiba_kusanagi
{
	visualTags = [ "Animals" ]
	
	player_audio_resource = "v_mbike_yaiba_asmx_kusanagi_misfit"
	traffic_audio_resource = "v_mbike_yaiba_asmx_kusanagi_misfit_traffic"
	destroyedAppearance = "yaiba_kusanagi_basic_burnt_animals_02"

	displayName = "LocKey#23290"
}

v_sportbike1_yaiba_kusanagi_mox : v_sportbike1_yaiba_kusanagi
{
	visualTags = [ "TheMox" ]
}

VehicleYaibaKusanagiUIData : VehicleDefaultUIData
{
	productionYear 	= "2068-2077"
	driveLayout 	= "LocKey#45371"
	horsepower 		= 183.0f
	mass 			= 372.0f
	info 			= "LocKey#53578"
}