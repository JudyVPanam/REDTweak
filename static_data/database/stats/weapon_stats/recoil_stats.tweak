package BaseStats
using RTDB

RecoilStat : Stat
{
	flags = [ "EquipOnPlayer", "EquipOnNPC" ]
}

Recoil : RecoilStat< Stat >
{
	localizedName = "LocKey#49872"
	enumName = "Recoil"
	min = 0
	max = 100
	improvementRelation = "ImprovementRelation.Inverse"
	enumComment = "Master stat that is influencing all other recoil stats via curves."
}

RecoilModifier: ConstantStatModifier
{
	statType = "BaseStats.Recoil"
	modifierType = "Additive"
}


RecoilAnimation : RecoilStat< Stat >
{
	enumName = "RecoilAnimation"
	min = 0
	max = 1
	
	localizedName = "Recoil Animation"

	enumComment = "Governs the weight of animations of recoil on the player"
}

RecoilAnimationModifier: ConstantStatModifier
{
	statType = "BaseStats.RecoilAnimation"
	modifierType = "Additive"
}


RecoilDir : RecoilStat< Stat >
{
	enumName = "RecoilDir"
	min = -360
	max = 360
	
	localizedName = "Recoil Direction"

	enumComment = "Default direction of recoil, increases to the left [ deg on 2D plane ]"
}

RecoilDirModifier: ConstantStatModifier
{
	statType = "BaseStats.RecoilDir"
	modifierType = "Additive"
}

RecoilAngle : RecoilStat< Stat >
{
	enumName = "RecoilAngle"
	min = 0
	max = 360
	
	localizedName = "Recoil Angle"

	enumComment = "Randomized angle added onto recoil direction per firing sequence"
}

RecoilAngleModifier: ConstantStatModifier
{
	statType = "BaseStats.RecoilAngle"
	modifierType = "Additive"
}

RecoilKickMin : RecoilStat< Stat >
{
	enumName = "RecoilKickMin"
	min = 0

	localizedName = "Recoil Minimum Kick"
	
	enumComment = "min radius of recoil sector [ deg in 3D ]"
}

RecoilKickMinModifier: ConstantStatModifier
{
	statType = "BaseStats.RecoilKickMin"
	modifierType = "Additive"
}

RecoilKickMax : RecoilStat< Stat >
{
	enumName = "RecoilKickMax"
	min = 0

	localizedName = "LocKey#754"
	
	enumComment = "max radius of recoil sector [ deg in 3D ]"
}

RecoilKickMaxModifier: ConstantStatModifier
{
	statType = "BaseStats.RecoilKickMax"
	modifierType = "Additive"
}

RecoilHoldDuration : RecoilStat< Stat >
{
	enumName = "RecoilHoldDuration"
	enumComment = "idle time between recoil and recovery [ s ]"
	min = 0
}

RecoilHoldDurationModifier: ConstantStatModifier
{
	statType = "BaseStats.RecoilHoldDuration"
	modifierType = "Additive"
}

RecoilDelay : RecoilStat< Stat >
{
	enumName = "RecoilDelay"
	min = 0
	enumComment = "speed of recoil [ deg in 3D / s ]"
	localizedName = "Recoil Time"
}

RecoilDelayModifier: ConstantStatModifier
{
	statType = "BaseStats.RecoilDelay"
	modifierType = "Additive"
}

RecoilTime : RecoilStat< Stat >
{
	enumName = "RecoilTime"
	min = 0.001
	enumComment = "Time to spend in recoiling in s"
	localizedName = "Recoil Time"
}

RecoilTimeModifier: ConstantStatModifier
{
	statType = "BaseStats.RecoilTime"
	modifierType = "Additive"
}
 
RecoilSpeed : RecoilStat< Stat >
{
	enumName = "RecoilSpeed"
	min = 0

	localizedName = "Recoil Speed"
	enumComment = "speed of the active recoil [ deg in 3D / s ]"
}

RecoilSpeedModifier: ConstantStatModifier
{
	statType = "BaseStats.RecoilSpeed"
	modifierType = "Additive"
}

RecoilRecoveryMinSpeed : RecoilStat< Stat >
{
	enumName = "RecoilRecoveryMinSpeed"
	min = 0
	enumComment = "Minimum speed of recovery [ deg in 3D / s ]. Usese Max(Speed based on RecoilRecoveryTime, RecoilRecoveryMinSpeed)"
}

RecoilRecoveryMinSpeedModifier: ConstantStatModifier
{
	statType = "BaseStats.RecoilRecoveryMinSpeed"
	modifierType = "Additive"
	
} 
RecoilRecoverySpeed : RecoilStat< Stat >
{
	enumName = "RecoilRecoverySpeed"
	min = 0
	enumComment = "speed of the recoil recovery [ deg in 3D / s ]"
}

RecoilRecoverySpeedModifier: ConstantStatModifier
{
	statType = "BaseStats.RecoilRecoverySpeed"
	modifierType = "Additive"
}

RecoilRecoveryTime : RecoilStat< Stat >
{
	enumName = "RecoilRecoveryTime"
	min = 0
	enumComment = "Time spent in recovery" 
}

RecoilRecoveryTimeModifier: ConstantStatModifier
{
	statType = "BaseStats.RecoilRecoveryTime"
	modifierType = "Additive"
}

RecoilMaxLength: RecoilStat< Stat >
{
	enumName = "RecoilMaxLength"
	min = 0
	enumComment = "Max amount of recoil allowed"
}

RecoilMaxLengthModifier: ConstantStatModifier
{
	statType = "BaseStats.RecoilMaxLength"
	modifierType = "Additive"
}

RecoilMaxLengthADS: RecoilStat< Stat >
{
	enumName = "RecoilMaxLengthADS"
	min = 0
	enumComment = "Max amount of recoil allowed in ads"
}

RecoilMaxLengthADSModifier: ConstantStatModifier
{
	statType = "BaseStats.RecoilMaxLengthADS"
	modifierType = "Additive"
}

RecoilEnableLinearX: RecoilStat< Stat >
{
	enumName = "RecoilEnableLinearX"
	
	min = 0
	max = 1
	enumComment = "Allows linear recoil values to be applied in X plane"

	flags = [ "Bool" ]
}

RecoilEnableLinearXModifier: ConstantStatModifier
{ 
	statType = "BaseStats.RecoilEnableLinearX" 
	modifierType = "Additive" 
}


RecoilEnableLinearY: RecoilStat< Stat >
{
	enumName = "RecoilEnableLinearY"
	enumComment = "Allows linear recoil values to be applied in Y plane"

	min = 0
	max = 1
	flags = [ "Bool" ]
}

RecoilEnableLinearYModifier: ConstantStatModifier
{ 
	statType = "BaseStats.RecoilEnableLinearY" 
	modifierType = "Additive" 
}


RecoilEnableCycleY: RecoilStat< Stat >
{
	enumName = "RecoilEnableCycleY"
	enumComment = "Allows cycle recoil values to be applied in Y plane"

	min = 0
	max = 1

	flags = [ "Bool" ]
	
}

RecoilEnableCycleYModifier: ConstantStatModifier
{
	statType = "BaseStats.RecoilEnableCycleY"
	modifierType = "Additive"
}

RecoilEnableCycleX: RecoilStat< Stat >
{
	enumName = "RecoilEnableCycleX"
	enumComment = "Uses sinsoidal curve to cycle recoil values to be applied in x plane"

	min = 0
	max = 1

	flags = [ "Bool" ]
	
}

RecoilEnableCycleXModifier: ConstantStatModifier
{
	statType = "BaseStats.RecoilEnableCycleX"
	modifierType = "Additive"
}

RecoilEnableScaleX: RecoilStat< Stat >
{
	enumName = "RecoilEnableScaleX"
	
	min = 0
	max = 1
	enumComment = "Allows scaling of recoil over time in x plane"
	flags = [ "Bool" ]
	
}
RecoilEnableScaleXModifier: ConstantStatModifier
{
	statType = "BaseStats.RecoilEnableScaleX"
	modifierType = "Additive"
}



RecoilEnableScaleY: RecoilStat< Stat >
{
	enumName = "RecoilEnableScaleY"
	
	min = 0
	max = 1
	enumComment = "Allows scaling of recoil over time in y plane"
	flags = [ "Bool" ]
	
}
RecoilEnableScaleYModifier: ConstantStatModifier
{
	statType = "BaseStats.RecoilEnableScaleY"
	modifierType = "Additive"
}

RecoilCycleTime: RecoilStat< Stat >
{
	enumName = "RecoilCycleTime"
	enumComment = "The period of oscillation when using cycle recoil"
	min = 0
	
}
RecoilCycleTimeModifier: ConstantStatModifier
{
	statType = "BaseStats.RecoilCycleTime"
	modifierType = "Additive"
}

RecoilCycleSize: RecoilStat< Stat >
{
	enumName = "RecoilCycleSize"
	enumComment = "Adds to recoil when cycling (in X or Y plane or both)"
	min = 0
	
}
RecoilCycleSizeModifier: ConstantStatModifier
{
	statType = "BaseStats.RecoilCycleSize"
	modifierType = "Additive"
}

RecoilScaleTime: RecoilStat< Stat >
{
	enumName = "RecoilScaleTime"
	min = 0
	enumComment = "Time over which to blend on the recoil scaling during a firing sequence [seconds]"
}
RecoilScaleTimeModifier: ConstantStatModifier
{
	statType = "BaseStats.RecoilScaleTime"
	modifierType = "Additive"
}

RecoilScaleMax: RecoilStat< Stat >
{
	enumName = "RecoilScaleMax"
	enumComment = "Max multiplier to recoil from scaling"
}
RecoilScaleMaxModifier: ConstantStatModifier
{
	statType = "BaseStats.RecoilScaleMax"
	modifierType = "Additive"
}

RecoilAlternateDir: RecoilStat< Stat >
{
	enumName = "RecoilAlternateDir"
	enumComment = "Specifies a secondary recoil direction. Which can be used by RecoilDirPlanSequence"
	min = -360
	max = 360
}
RecoilAlternateDirModifier: ConstantStatModifier
{
	statType = "BaseStats.RecoilAlternateDir"
	modifierType = "Additive"
}

RecoilDirPlanSequence: RecoilStat< Stat >	
{
	enumName = "RecoilDirPlanSequence"
	enumComment = "Enables a recoil plan of RecoilAlternateDir then RecoilDir"
}
RecoilDirPlanSequenceModifier: ConstantStatModifier
{
	statType = "BaseStats.RecoilDirPlanSequence"
	modifierType = "Additive"
	
}

RecoilDirPlanCycleRandDir: RecoilStat< Stat >
{
	enumName = "RecoilDirPlanCycleRandDir"
	enumComment = "Randomly chooses either RecoilDir or RecoilAlternateDir"
}
RecoilDirPlanCycleRandDirModifier: ConstantStatModifier
{
	statType = "BaseStats.RecoilDirPlanCycleRandDir"
	modifierType = "Additive"
}

RecoilDirPlanCycleRandRangeDir: RecoilStat< Stat >
{
	enumName = "RecoilDirPlanCycleRandRangeDir"
	enumComment = "Randomizes a direction between RecoilDir then RecoilAlternateDir"
}
RecoilDirPlanCycleRandRangeDirModifier: ConstantStatModifier
{
	statType = "BaseStats.RecoilDirPlanCycleRandRangeDir"
	modifierType = "Additive"
}


LinearDirectionUpdateMin: RecoilStat< Stat >
{
	enumName = "LinearDirectionUpdateMin"
	enumComment = "Min recoil time before updating the recoil direction plan [seconds]"
}
LinearDirectionUpdateMinModifier: ConstantStatModifier
{
	statType = "BaseStats.LinearDirectionUpdateMin"
	modifierType = "Additive"
}

LinearDirectionUpdateMax: RecoilStat< Stat >
{
	enumName = "LinearDirectionUpdateMax"
	enumComment = "Max recoil time before updating the recoil direction plan [seconds]"
}
LinearDirectionUpdateMaxModifier: ConstantStatModifier
{
	statType = "BaseStats.LinearDirectionUpdateMax"
	modifierType = "Additive"
}

RecoilChargeMult: RecoilStat< Stat >
{
	enumName = "RecoilChargeMult"
	enumComment = "Multiplier to recoil kick as a weapon is charging"
}
RecoilChargeMultModifier: ConstantStatModifier
{
	statType = "BaseStats.RecoilChargeMult"
	modifierType = "Additive"
}

RecoilFullChargeMult: RecoilStat< Stat >
{
	enumName = "RecoilFullChargeMult"
	enumComment = "Multiplier to recoil kick when a weapon is fully charged"
}
RecoilFullChargeMultModifier: ConstantStatModifier
{
	statType = "BaseStats.RecoilFullChargeMult"
	modifierType = "Additive"
}

RecoilAllowSway: RecoilStat< Stat >
{
	enumName = "RecoilAllowSway"
	enumComment = "bool flag to allow sway to be active while recoil is active"
	min = 0
	max = 1
	flags = [ "Bool" ]
}
RecoilAllowSwayModifier: ConstantStatModifier
{
	statType = "BaseStats.RecoilAllowSway"
	modifierType = "Additive"
}

RecoilDriftRandomRangeMin: RecoilStat< Stat >
{
	enumName = "RecoilDriftRandomRangeMin"
	enumComment = "Min range for recoil drift (new final position when recovery ends)"
}
RecoilDriftRandomRangeMinModifier: ConstantStatModifier
{
	statType = "BaseStats.RecoilDriftRandomRangeMin"
	modifierType = "Additive"
}

RecoilDriftRandomRangeMax: RecoilStat< Stat >
{
	enumName = "RecoilDriftRandomRangeMax"
	enumComment = "Max range for recoil drift (new final position when recovery ends)"
}
RecoilDriftRandomRangeMaxModifier: ConstantStatModifier
{
	statType = "BaseStats.RecoilDriftRandomRangeMax"
	modifierType = "Additive"
}

RecoilMagForFullDrift: RecoilStat< Stat >
{
	enumName = "RecoilMagForFullDrift"
	enumComment = "The recoil must reach this much kick/length before full drift is applied"
}
RecoilMagForFullDriftModifier: ConstantStatModifier
{
	statType = "BaseStats.RecoilMagForFullDrift"
	modifierType = "Additive"
}
        

RecoilUseDifferentStatsInADS : RecoilStat< Stat >
{
	enumName = "RecoilUseDifferentStatsInADS"
	enumComment = "bool flag to enable the rest of stats, by default false"
	min = 0
	max = 1

	flags = [ "Bool" ]
}

RecoilUseDifferentStatsInADSModifier: ConstantStatModifier
{
	statType = "BaseStats.RecoilUseDifferentStatsInADS"
	modifierType = "Additive"
}
 
RecoilDirADS : RecoilStat< Stat >
{
	enumName = "RecoilDirADS"
	min = -360
	max = 360
}

RecoilDirADSModifier: ConstantStatModifier
{
	statType = "BaseStats.RecoilDirADS"
	modifierType = "Additive"
}
 
RecoilAngleADS : RecoilStat< Stat >
{
	enumName = "RecoilAngleADS"
	min = 0
}

RecoilAngleADSModifier: ConstantStatModifier
{
	statType = "BaseStats.RecoilAngleADS"
	modifierType = "Additive"
}
 
RecoilKickMinADS : RecoilStat< Stat >
{
	enumName = "RecoilKickMinADS"
	min = 0
}

RecoilKickMinADSModifier: ConstantStatModifier
{
	statType = "BaseStats.RecoilKickMinADS"
	modifierType = "Additive"
}
 
RecoilKickMaxADS : RecoilStat< Stat >
{
	enumName = "RecoilKickMaxADS"
	min = 0
}


RecoilTimeADS : RecoilStat< Stat >
{
	enumName = "RecoilTimeADS"
	min = 0

	localizedName = "Recoil Time"
	
 
}

RecoilTimeADSModifier: ConstantStatModifier
{
	statType = "BaseStats.RecoilTimeADS"
	modifierType = "Additive"
}

RecoilRecoveryMinSpeedADS : RecoilStat< Stat >
{
	enumName = "RecoilRecoveryMinSpeedADS"
	min = 0
}

RecoilRecoveryMinSpeedADSModifier: ConstantStatModifier
{
	statType = "BaseStats.RecoilRecoveryMinSpeedADS"
	modifierType = "Additive"
	
}

RecoilRecoveryTimeADS : RecoilStat< Stat >
{
	enumName = "RecoilRecoveryTimeADS"
	min = 0

	localizedName = "Recoil Time"
}

RecoilRecoveryTimeADSModifier: ConstantStatModifier
{
	statType = "BaseStats.RecoilRecoveryTimeADS"
	modifierType = "Additive"
}

RecoilKickMaxADSModifier: ConstantStatModifier
{
	statType = "BaseStats.RecoilKickMaxADS"
	modifierType = "Additive"
}

RecoilHoldDurationADS : RecoilStat< Stat >
{
	enumName = "RecoilHoldDurationADS"
	min = 0
}

RecoilHoldDurationADSModifier: ConstantStatModifier
{
	statType = "BaseStats.RecoilHoldDurationADS"
	modifierType = "Additive"
}

RecoilEnableLinearXADS: RecoilStat< Stat >
{
	enumName = "RecoilEnableLinearXADS"
	min = 0
	max = 1
	flags = [ "Bool" ]
}

RecoilEnableLinearXADSModifier: ConstantStatModifier
{ 
	statType = "BaseStats.RecoilEnableLinearXADS" 
	modifierType = "Additive" 
}
RecoilEnableLinearYADS: RecoilStat< Stat >
{
	enumName = "RecoilEnableLinearYADS"
	min = 0
	max = 1
	flags = [ "Bool" ]
}

RecoilEnableLinearYADSModifier: ConstantStatModifier
{ 
	statType = "BaseStats.RecoilEnableLinearYADS" 
	modifierType = "Additive" 
}

RecoilEnableCycleXADS: RecoilStat< Stat >
{
	enumName = "RecoilEnableCycleXADS"
	
	min = 0
	max = 1

	flags = [ "Bool" ]
	
}
RecoilEnableCycleXADSModifier: ConstantStatModifier
{
	statType = "BaseStats.RecoilEnableCycleXADS"
	modifierType = "Additive"
}

RecoilEnableCycleYADS: RecoilStat< Stat >
{
	enumName = "RecoilEnableCycleYADS"
	
	min = 0
	max = 1

	flags = [ "Bool" ]
	
}
RecoilEnableCycleYADSModifier: ConstantStatModifier
{
	statType = "BaseStats.RecoilEnableCycleYADS"
	modifierType = "Additive"
}

RecoilEnableScaleXADS: RecoilStat< Stat >
{
	enumName = "RecoilEnableScaleXADS"
	
	min = 0
	max = 1

	flags = [ "Bool" ]
	
}
RecoilEnableScaleXADSModifier: ConstantStatModifier
{
	statType = "BaseStats.RecoilEnableScaleXADS"
	modifierType = "Additive"
}

RecoilEnableScaleYADS: RecoilStat< Stat >
{
	enumName = "RecoilEnableScaleYADS"
	
	min = 0
	max = 1

	flags = [ "Bool" ]
	
}
RecoilEnableScaleYADSModifier: ConstantStatModifier
{
	statType = "BaseStats.RecoilEnableScaleYADS"
	modifierType = "Additive"
}

RecoilCycleTimeADS: RecoilStat< Stat >
{
	enumName = "RecoilCycleTimeADS"
	min = 0
	
}
RecoilCycleTimeADSModifier: ConstantStatModifier
{
	statType = "BaseStats.RecoilCycleTimeADS"
	modifierType = "Additive"
}

RecoilCycleSizeADS: RecoilStat< Stat >
{
	enumName = "RecoilCycleSizeADS"
	min = 0
	
}
RecoilCycleSizeADSModifier: ConstantStatModifier
{
	statType = "BaseStats.RecoilCycleSizeADS"
	modifierType = "Additive"
}

RecoilScaleTimeADS: RecoilStat< Stat >
{
	enumName = "RecoilScaleTimeADS"
	min = 0
}
RecoilScaleTimeADSModifier: ConstantStatModifier
{
	statType = "BaseStats.RecoilScaleTimeADS"
	modifierType = "Additive"
}

RecoilScaleMaxADS: RecoilStat< Stat >
{
	enumName = "RecoilScaleMaxADS"
}
RecoilScaleMaxADSModifier: ConstantStatModifier
{
	statType = "BaseStats.RecoilScaleMaxADS"
	modifierType = "Additive"
}


RecoilSpeedADS : RecoilStat< Stat >
{
	enumName = "RecoilSpeedADS"
	min = 0

	localizedName = "Recoil Speed"
	
 
}

RecoilSpeedADSModifier: ConstantStatModifier
{
	statType = "BaseStats.RecoilSpeedADS"
	modifierType = "Additive"
}

RecoilRecoverySpeedADS : RecoilStat< Stat >
{
	enumName = "RecoilRecoverySpeedADS"
	min = 0
	
 
}

RecoilRecoverySpeedADSModifier: ConstantStatModifier
{
	statType = "BaseStats.RecoilRecoverySpeedADS"
	modifierType = "Additive"
}

RecoilAlternateDirADS: RecoilStat< Stat >
{
	enumName = "RecoilAlternateDirADS"
	min = -360
	max = 360
}
RecoilAlternateDirADSModifier: ConstantStatModifier
{
	statType = "BaseStats.RecoilAlternateDirADS"
	modifierType = "Additive"
}

RecoilDirPlanSequenceADS: RecoilStat< Stat >
{
	enumName = "RecoilDirPlanSequenceADS"
}
RecoilDirPlanSequenceADSModifier: ConstantStatModifier
{
	statType = "BaseStats.RecoilDirPlanSequenceADS"
	modifierType = "Additive"
}

RecoilDirPlanCycleRandDirADS: RecoilStat< Stat >
{
	enumName = "RecoilDirPlanCycleRandDirADS"
}
RecoilDirPlanCycleRandDirADSModifier: ConstantStatModifier
{
	statType = "BaseStats.RecoilDirPlanCycleRandDirADS"
	modifierType = "Additive"
}

RecoilDirPlanCycleRandRangeDirADS: RecoilStat< Stat >
{
	enumName = "RecoilDirPlanCycleRandRangeDirADS"
}
RecoilDirPlanCycleRandRangeDirADSModifier: ConstantStatModifier
{
	statType = "BaseStats.RecoilDirPlanCycleRandRangeDirADS"
	modifierType = "Additive"
}


LinearDirectionUpdateMinADS: RecoilStat< Stat >
{
	enumName = "LinearDirectionUpdateMinADS"
}
LinearDirectionUpdateMinADSModifier: ConstantStatModifier
{
	statType = "BaseStats.LinearDirectionUpdateMinADS"
	modifierType = "Additive"
}

LinearDirectionUpdateMaxADS: RecoilStat< Stat >
{
	enumName = "LinearDirectionUpdateMaxADS"
}
LinearDirectionUpdateMaxADSModifier: ConstantStatModifier
{
	statType = "BaseStats.LinearDirectionUpdateMaxADS"
	modifierType = "Additive"
}

RecoilChargeMultADS: RecoilStat< Stat >
{
	enumName = "RecoilChargeMultADS"
}
RecoilChargeMultADSModifier: ConstantStatModifier
{
	statType = "BaseStats.RecoilChargeMultADS"
	modifierType = "Additive"
}
RecoilFullChargeMultADS: RecoilStat< Stat >
{
	enumName = "RecoilFullChargeMultADS"
}
RecoilFullChargeMultADSModifier: ConstantStatModifier
{
	statType = "BaseStats.RecoilFullChargeMultADS"
	modifierType = "Additive"
}


