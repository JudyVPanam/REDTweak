package Loot
using RTDB, Items

// --------------------------------------------------------------- //
// --------------------------- GENERIC --------------------------- //
// --------------------------------------------------------------- //

TygerClawsLoot : LootTable
{
	//devNotes = "This is a base loot table for TygerClaws"
    minItemsToLoot = 1
    maxItemsToLoot = 2
	lootGenerationType = "dropChance"
}

TygerClawsMisc : TygerClawsLoot
{
	lootTableInclusions = 
	[ 
		"Loot.TygerClawsJunk", 
		"Loot.TygerClawsJewellery",
		"Loot.TygerClawsEdibles",
		"Loot.NPCGenericJunk",
		"Loot.NPCGenericGangJunk",
	]
}

TygerClawsJunk : TygerClawsLoot
{
	queries =
	[
		{ dropChance = 0.4 query = "Query.TygerClawsJunk" },
	]
}

TygerClawsJewellery : TygerClawsLoot
{
	queries =
	[
		{ dropChance = 0.4 query = "Query.TygerClawsJewellery" },
	]
}

TygerClawsEdibles : TygerClawsLoot
{
	queries =
	[
		{ dropChance = 0.2f query = "Query.LowQualityFood" },
		{ dropChance = 0.2f query = "Query.LowQualityDrink" },
		{ dropChance = 0.2f query = "Query.LowQualityAlcohol" },
		{ dropChance = 0.2f query = "Query.MediumQualityFood" },
		{ dropChance = 0.2f query = "Query.MediumQualityDrink" },
		{ dropChance = 0.2f query = "Query.MediumQualityAlcohol" },
	]
}

TygerClawsConsumablesTier1 : NPCGenericConsumablesTier1{}

TygerClawsConsumablesTier2 : NPCGenericConsumablesTier2{}

TygerClawsConsumablesTier3 : NPCGenericConsumablesTier3
{
}

AllTygerClawsConsumables : TygerClawsLoot
{
	lootTableInclusions += 
	[ 
		"Loot.TygerClawsConsumablesTier1", 
		"Loot.TygerClawsConsumablesTier2", 
		"Loot.TygerClawsConsumablesTier3", 
	]
}


TygerClawsGadgets : TygerClawsLoot
{
	lootItems =
	[
		{ dropChance = 0.35 dropCountMin = 3 dropCountMax = 5 itemID = "Items.GrenadeFragRegular" },
	]
}

TygerClawsWeaponMods : NPCGenericWeaponMods
{
	lootItems +=
	[
		{ 
			dropChance = 0.2f itemID = "Items.w_att_scope_short_01"
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		}
		{ 
			dropChance = 0.2f itemID = "Items.w_att_scope_short_03"
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		}
		{ 
			dropChance = 0.1f itemID = "Items.w_att_scope_short_04"
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		}
		{ 
			dropChance = 0.05f itemID = "Items.w_att_scope_short_05"
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		}
		{ 
			dropChance = 0.05f itemID = "Items.w_att_scope_long_01"
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		}
		{ 
			dropChance = 0.2f itemID = "Items.w_att_scope_long_03"
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		}
		{ 
			dropChance = 0.1f itemID = "Items.w_att_scope_long_04"
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		}
	]
}

// --------------------------------------------------------------- //
// ----------------------- GENERIC RANGED ------------------------ //
// --------------------------------------------------------------- //


// -------------------- TIER 1 -------------------- //

TygerClawsGenericRangedAmmoT1 : TygerClawsLoot
{
	lootTableInclusions = 
    [		
        "Ammo.AmmoLootTable",
    ]
}

TygerClawsGenericRangedBikerClothingT1 : TygerClawsLoot
{
	queries =
	[
		{ 
			dropChance = 0.4 query = "Query.TygerClawsBikerClothing"
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
		{ 
			dropChance = 0.4 query = "Query.BikerClothingNoFaction"
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
		{ 
			dropChance = 0.4 query = "Query.Mask"
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
        { 
			dropChance = 0.4 query = "Query.Visor"
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
		{ 
			dropChance = 0.4 query = "Query.TygerClawsFeet"
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
	]
}

TygerClawsGenericRangedGangsterClothingT1 : TygerClawsLoot
{
	queries =
	[
		{ 
			dropChance = 0.4 query = "Query.FormalClothing"
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
        { 
			dropChance = 0.4 query = "Query.Mask"
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			] 
		},
        { 
			dropChance = 0.4 query = "Query.Glasses"
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			] 
		},
		{ 
			dropChance = 0.4 query = "Query.TygerClawsFeet"
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
	]

	lootItems =
	[
		{ 
			dropChance = 0.4 itemID = "Items.FormalJacket_01_basic_01"
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
		{ 
			dropChance = 0.4 itemID = "Items.Glasses_03_basic_01" 
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
	]
}

TygerClawsGenericRangedConsumablesT1 : TygerClawsLoot
{
	lootTableInclusions += 
	[
		"Loot.TygerClawsConsumablesTier1",
		"Loot.TygerClawsGadgets",
	]
}

TygerClawsGenericRangedPartsT1 : TygerClawsLoot
{
	lootTableInclusions += 
	[
		"Loot.TygerClawsWeaponMods",
	]
}

TygerClawsGenericRangedMiscT1 : TygerClawsLoot
{
	lootTableInclusions += 
	[
		"Loot.TygerClawsGenericRangedAmmoT1",
		"Loot.TygerClawsMisc",
	]
}

// -------------------- TIER 2 -------------------- //

TygerClawsGenericRangedAmmoT2 : TygerClawsLoot
{
	lootTableInclusions = 
    [		
        "Ammo.AmmoLootTable",
    ]
}

TygerClawsGenericRangedBikerClothingT2 : TygerClawsLoot
{
	queries =
	[
		{ 
			dropChance = 0.4 query = "Query.TygerClawsBikerClothing" 
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
		{ 
			dropChance = 0.4 query = "Query.BikerClothingNoFaction" 
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
		{ 
			dropChance = 0.4 query = "Query.Jumpsuit" 
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
		{ 
			dropChance = 0.4 query = "Query.Balaclava" 
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
		{ 
			dropChance = 0.4 query = "Query.Mask" 
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
        { 
			dropChance = 0.4 query = "Query.Glasses" 
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
		{ 
			dropChance = 0.4 query = "Query.TygerClawsFeet"
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
	]
}

TygerClawsGenericRangedGangsterClothingT2 : TygerClawsLoot
{
	queries =
	[
		{ 
			dropChance = 0.4 query = "Query.FormalClothingNoFaction"
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
		{ 
			dropChance = 0.4 query = "Query.RichClothing" 
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
        { 
			dropChance = 0.4 query = "Query.Mask" 
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
        { 
			dropChance = 0.4 query = "Query.Glasses" 
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
		{ 
			dropChance = 0.4 query = "Query.TygerClawsFeet"
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
	]

	lootItems =
	[
		{ 
			dropChance = 0.4 itemID = "Items.FormalJacket_01_basic_01"
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			] 
		},
		{ 
			dropChance = 0.4 itemID = "Items.Glasses_03_basic_01" 
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
	]
}


TygerClawsGenericRangedConsumablesT2 : TygerClawsLoot
{
	lootTableInclusions += 
	[
		"Loot.TygerClawsConsumablesTier1",
		"Loot.TygerClawsConsumablesTier2",
		"Loot.TygerClawsGadgets",
	]
}

TygerClawsGenericRangedPartsT2 : TygerClawsLoot
{
	lootTableInclusions += 
	[
		"Loot.TygerClawsWeaponMods",
	]
}

TygerClawsGenericRangedMiscT2 : TygerClawsLoot
{
	lootTableInclusions += 
	[
		"Loot.TygerClawsMisc",
		"Loot.TygerClawsGenericRangedAmmoT2",
	]
}

// -------------------- TIER 3 -------------------- //

TygerClawsGenericRangedAmmoT3 : TygerClawsLoot
{
	lootTableInclusions = 
    [		
        "Ammo.AmmoLootTable",
    ]
}

TygerClawsGenericRangedGadgetsT3 : TygerClawsGadgets
{
}

TygerClawsGenericRangedGangsterClothingT3 : TygerClawsLoot
{
	queries =
	[
		{ 
			dropChance = 0.4 query = "Query.FormalClothingNoFaction" 
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
		{ 
			dropChance = 0.4 query = "Query.Mask" 
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
		{ 
			dropChance = 0.4 query = "Query.Glasses" 
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
		{ 
			dropChance = 0.4 query = "Query.TygerClawsFeet"
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
	]

	lootItems =
	[
		{ 
			dropChance = 0.4 itemID = "Items.FormalJacket_01_basic_01" 
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
		{ 
			dropChance = 0.4 itemID = "Items.FormalPants_02_basic_01" 
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
		{ 
			dropChance = 0.4 itemID = "Items.FormalShoes_01_basic_01" 
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
		{ 
			dropChance = 0.4 itemID = "Items.Glasses_03_basic_01" 
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
		{ 
			dropChance = 0.4 itemID = "Items.Visor_01_basic_05" 
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
		{ 
			dropChance = 0.4 itemID = "Items.Visor_02_basic_01"
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
	]
}


TygerClawsGenericRangedConsumablesT3 : TygerClawsLoot
{
	lootTableInclusions += 
	[
		"Loot.AllTygerClawsConsumables",
		"Loot.TygerClawsGenericRangedGadgetsT3", 
	]
}

TygerClawsGenericRangedPartsT3 : TygerClawsLoot
{
	lootTableInclusions += 
	[
		"Loot.TygerClawsWeaponMods",
	]
}

TygerClawsGenericRangedMiscT3 : TygerClawsLoot
{
	lootTableInclusions += 
	[
		"Loot.TygerClawsMisc",
		"Loot.TygerClawsGenericRangedAmmoT3",
	]
}

// --------------------------------------------------------------- //
// ----------------------- GENERIC MELEE ----------------------- //
// --------------------------------------------------------------- //


// -------------------- TIER 1 -------------------- //

TygerClawsGenericMeleeAmmoT1 : TygerClawsLoot
{
	lootTableInclusions = 
    [		
        "Ammo.AmmoLootTable",
    ]
}

TygerClawsGenericMeleeBikerClothingT1 : TygerClawsLoot
{
	queries =
	[
		{ 
			dropChance = 0.4 query = "Query.TygerClawsBikerClothing" 
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
		{ 
			dropChance = 0.4 query = "Query.BikerClothingNoFaction" 
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
		{ 
			dropChance = 0.4 query = "Query.Mask" 
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
        { 
			dropChance = 0.4 query = "Query.Visor" 
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
		{ 
			dropChance = 0.4 query = "Query.TygerClawsFeet"
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
	]
}

TygerClawsGenericMeleeGangsterClothingT1 : TygerClawsLoot
{
	queries =
	[
		{ 
			dropChance = 0.4 query = "Query.FormalClothingNoFaction" 
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
        { 
			dropChance = 0.4 query = "Query.Mask" 
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
        { 
			dropChance = 0.4 query = "Query.Glasses"
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
		{ 
			dropChance = 0.4 query = "Query.TygerClawsFeet"
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
	]

	lootItems =
	[
		{ 
			dropChance = 0.4 itemID = "Items.FormalJacket_01_basic_01" 
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
		{ 
			dropChance = 0.4 itemID = "Items.Glasses_03_basic_01" 
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
	]
}

TygerClawsGenericMeleeConsumablesT1 : TygerClawsLoot
{
	lootTableInclusions += 
	[
		"Loot.TygerClawsConsumablesTier1",
		"Loot.TygerClawsGadgets",
	]
}

TygerClawsGenericMeleeMiscT1 : TygerClawsLoot
{
	lootTableInclusions += 
	[
		"Loot.TygerClawsGenericMeleeAmmoT1",
		"Loot.TygerClawsMisc",
	]
}
// -------------------- TIER 2 -------------------- //

TygerClawsGenericMeleeAmmoT2 : TygerClawsLoot
{
	lootTableInclusions = 
    [		
        "Ammo.AmmoLootTable",
    ]
}

TygerClawsGenericMeleeBikerClothingT2 : TygerClawsLoot
{
	queries =
	[
		{ 
			dropChance = 0.4 query = "Query.TygerClawsBikerClothing" 
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
		{ 
			dropChance = 0.4 query = "Query.BikerClothingNoFaction" 
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
		{ 
			dropChance = 0.4 query = "Query.Jumpsuit" 
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
		{ 
			dropChance = 0.4 query = "Query.Balaclava" 
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
		{ 
			dropChance = 0.4 query = "Query.TygerClawsMask" 
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
        { 
			dropChance = 0.4 query = "Query.Glasses" 
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
		{ 
			dropChance = 0.4 query = "Query.TygerClawsFeet"
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
	]
}

TygerClawsGenericMeleeGangsterClothingT2 : TygerClawsLoot
{
	queries =
	[
		{ 
			dropChance = 0.4 query = "Query.FormalClothingNoFaction" 
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
		{ 
			dropChance = 0.4 query = "Query.RichClothing" 
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
        { 
			dropChance = 0.4 query = "Query.TygerClawsMask" 
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
        { 
			dropChance = 0.4 query = "Query.Glasses" 
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
		{ 
			dropChance = 0.4 query = "Query.TygerClawsFeet"
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
	]

	lootItems =
	[
		{ dropChance = 0.4 itemID = "Items.FormalJacket_01_basic_01" },
		{ dropChance = 0.4 itemID = "Items.Glasses_03_basic_01" },
	]
}

TygerClawsGenericMeleeConsumablesT2 : TygerClawsLoot
{
	lootTableInclusions += 
	[
		"Loot.TygerClawsConsumablesTier1",
		"Loot.TygerClawsConsumablesTier2", 
		"Loot.TygerClawsGadgets",
	]
}

TygerClawsGenericMeleeMiscT2 : TygerClawsLoot
{
	lootTableInclusions += 
	[
		"Loot.TygerClawsMisc",
		"Loot.TygerClawsGenericMeleeAmmoT2",
	]
}

// --------------------------------------------------------------- //
// --------------------------- SHOTGUN --------------------------- //
// --------------------------------------------------------------- //


// -------------------- TIER 2 -------------------- //

TygerClawsShotgunAmmoT2 : TygerClawsLoot
{
	lootTableInclusions = 
    [		
        "Ammo.AmmoLootTable",
    ]
}

TygerClawsShotgunBikerClothingT2 : TygerClawsLoot
{
	queries =
	[
		{ 
			dropChance = 0.4 query = "Query.TygerClawsBikerClothing"
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
		{ 
			dropChance = 0.4 query = "Query.BikerClothingNoFaction" 
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
		{ 
			dropChance = 0.4 query = "Query.Jumpsuit" 
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
		{ 
			dropChance = 0.4 query = "Query.Balaclava" 
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
		{ 
			dropChance = 0.4 query = "Query.TygerClawsBikerHelmet" 
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
		{ 
			dropChance = 0.4 query = "Query.TygerClawsMask" 
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
        { 
			dropChance = 0.4 query = "Query.Glasses" 
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
		{ 
			dropChance = 0.4 query = "Query.TygerClawsFeet"
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
	]
}

TygerClawsShotgunConsumablesT2 : TygerClawsLoot
{
	lootTableInclusions += 
	[
		"Loot.TygerClawsConsumablesTier1",
		"Loot.TygerClawsConsumablesTier2",
		"Loot.TygerClawsGadgets",
	]
}

TygerClawsShotgunPartsT2 : TygerClawsLoot
{
	lootTableInclusions += 
	[
		"Loot.TygerClawsWeaponMods",
	]
}

TygerClawsShotgunMiscT2 : TygerClawsLoot
{
	lootTableInclusions += 
	[
		"Loot.TygerClawsShotgunAmmoT2",
		"Loot.TygerClawsMisc",
	]
}


// -------------------- TIER 3 -------------------- //

TygerClawsShotgunAmmoT3 : TygerClawsLoot
{
	lootTableInclusions = 
    [		
        "Ammo.AmmoLootTable",
    ]
}

TygerClawsShotgunGadgetsT3 : TygerClawsGadgets
{
}

TygerClawsShotgunBikerClothingT3 : TygerClawsLoot
{
	queries =
	[
		{ 
			dropChance = 0.4 query = "Query.TygerClawsBikerClothing" 
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
		{ 
			dropChance = 0.4 query = "Query.BikerClothingNoFaction"
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
		{ 
			dropChance = 0.4 query = "Query.Jumpsuit" 
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
		{ 
			dropChance = 0.4 query = "Query.BikerHelmetNoFaction" 
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
		{ 
			dropChance = 0.4 query = "Query.TygerClawsBikerHelmet" 
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
		{ 
			dropChance = 0.4 query = "Query.TygerClawsMask" 
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
		{ 
			dropChance = 0.4 query = "Query.Glasses" 
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
		{ 
			dropChance = 0.4 query = "Query.TygerClawsFeet"
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
	]
}

TygerClawsShotgunConsumablesT3 : TygerClawsLoot
{
	lootTableInclusions += 
	[
		"Loot.AllTygerClawsConsumables",
		"Loot.TygerClawsShotgunGadgetsT3", 
	]
}

TygerClawsShotgunPartsT3 : TygerClawsLoot
{
	lootTableInclusions += 
	[
		"Loot.TygerClawsWeaponMods",
	]
}

TygerClawsShotgunMiscT3 : TygerClawsLoot
{
	lootTableInclusions += 
	[
		"Loot.TygerClawsMisc",
		"Loot.TygerClawsShotgunAmmoT3",
	]
}

// --------------------------------------------------------------- //
// -------------------------- FAST MELEE ------------------------- //
// --------------------------------------------------------------- //

// -------------------- TIER 2 -------------------- //

TygerClawsFastMeleeAmmoT2 : TygerClawsLoot
{
	lootTableInclusions = 
    [		
        "Ammo.AmmoLootTable",
    ]
}

TygerClawsFastMeleeFragmentsT2 : TygerClawsLoot
{
	lootItems =
	[
		{ dropChance = 0.2 itemID = "Items.TygerClawsSandevistanFragment1" },
		{ dropChance = 0.2 itemID = "Items.SandevistanFragment1" },
		{ dropChance = 0.2 itemID = "Items.SandevistanFragment3" },
	]
}

TygerClawsFastMeleeMartialClothingT2 : TygerClawsLoot
{
	lootItems =
	[
		{ 
			dropChance = 0.4 itemID = "Items.Pants_14_basic_01" 
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
		{ 
			dropChance = 0.4 itemID = "Items.Pants_14_basic_02" 
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
		{ 
			dropChance = 0.4 itemID = "Items.Pants_14_old_02" 
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
		{ 
			dropChance = 0.4 itemID = "Items.Pants_14_rich_01" 
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
		{ 
			dropChance = 0.4 itemID = "Items.Pants_14_rich_02"
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
		{ 
			dropChance = 0.4 itemID = "Items.Boots_09_basic_02" 
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
		{ 
			dropChance = 0.4 itemID = "Items.Boots_09_old_02" 
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
		{ 
			dropChance = 0.4 itemID = "Items.Boots_09_rich_01" 
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
		{ 
			dropChance = 0.4 itemID = "Items.Boots_09_rich_02" 
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
		{ 
			dropChance = 0.4 itemID = "Items.Boots_09_rich_03" 
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
	]
}

TygerClawsFastMeleeConsumablesT2 : TygerClawsLoot
{
	lootTableInclusions += 
	[
		"Loot.TygerClawsConsumablesTier1",
		"Loot.TygerClawsConsumablesTier2",
		"Loot.TygerClawsGadgets",
	]
}

TygerClawsFastMeleePartsT2 : TygerClawsLoot
{
	lootTableInclusions += 
	[
		"Loot.TygerClawsFastMeleeFragmentsT2",
	]
}

TygerClawsFastMeleeMiscT2 : TygerClawsLoot
{
	lootTableInclusions += 
	[
		"Loot.TygerClawsFastMeleeAmmoT2",
		"Loot.TygerClawsMisc",
	]
}

// -------------------- TIER 3 -------------------- //

TygerClawsFastMeleeAmmoT3 : TygerClawsLoot
{
	lootTableInclusions = 
    [		
        "Ammo.AmmoLootTable",
    ]
}

TygerClawsFastMeleeFragmentsT3 : TygerClawsLoot
{
	lootItems =
	[
		{ dropChance = 0.2  itemID = "Items.TygerClawsSandevistanFragment1" },
		{ dropChance = 0.15 itemID = "Items.SandevistanFragment2" },
		{ dropChance = 0.15 itemID = "Items.SandevistanFragment4" },
	]
}

TygerClawsFastMeleeGadgetsT3 : TygerClawsGadgets
{
}

TygerClawsFastMeleeKunoichiClothingT3 : TygerClawsLoot
{
	maxItemsToLoot = 1
	
	lootItems =
	[
		{ 
			dropChance = 0.4 itemID = "Items.TightJumpsuit_01_rich_02" 
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
		{ 
			dropChance = 0.4 itemID = "Items.TightJumpsuit_01_basic_01" 
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
		{ 
			dropChance = 0.4 itemID = "Items.Boots_09_rich_01" 
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
		{ 
			dropChance = 0.4 itemID = "Items.Boots_09_rich_02"
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			] 
		},
		{ 
			dropChance = 0.4 itemID = "Items.Visor_02_basic_01" 
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
	]
}

TygerClawsFastMeleeConsumablesT3 : TygerClawsLoot
{
	lootTableInclusions += 
	[
		"Loot.AllTygerClawsConsumables", 
		"Loot.TygerClawsFastMeleeGadgetsT3", 
	]
}

TygerClawsFastMeleePartsT3 : TygerClawsLoot
{
	lootTableInclusions += 
	[
		"Loot.TygerClawsFastMeleeFragmentsT3",
	]
}

TygerClawsFastMeleeMiscT3 : TygerClawsLoot
{
	lootTableInclusions += 
	[
		"Loot.TygerClawsFastMeleeAmmoT3",
		"Loot.TygerClawsMisc",
	]
}

// --------------------------------------------------------------- //
// ------------------------- HEAVY MELEE ------------------------- //
// --------------------------------------------------------------- //

// -------------------- TIER 3 -------------------- //

TygerClawsHeavyMeleeAmmoT3 : TygerClawsLoot
{
	lootTableInclusions = 
    [		
        "Ammo.AmmoLootTable",
    ]
}

TygerClawsHeavyMeleeGadgetsT3 : TygerClawsGadgets{}

TygerClawsHeavyMeleeOzekiClothingT3 : TygerClawsLoot
{
	queries =
	[
		{ 
			dropChance = 0.4 query = "Query.Jumpsuit" 
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
		{ 
			dropChance = 0.4 query = "Query.Mask" 
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
		{ 
			dropChance = 0.4 query = "Query.CasualShoes"
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			] 
		},
	]

}

TygerClawsHeavyMeleeConsumablesT3 : TygerClawsLoot
{
	lootTableInclusions += 
	[
		"Loot.AllTygerClawsConsumables",
		"Loot.TygerClawsHeavyMeleeGadgetsT3", 
	]
}

TygerClawsHeavyMeleeMiscT3 : TygerClawsLoot
{
	lootTableInclusions += 
	[
		"Loot.TygerClawsMisc",
		"Loot.TygerClawsHeavyMeleeAmmoT3",
	]
}

// --------------------------------------------------------------- //
// -------------------------- NETRUNNER -------------------------- //
// --------------------------------------------------------------- //

// -------------------- TIER 3 -------------------- //

TygerClawsNetrunnerProgramsT3 : TygerClawsLoot
{
	maxItemsToLoot = 1

	lootItems =
	[
		{ dropChance = 0.1 itemID = "Items.BlindProgram" },
		{ dropChance = 0.1 itemID = "Items.CommsNoiseProgram" },
		{ dropChance = 0.1 itemID = "Items.DisableCyberwareProgram" },
		{ dropChance = 0.1 itemID = "Items.OverheatProgram" },
	]
}

TygerClawsNetrunnerConsumablesTier3 : TygerClawsLoot
{
	lootItems =
	[
		{ dropChance = 0.25 itemID = "Items.MemoryBooster" },
	]
}

TygerClawsNetrunnerGadgetsT3 : TygerClawsLoot
{
	lootItems +=
	[
		{ dropChance = 0.3 dropCountMin = 2 dropCountMax = 4 itemID = "Items.GrenadeReconRegular" },
	]
}

TygerClawsNetrunnerGangsterClothingT3 : TygerClawsLoot
{
	maxItemsToLoot = 1

	queries =
	[
		{ 
			dropChance = 0.4 query = "Query.FormalClothingNoFaction" 
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
		{ 
			dropChance = 0.4 query = "Query.TygerClawsMask" 
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
		{ 
			dropChance = 0.4 query = "Query.TygerClawsFeet"
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
	]

	lootItems =
	[
		{ 
			dropChance = 0.4 itemID = "Items.FormalJacket_01_basic_01"
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			] 
		},
		{ 
			dropChance = 0.4 itemID = "Items.FormalPants_02_basic_01" 
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
		{ 
			dropChance = 0.4 itemID = "Items.FormalShoes_01_basic_01" 
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
		{ 
			dropChance = 0.4 itemID = "Items.Glasses_03_basic_01" 
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
		{ 
			dropChance = 0.4 itemID = "Items.Visor_01_basic_05"
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			] 
		},
		{ 
			dropChance = 0.4 itemID = "Items.Visor_02_basic_01" 
			fk< StatModifier >[] statModifiers =
			[
				"Quality.MaximumUncommonQuality"
			]
		},
	]
}

TygerClawsNetrunnerConsumablesT3 : TygerClawsLoot
{
	lootTableInclusions = 
	[
		"Loot.TygerClawsNetrunnerGadgetsT3",
		"Loot.TygerClawsNetrunnerConsumablesTier3",
	]
}

TygerClawsNetrunnerPartsT3 : TygerClawsLoot
{
	lootTableInclusions = 
	[
		"Loot.TygerClawsNetrunnerProgramsT3",
	]
}

TygerClawsNetrunnerCraftingT3 : TygerClawsLoot
{
	lootItems =
	[
		{ dropChance = 0.2  dropCountMin = 3 dropCountMax = 7 itemID = "Items.QuickHackUncommonMaterial1" },
		{ dropChance = 0.15 dropCountMin = 2 dropCountMax = 5 itemID = "Items.QuickHackRareMaterial1" },
		{ dropChance = 0.1  dropCountMin = 1 dropCountMax = 3 itemID = "Items.QuickHackEpicMaterial1" },
	]
}