package AimAssist

using RTDB


AimAssistMagnetism_Default : AimAssistMagnetism
{
	bool 	isEnabled						= true			
	Vector2	maxStrength						= (0.65f, 0.65f) //(yaw, pitch) at a value of 1 we completely overide the players input, at  0 we do nothing 
	
	// Aim assist kicks in based on the time until the players crosshair is no longer on the target. These define the time period where aimAssist is active
	float	maxTimeTillOffTarget			= 0.2f		// the max time till the player will be no longer looking at the target for the aim assist to kick in
	float	minTimeTillOffTarget			= 0.06f	// the min time till the player will be no longer looking at the target for the aim assist to be at max strength
	
	CName	distanceMultiplier				= 'Default_DistanceMod'		
	CName	stickInputMagMultiplier			= 'Default_InputMod'
	
	float	blendOnTime						= 0.0f		// how long to take to blend on the aim assist
	float 	blendOffTime					= 0.25f		// how long to take to blend off the aim assist
	
	
}


AimAssistMagnetism_DefaultLight : AimAssistMagnetism_Default
{
	Vector2	maxStrength						= (0.45f, 0.45f)	
}

AimAssistMagnetism_Melee : AimAssistMagnetism
{
	bool 	isEnabled						= true			
	Vector2	maxStrength						= (1.0f, 1.0f)		
	float	maxTimeTillOffTarget			= 0.15f		
	float	minTimeTillOffTarget			= 0.033f	
	CName	distanceMultiplier				= 'Melee_DistanceMod'		
	CName	stickInputMagMultiplier			= 'Melee_InputMod'
	float	blendOnTime						= 0.0001f		
	float 	blendOffTime					= 0.5f	
	bool	finishingEnabled 				= false
	bool	disableWithNoInput				= false	
	float 	softLockTimeToReach				= 0.05f 
	
}
AimAssistMagnetism_MeleeLight : AimAssistMagnetism_Melee
{
	Vector2	maxStrength						= (0.85f, 0.65f)	
}


AimAssistMagnetism_Aiming : AimAssistMagnetism
{
	bool 	isEnabled						= true			
	Vector2	maxStrength						= (0.6f, 0.6f)		
	float	maxTimeTillOffTarget			= 0.25f	
	float	minTimeTillOffTarget			= 0.1f
	CName	distanceMultiplier				= 'Aiming_DistanceMod'		
	CName	stickInputMagMultiplier			= 'Default_InputMod'
	float	blendOnTime						= 0.1f			
	float 	blendOffTime					= 0.25f		


	float pitchBoundAdditiveForYawMagnetism = 0.5f
	float yawBoundAdditiveForPitchMagnetism	= 0.5f	
	
}

AimAssistMagnetism_AimingLight : AimAssistMagnetism_Aiming
{
	Vector2	maxStrength						= (0.4f, 0.4f)	
}

AimAssistMagnetism_VehicleCombat : AimAssistMagnetism
{
	bool 	isEnabled						= true			
	Vector2	maxStrength						= (0.85f, 0.85f)		
	float	maxTimeTillOffTarget			= 0.25f		
	float	minTimeTillOffTarget			= 0.1f
	CName	distanceMultiplier				= 'VehicleCombat_DistanceMod'		
	CName	stickInputMagMultiplier			= ''
	float	blendOnTime						= 0.05f		
	float 	blendOffTime					= 0.2f	

	bool  disableWithNoInput				= false

	float pitchBoundAdditiveForYawMagnetism = 4.0f
	float yawBoundAdditiveForPitchMagnetism	= 5.0f
}

AimAssistMagnetism_VehicleCombatLight : AimAssistMagnetism_VehicleCombat
{
	Vector2	maxStrength						= (0.3f, 0.3f)	
}

AimAssistMagnetism_Scanning : AimAssistMagnetism
{
	bool 	isEnabled						= true			
	Vector2	maxStrength						= (0.75f, 0.75f)			
	float	maxTimeTillOffTarget			= 0.15f		
	float	minTimeTillOffTarget			= 0.08f	
	CName	distanceMultiplier				= 'Scanning_DistanceMod'	
	CName	stickInputMagMultiplier			= 'Default_InputMod'	
	float	blendOnTime						= 0.05f		
	float 	blendOffTime					= 0.35f	
	bool	checkWeaponEffectiveRange		= false
}
AimAssistMagnetism_ScanningLight : AimAssistMagnetism_Scanning
{
	Vector2	maxStrength						= (0.6f, 0.6f)	
}

AimAssistMagnetism_QuickMelee : AimAssistMagnetism
{
	bool 	isEnabled						= true			
	Vector2	maxStrength						= (1.0f, 0.6f)		
	float	maxTimeTillOffTarget			= 0.15f		
	float	minTimeTillOffTarget			= 0.033f	
	CName	distanceMultiplier				= 'Melee_DistanceMod'		
	CName	stickInputMagMultiplier			= 'Melee_InputMod'
	float	blendOnTime						= 0.01f		
	float 	blendOffTime					= 0.35f	
	bool	finishingEnabled 				= false
	bool	disableWithNoInput				= false
}
AimAssistMagnetism_QuickMeleeLight : AimAssistMagnetism_QuickMelee
{
	Vector2	maxStrength						= (0.5f, 0.35f)	
}


AimAssistMagnetism_LeftHandCyberware : AimAssistMagnetism
{
	bool 	isEnabled						= true			
	Vector2	maxStrength						= (0.65f, 0.6f)		
	float	maxTimeTillOffTarget			= 0.15f	
	float	minTimeTillOffTarget			= 0.033f
	CName	distanceMultiplier				= 'Default_DistanceMod'		
	CName	stickInputMagMultiplier			= 'Default_InputMod'
	float	blendOnTime						= 0.01f		
	float 	blendOffTime					= 0.35f		
}
AimAssistMagnetism_LeftHandCyberwareLight : AimAssistMagnetism_LeftHandCyberware
{
	Vector2	maxStrength						= (0.35f, 0.35f)	
}


AimAssistMagnetism_LeftHandCyberwareCharge : AimAssistMagnetism
{
	bool 	isEnabled						= true			
	Vector2	maxStrength						= (0.95f, 0.6f)		
	float	maxTimeTillOffTarget			= 0.15f	
	float	minTimeTillOffTarget			= 0.033f
	CName	distanceMultiplier				= 'Default_DistanceMod'		
	CName	stickInputMagMultiplier			= 'Default_InputMod'
	float	blendOnTime						= 0.01f		
	float 	blendOffTime					= 0.35f		
}
AimAssistMagnetism_LeftHandCyberwareChargeLight : AimAssistMagnetism_LeftHandCyberwareCharge
{
	Vector2	maxStrength						= (0.5f, 0.35f)	
}